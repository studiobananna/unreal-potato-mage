// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "UObject/Interface.h"
#include "HitboxEffect.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class UHitboxEffect : public UInterface
{
	GENERATED_BODY()
};

/**
 * 
 */
class GAMEPROJECT_API IHitboxEffect
{
	GENERATED_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Battle")
		void SpawnHitEffect();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle")
		void OnHitEffect();
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle")
		FVector GetDirection();
};

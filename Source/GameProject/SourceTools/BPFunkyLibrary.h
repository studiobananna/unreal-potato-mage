// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Kismet/BlueprintFunctionLibrary.h"
#include "BPFunkyLibrary.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API UBPFunkyLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()
public:
	UFUNCTION(BlueprintPure)
		static FString AddLeadingZeroes(const int32& UglyNumber, const int Length);

	UFUNCTION(BlueprintPure)
		static FVector RandomPointBetweenTwoPoints(const FVector& point1, const FVector& point2);
};

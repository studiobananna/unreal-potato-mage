// (C) Copyright Studio Bananna, all rights reserved


#include "SourceTools/BPFunkyLibrary.h"

FString UBPFunkyLibrary::AddLeadingZeroes(const int32& UglyNumber, const int Length)
{
	char Buffer[100];
	int RespCode;
	RespCode = snprintf(Buffer, 100, "%0*d", Length, UglyNumber);

	return FString(ANSI_TO_TCHAR(Buffer));
}

FVector UBPFunkyLibrary::RandomPointBetweenTwoPoints(const FVector& point1, const FVector& point2)
{
	// Create vector point in between two points
	float x = FMath::FRandRange(point1.X, point2.X);
	float y = FMath::FRandRange(point1.Y, point2.Y);
	float z = FMath::FRandRange(point1.Z, point2.Z);

	return FVector(x, y, z);
}

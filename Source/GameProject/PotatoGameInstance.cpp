// (C) Copyright Studio Bananna, all rights reserved


#include "PotatoGameInstance.h"
#include "GameProject/PotatoSaveGame.h"

UPotatoGameInstance::UPotatoGameInstance()
{

}

void UPotatoGameInstance::Init()
{
	Super::Init();
	/** virtual function to allow custom GameInstances an opportunity to set up what it needs */

	// Set current room we are in just for testing
	SCENE_MAP_NAME = GetWorld()->GetMapName();
	SCENE_MAP_NAME.RemoveFromStart(GetWorld()->StreamingLevelsPrefix);

	PL_FightUnlocks.Add("LUMINA");
	PL_FightUnlocks.Add("ROGELIO");

	// Get level scale global
	APotatoWorldSettings* WorldSettings = Cast<APotatoWorldSettings>(GetWorld()->GetWorldSettings());
	if (WorldSettings) {
		LevelScale = WorldSettings->LevelScale;
	}

}

void UPotatoGameInstance::Shutdown()
{
	Super::Shutdown();
	/** virtual function to allow custom GameInstances an opportunity to do cleanup when shutting down */

}

void UPotatoGameInstance::EmptySavedInventory()
{
	PL_InvMagicArry.Empty();
	PL_MagicArry.Empty();
    PL_InvWeaponArry.Empty();
	PL_EquipWeaponArry.Empty();
	PL_QuestArry.Empty();
	PL_InvItemArry.Empty();
}

void UPotatoGameInstance::SaveGame(int SaveSlot)
{
	// Create instance of our save game class
	UPotatoSaveGame* SaveGameInstance = Cast<UPotatoSaveGame>(UGameplayStatics::CreateSaveGameObject(UPotatoSaveGame::StaticClass()));

	// Set the save game variables to these variables
	// -------------SAVE GAME VARIABLES TO FILE--------------------------
	SaveGameInstance->PL_health			= PL_health;
	SaveGameInstance->PL_mana			= PL_mana;
	SaveGameInstance->PL_Level			= PL_Level;
	SaveGameInstance->PL_Skillpoints	= PL_Skillpoints;
	SaveGameInstance->PL_vitality		= PL_vitality;
	SaveGameInstance->PL_haste			= PL_haste;
	SaveGameInstance->PL_dexterity		= PL_dexterity;
	SaveGameInstance->PL_intelligence	= PL_intelligence;
	SaveGameInstance->PL_EXP			= PL_EXP;
	SaveGameInstance->PL_EXP_MAX		= PL_EXP_MAX;
	SaveGameInstance->PL_B_UNLOCKED_SPRINT	= PL_B_UNLOCKED_SPRINT;
	SaveGameInstance->PL_B_UNLOCKED_SKILLS	= PL_B_UNLOCKED_SKILLS;
	SaveGameInstance->PL_SpawnPoint			= PL_SpawnPoint;
	SaveGameInstance->MYTH_wep				= MYTH_wep;
	SaveGameInstance->MYTH_BodyType			= MYTH_BodyType;
	SaveGameInstance->EVT_EventHolder01		= EVT_EventHolder01;
	SaveGameInstance->EVT_HideHUD			= EVT_HideHUD;
	SaveGameInstance->EVT_Intro_Scene003	= EVT_Intro_Scene003;
	SaveGameInstance->EVT_NPC_Flan			= EVT_NPC_Flan;
	SaveGameInstance->EVT_NPC_Frederick		= EVT_NPC_Frederick;
	SaveGameInstance->PL_shungite_CUR		= PL_shungite_CUR;
	SaveGameInstance->PL_InvMagicArry		= PL_InvMagicArry;
	SaveGameInstance->PL_MagicArry			= PL_MagicArry;
	SaveGameInstance->PL_DevilVision		= PL_DevilVision;
	SaveGameInstance->PL_InvWeaponArry		= PL_InvWeaponArry;
	SaveGameInstance->PL_EquipWeaponArry	= PL_EquipWeaponArry;
	SaveGameInstance->PL_InvItemArry		= PL_InvItemArry;
	SaveGameInstance->PL_MAP_CollectedItems = PL_MAP_CollectedItems;
	SaveGameInstance->PL_MagicArryScrollType = PL_MagicArryScrollType;

	// Quest Vars
	SaveGameInstance->PL_QuestArry			= PL_QuestArry;
	SaveGameInstance->PL_TrackedQuest		= PL_TrackedQuest;

	// Get Map Name and trim unnecessary stuff (OUTDATED)
	SaveGameInstance->SCENE_MAP_NAME		= GetWorld()->GetMapName();
	SaveGameInstance->SCENE_MAP_NAME.RemoveFromStart(GetWorld()->StreamingLevelsPrefix);

	// New scene saving system should have a name provided by the save station.
	SaveGameInstance->SCENE_SAVE_STATION_NAME = SCENE_SAVE_STATION_NAME;

	// Save General/Global Vars
	int32 seconds = 0;
	float partialSeconds = 0.f;
	UGameplayStatics::GetAccurateRealTime(seconds, partialSeconds); // Gets the time since app was started
	SaveGameInstance->GLOB_GameTime = seconds;	// Save the time in seconds

	// Save game time
	if (LoadGame(SaveSlot)) // If time exists in overwrite, then add onto it
	{
		SaveGameInstance->GLOB_GameTime += LoadGame(SaveSlot)->GLOB_GameTime - TimerResetModifier;
		TimerResetModifier = seconds;
	}

	// Get random char portrait
	SaveGameInstance->GLOB_Charport			= FMath::RandRange(0, 2);

	// Save current player stuff if valid
	if (UGameplayStatics::GetPlayerPawn(this, 0))
	{
		SaveGameInstance->SCENE_PLAYER_LOCATION = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorLocation();
		SaveGameInstance->SCENE_PLAYER_ROTATION = UGameplayStatics::GetPlayerPawn(this, 0)->GetActorRotation();
	}

	// Save the save game instance
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, FString::FromInt(SaveSlot), 0);

	// Print game was saved
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("GAME SAVED!"));
}

UPotatoSaveGame* UPotatoGameInstance::LoadGame(int SaveSlot)
{
	// Create instance of our save game class
	UPotatoSaveGame* SaveGameInstance = Cast<UPotatoSaveGame>(UGameplayStatics::CreateSaveGameObject(UPotatoSaveGame::StaticClass()));
	//Load the save game into our save instance variable
	SaveGameInstance  = Cast<UPotatoSaveGame>(UGameplayStatics::LoadGameFromSlot(FString::FromInt(SaveSlot), 0));
	// Print game was loaded
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("GAME LOADED!"));

	// return to save inst to see if it's valid
	return SaveGameInstance;
}

bool UPotatoGameInstance::StartLoadedGame(int SaveSlot)
{
	if (LoadGame(SaveSlot))
	{
		UPotatoSaveGame* SaveGameInstance = LoadGame(SaveSlot);
		PL_health			= SaveGameInstance->PL_health;
		PL_mana				= SaveGameInstance->PL_mana;
		PL_Level			= SaveGameInstance->PL_Level;
		PL_Skillpoints		= SaveGameInstance->PL_Skillpoints;
		PL_vitality			= SaveGameInstance->PL_vitality;
		PL_haste			= SaveGameInstance->PL_haste;
		PL_dexterity		= SaveGameInstance->PL_dexterity;
		PL_intelligence		= SaveGameInstance->PL_intelligence;
		PL_EXP				= SaveGameInstance->PL_EXP;
		PL_EXP_MAX			= SaveGameInstance->PL_EXP_MAX;
		PL_B_UNLOCKED_SPRINT	= SaveGameInstance->PL_B_UNLOCKED_SPRINT;
		PL_B_UNLOCKED_SKILLS	= SaveGameInstance->PL_B_UNLOCKED_SKILLS;
		PL_SpawnPoint			= SaveGameInstance->PL_SpawnPoint;
		MYTH_wep				= SaveGameInstance->MYTH_wep;
		MYTH_BodyType			= SaveGameInstance->MYTH_BodyType;
		EVT_EventHolder01		= SaveGameInstance->EVT_EventHolder01;
		EVT_HideHUD				= SaveGameInstance->EVT_HideHUD;
		EVT_Intro_Scene003		= SaveGameInstance->EVT_Intro_Scene003;
		EVT_NPC_Flan			= SaveGameInstance->EVT_NPC_Flan;
		EVT_NPC_Frederick		= SaveGameInstance->EVT_NPC_Frederick;
		PL_shungite_CUR			= SaveGameInstance->PL_shungite_CUR;
		PL_InvMagicArry			= SaveGameInstance->PL_InvMagicArry;
		PL_MagicArry			= SaveGameInstance->PL_MagicArry;
		PL_DevilVision			= SaveGameInstance->PL_DevilVision;
		PL_InvWeaponArry		= SaveGameInstance->PL_InvWeaponArry;
		PL_EquipWeaponArry		= SaveGameInstance->PL_EquipWeaponArry;
		PL_InvItemArry			= SaveGameInstance->PL_InvItemArry;
		PL_MAP_CollectedItems	= SaveGameInstance->PL_MAP_CollectedItems;
		PL_MagicArryScrollType  = SaveGameInstance->PL_MagicArryScrollType;

		// Quest Vars
		PL_QuestArry = SaveGameInstance->PL_QuestArry;
		PL_TrackedQuest = SaveGameInstance->PL_TrackedQuest;

		// Get Map Name and trim unnecessary stuff
		SCENE_MAP_NAME = SaveGameInstance->SCENE_MAP_NAME;
		SCENE_SAVE_STATION_NAME = SaveGameInstance->SCENE_SAVE_STATION_NAME;
		SCENE_PLAYER_LOCATION = SaveGameInstance->SCENE_PLAYER_LOCATION;
		SCENE_PLAYER_ROTATION = SaveGameInstance->SCENE_PLAYER_ROTATION;

		// Print load completed
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("File load COMPLETED!"));
		return true;
	}
	else
	{
		// Print load failed
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("File load FAILED!"));
		return false;
	}
}

void UPotatoGameInstance::ResetGameInstance()
{

	// Resets everything in the game instance
	UPotatoGameInstance* DefaultClass = (UPotatoGameInstance*)GetClass()->GetDefaultObject();

	PL_health					= DefaultClass->PL_health;
	PL_mana						= DefaultClass->PL_mana;
	PL_Level				    = DefaultClass->PL_Level;
	PL_Skillpoints				= DefaultClass->PL_Skillpoints;
	PL_vitality					= DefaultClass->PL_vitality;
	PL_haste					= DefaultClass->PL_haste;
	PL_dexterity				= DefaultClass->PL_dexterity;
	PL_intelligence			    = DefaultClass->PL_intelligence;
	PL_EXP						= DefaultClass->PL_EXP;
	PL_EXP_MAX					= DefaultClass->PL_EXP_MAX;
	PL_B_UNLOCKED_SPRINT		= DefaultClass->PL_B_UNLOCKED_SPRINT;
	PL_B_UNLOCKED_SKILLS		= DefaultClass->PL_B_UNLOCKED_SKILLS;
	PL_SpawnPoint				= DefaultClass->PL_SpawnPoint;
	MYTH_wep					= DefaultClass->MYTH_wep;
	MYTH_BodyType				= DefaultClass->MYTH_BodyType;
	EVT_EventHolder01			= DefaultClass->EVT_EventHolder01;
	EVT_HideHUD					= DefaultClass->EVT_HideHUD;
	EVT_Intro_Scene003			= DefaultClass->EVT_Intro_Scene003;
	EVT_NPC_Flan				= DefaultClass->EVT_NPC_Flan;
	EVT_NPC_Frederick			= DefaultClass->EVT_NPC_Frederick;
	PL_shungite_CUR				= DefaultClass->PL_shungite_CUR;
	PL_InvMagicArry				= DefaultClass->PL_InvMagicArry;
	PL_MagicArry				= DefaultClass->PL_MagicArry;
	PL_DevilVision				= DefaultClass->PL_DevilVision;
	PL_InvWeaponArry			= DefaultClass->PL_InvWeaponArry;
	PL_EquipWeaponArry			= DefaultClass->PL_EquipWeaponArry;
	PL_InvItemArry				= DefaultClass->PL_InvItemArry;
	PL_MAP_CollectedItems		= DefaultClass->PL_MAP_CollectedItems;

	// Quest Vars
	PL_QuestArry    = DefaultClass->PL_QuestArry;
	PL_TrackedQuest = DefaultClass->PL_TrackedQuest;

	// Get Map Name and trim unnecessary stuff
	SCENE_MAP_NAME		  = DefaultClass->SCENE_MAP_NAME;
	SCENE_PLAYER_LOCATION = DefaultClass->SCENE_PLAYER_LOCATION;
	SCENE_PLAYER_ROTATION = DefaultClass->SCENE_PLAYER_ROTATION;

	// Print load completed
	GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, TEXT("RESET GAME INST COMPLETED!"));
}

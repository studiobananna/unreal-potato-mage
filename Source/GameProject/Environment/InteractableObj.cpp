// (C) Copyright Studio Bananna, all rights reserved


#include "Environment/InteractableObj.h"

// Sets default values
AInteractableObj::AInteractableObj()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	TargetSphere = CreateDefaultSubobject<USphereComponent>(TEXT("TargettingSphere"));
	TargetSphere->OnComponentBeginOverlap.AddDynamic(this, &AInteractableObj::OnOverlapBegin);
	TargetSphere->OnComponentEndOverlap.AddDynamic(this, &AInteractableObj::OnOverlapEnd);
	RootComponent = TargetSphere;
}

// Called when the game starts or when spawned
void AInteractableObj::BeginPlay()
{
	Super::BeginPlay();
	TargetSphere->SetSphereRadius(0.f, true);
	
}

// Called every frame
void AInteractableObj::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Get the current radius
	float currRad = TargetSphere->GetUnscaledSphereRadius();

	// Then scale the target comp. to that radius on startup
	if(currRad <= TargetMaxRadius - 2.f)
		TargetSphere->SetSphereRadius(FMath::FInterpConstantTo(currRad, TargetMaxRadius, DeltaTime, 5.f), true);

}

void AInteractableObj::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	if (bActiveInteract) 
	{
		// When the player collides with the target radius, set the player's 'event target' to this
		if (OtherActor->GetClass()->IsChildOf(ABaseCharacter::StaticClass()))
		{
			if (UGameplayStatics::GetPlayerPawn(this, 0) == OtherActor)
			{
				ABaseCharacter* Target = (ABaseCharacter*)OtherActor;
				Target->EventUseTarget = this;

				// Print Message
				if (GEngine) GEngine->AddOnScreenDebugMessage(101, 1.0f, FColor::Blue, FString::Printf(TEXT("Interactable Set- %s"), *this->GetName()), true);
			}
		}
	}
}

void AInteractableObj::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (bActiveInteract)
	{
		// Then when not colliding with the player anymore, set the player's target to null
		if (OtherActor->GetClass()->IsChildOf(ABaseCharacter::StaticClass()))
		{
			if (UGameplayStatics::GetPlayerPawn(this, 0) == OtherActor)
			{
				ABaseCharacter* Target = (ABaseCharacter*)OtherActor;
				Target->EventUseTarget = nullptr;

				// Print Message
				if (GEngine) GEngine->AddOnScreenDebugMessage(101, 1.0f, FColor::Blue, FString::Printf(TEXT("Interactable Unset")), true);
			}
		}
	}
}

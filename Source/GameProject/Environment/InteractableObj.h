// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SphereComponent.h"
#include "mythorChar/BaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "InteractableObj.generated.h"

UCLASS()
class GAMEPROJECT_API AInteractableObj : public AActor, public IUseTalkEvent
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AInteractableObj();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	/*----------Collision Triggers----------*/
	//Overlap Functions for player targetting obj
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Targetting")
		USphereComponent* TargetSphere;

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Radius the Target Comp. will scale to upon startup
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Targeting")
		float TargetMaxRadius = 1000.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Targeting")
		bool bActiveInteract = true;

};

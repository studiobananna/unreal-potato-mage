// (C) Copyright Studio Bananna, all rights reserved


#include "BreakableObj.h"
#include "Kismet/GameplayStatics.h"
#include "Enemy/General/EnemyBattleActive.h"
#include "mythorChar/BaseCharacter.h"

// Sets default values
ABreakableObj::ABreakableObj()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ABreakableObj::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ABreakableObj::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ABreakableObj::DropItems()
{
	// Spawn any drops
	FActorSpawnParameters SpawnInfo;

	for (int j = 0; j < numOfDrops; j++) {
		int32 i = FMath::RandRange(0, DropsArry.Num() - 1);

		if (i < DropsArry.Num()) {
			if (DropsArry[i])
			{
				AActor* item = GetWorld()->SpawnActor<AActor>(DropsArry[i], GetActorLocation()+FVector(0.f,0.f,12.f), FRotator(0.f, 0.f, 0.f), SpawnInfo);

				if (bSpawnDropsOnUser)
				{
					AActor* Character = UGameplayStatics::GetPlayerPawn(GetWorld(), 0);
					item->SetActorLocation(Character->GetActorLocation());
				}
			}
		}
	}

	// Save Collected
	SaveObjectUsed();
}

void ABreakableObj::TriggerEventActor()
{
	// Tell any event actors to remove self
	if (eventBattleActor) {
		for (int i = 0; i < eventBattleActor->InteractableArray.Num(); i++) {
			if (eventBattleActor->InteractableArray[i] == this) 
			{
				eventBattleActor->InteractableArray.RemoveAt(i);
				if (eventBattleActor->InteractableArray.Num() == 0 && eventBattleActor->EnemyArray.Num() == 0) 
				{
					// Trigger the Ending Result of the Event Actor
					eventBattleActor->OnDefeatedEnemies();
				}
			}
		}
	}
}

void ABreakableObj::TriggerQuestLog()
{
	// Tell Player, the Enemy was Defeated
	ABaseCharacter* MyCharacter = Cast<ABaseCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));

	if (MyCharacter) {
		MyCharacter->OnDefeatDelegate.Broadcast(QuestTargName);
	}
}

void ABreakableObj::ObjectCollected()
{
	// If already collected, then just set to hit
	bisHit = true;

	Super::ObjectCollected();
}


// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Inventory/SavableCollectedObject.h"
#include "BreakableObj.generated.h"

UCLASS()
class GAMEPROJECT_API ABreakableObj : public ASavableCollectedObject
{
	GENERATED_BODY()
	
public:	
	// ----------------BREAKABLE / INTERACTABLE OBJECT----------------

	// Sets default values for this actor's properties
	ABreakableObj();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drops", meta = (ExposeOnSpawn = "true"))
		int32 numOfDrops = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drops", meta = (ExposeOnSpawn = "true"))
		bool bSpawnDropsOnUser = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drops", meta = (ExposeOnSpawn = "true"))
		bool bisHit = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drops", meta = (ExposeOnSpawn = "true"))
		TArray<TSubclassOf<AActor>> DropsArry;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battle")
		class AEnemyBattleActive* eventBattleActor;

	UFUNCTION(BlueprintCallable, Category = "Events")
		void DropItems();

	UFUNCTION(BlueprintCallable, Category = "Events")
		void TriggerEventActor();

	UFUNCTION(BlueprintCallable, Category = "Events")
		void TriggerQuestLog();

		virtual void ObjectCollected() override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quests")
		FString QuestTargName = "QUEST_TARGET";

};

// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/SaveGame.h"
#include "Kismet/GameplayStatics.h"
#include "PotatoSaveGame.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API UPotatoSaveGame : public USaveGame
{
	GENERATED_BODY()
public:

	UPotatoSaveGame();
	
	// -------------PLAYER GENERAL STAT SAVE--------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Current Stats")
		float PL_health = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Current Stats")
		float PL_mana = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stats")
		int32 PL_Level = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stats")
		int32 PL_Skillpoints = 8;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Leveling Stats")
		int32 PL_vitality = 3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Leveling Stats")
		int32 PL_haste = 3;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Leveling Stats")
		int32 PL_dexterity = 4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Leveling Stats")
		int32 PL_intelligence = 4;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stats")
		int32 PL_EXP = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stats")
		int32 PL_EXP_MAX = 120.f;

	// UNLOCKS
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Unlocks")
		bool PL_B_UNLOCKED_SPRINT = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Unlocks")
		bool PL_B_UNLOCKED_SKILLS = false;

	// -------------PLAYER SPAWN LOCATION--------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stats")
		int32 PL_SpawnPoint = 0;

	// -------------PLAYER MYTHOR SAVE--------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Mythor Stats")
		int32 MYTH_wep = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Mythor Stats") // Change Mythor's materials for events
		int32 MYTH_BodyType = 0;

	// -------------GLOBAL/GENERAL BASED VARS--------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Events") // Total game time
		int32 GLOB_GameTime = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Events") // Total game time
		int32 GLOB_Charport = 0;



	// -------------EVENT BASED VARS--------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Events") // Holds a value to trigger certain events (which boss to spawn for boss arena...etc.) (USE WITH CAUTION)
		int32 EVT_EventHolder01 = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Events") // Hide the HUD
		bool EVT_HideHUD = false;


	// --------------------------NPC EVENT VARS--------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NPC") // Event trigger for Intro003
		bool EVT_Intro_Scene003 = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NPC") // Event handling for Flanna NPC
		int32 EVT_NPC_Flan = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "NPC") // Event handling for Frederick NPC
		int32 EVT_NPC_Frederick = 0;



	// -------------SAVE MAP AND LOCATION VARS--------------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map") // Save Map Name to Load
		FString SCENE_MAP_NAME = "MP_Intro_001";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map") // Save Station saved at to display
		FString SCENE_SAVE_STATION_NAME = "Test";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map") // Save Map Player Location
		FVector SCENE_PLAYER_LOCATION = FVector(0.f, 0.f, 0.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Map") // Save Map Player Rotation
		FRotator SCENE_PLAYER_ROTATION = FRotator(0.f, 0.f, 0.f);



	// -------------PLAYER SAVE INVENTORY--------------------------
	// ---------------------Currency---------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Stats")
		int32 PL_shungite_CUR = 0;

	//---------------------Magics---------------------
	// All collected Magics in inventory
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		TArray<FString> PL_InvMagicArry;
	// Equipped Magics
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		TArray<FString> PL_MagicArry;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		int32 PL_MagicArryScrollType = 1;

	//----------PASSIVES--------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Passives")
		bool PL_DevilVision = true;

	//---------------------Weapon Equips---------------------
	// All collected Weapons in inventory
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		TArray<FString> PL_InvWeaponArry;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		TArray<FString> PL_EquipWeaponArry;

	//---------------------Items---------------------
	// All collected Items in inventory
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		TArray<FString> PL_InvItemArry;
	// Save Collected Items
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		TMap<FString, bool> PL_MAP_CollectedItems;

	//---------------------Quests---------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quests")
		TArray<FString> PL_QuestArry;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quests")
		FString PL_TrackedQuest;

};

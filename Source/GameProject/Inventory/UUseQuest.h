// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "UUseQuest.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent), Blueprintable)
class GAMEPROJECT_API UUUseQuest : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUUseQuest();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// The optional thumbnail for this Quest
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Quests")
		class UTexture2D* Thumbnail;

	// The display name for this Quest in the inventory
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Quests")
		FString QuestName = "Quest Name";

	// A description for the Quest
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Quests", meta = (MultiLine = true))
		FText QuestDescription;

	// Is Quest Complete?
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Quests")
		bool isCompleted = false;

	// The Area where this quest should be
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Quests")
		FString QuestArea = "Insert Map Name";

	// The Location in an Area where this quest should be
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Quests")
		FVector QuestLocation = FVector(0.f, 0.f, 0.f);

	// The Enemies to Defeat to Complete the Quest
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Quests")
		TArray<AActor*> EnemiesToDefeat;

	// The amount to defeat per enemy
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Quests")
		TArray<int32> EnemyCount;

	// The Character that owns the Quest
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character", meta = (ExposeOnSpawn = "true"))
		class ABaseCharacter* Character;

	UFUNCTION(BlueprintImplementableEvent)
		void OnComplete();
};

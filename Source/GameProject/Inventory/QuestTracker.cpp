// (C) Copyright Studio Bananna, all rights reserved


#include "QuestTracker.h"


UQuestTracker::UQuestTracker(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer){

}
void UQuestTracker::NativePreConstruct()
{
	Super::NativePreConstruct();
}

void UQuestTracker::NativeConstruct()
{
	Super::NativeConstruct();
}

void UQuestTracker::NativeDestruct()
{
	Super::NativeDestruct();
}

void UQuestTracker::NativeTick(const FGeometry & MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

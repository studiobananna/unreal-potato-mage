// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../mythorChar/Magic/UseMagic.h"
#include "GenericPlatform/GenericPlatformMisc.h"
#include "../Inventory/CharHotbarAbilityComponent.h"
#include "HotbarComponent.generated.h"


UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAMEPROJECT_API UHotbarComponent : public UActorComponent
{
	GENERATED_BODY()
private:
	ABaseCharacter* Character;

public:	
	// Sets default values for this component's properties
	UHotbarComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;
	virtual void BeginDestroy() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(BlueprintReadWrite, Category = "Magic")
		int32 hotbarArrayType = 0;

	// Hotbar Functions
	UFUNCTION()void CycleHotbarRowUp();
	UFUNCTION()void CycleHotbarRowDown();
	UFUNCTION()void UseHotbarItemStart(int inputType);
	UFUNCTION()void UseHotbarItem000();
	UFUNCTION()void UseHotbarItem001();
	UFUNCTION()void UseHotbarItem002();
	UFUNCTION()void UseHotbarItem003();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Magic")
		void NotEnoughMana();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Magic")
		void OnCoolDown();

	// Character owner functions
	UFUNCTION(BlueprintCallable)
		ABaseCharacter* GetOwnerCharacter() { return Character; }
	UFUNCTION()
		void SetOwnerCharacter(ABaseCharacter* inCharacter) { Character = inCharacter; }
};

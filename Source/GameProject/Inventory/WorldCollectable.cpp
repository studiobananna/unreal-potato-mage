// (C) Copyright Studio Bananna, all rights reserved


#include "WorldCollectable.h"

// Sets default values
AWorldCollectable::AWorldCollectable()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Get Targetting Sphere----------------------------
	Collision = CreateDefaultSubobject<USphereComponent>(TEXT("TargetSphere"));
	Collision->SetCollisionProfileName(TEXT("Trigger"));
	Collision->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	Collision->SetVisibility(true);
	Collision->SetCollisionObjectType(ECollisionChannel::ECC_Pawn);
	Collision->CanCharacterStepUp(false);
	Collision->SetCanEverAffectNavigation(false);
	SetRootComponent(Collision);

	Collision->OnComponentBeginOverlap.AddDynamic(this, &AWorldCollectable::OnOverlapBegin);
	Collision->OnComponentEndOverlap.AddDynamic(this, &AWorldCollectable::OnOverlapEnd);

}

// Called when the game starts or when spawned
void AWorldCollectable::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AWorldCollectable::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AWorldCollectable::OnOverlapBegin(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult & SweepResult)
{
	// When this actor is hit by the player
	if (OverlappedComp == Collision && OtherActor && m_CanCollectItem) {
		if (OtherActor == UGameplayStatics::GetPlayerPawn(this, 0) && OtherActor->GetClass()->IsChildOf(ABaseCharacter::StaticClass())) {

			// Get the character
			ABaseCharacter* Character = (ABaseCharacter*)OtherActor;

			// Check if only the capsule component
			if (OtherComp == Character->GetCapsuleComponent()) {
				for (const TPair<TSubclassOf<UUseItem>, int32>& pair : items)
				{
					// For Each Item in the Array
					if (pair.Key && pair.Value)
					{
						// Get the item name
						FName itemName = *pair.Key.Get()->GetName();
						UE_LOG(LogTemp, Warning, TEXT("Item %s In Pool!"), *FString(itemName.ToString()));

						// Construct the item for the character, then add it
						UActorComponent* newItem = Character->CharActorCompHandler->ConstructFromClass(pair.Key);

						// Reassign the Component to get and change the item count
						UUseItem* newUseItem = (UUseItem*)newItem;

						// return if item is not valid
						if (!newUseItem)
						{
							UE_LOG(LogTemp, Error, TEXT("ITEM %s NOT VALID!!!"), *FString(itemName.ToString()));
							return;
						}

						newUseItem->ItemCount = pair.Value;

						// Pass the item reference to blueprints
						AddItemToCharacter_BP(newUseItem);

						// Execute any item on pickup function
						newUseItem->OnPickUp();

						// Add the item
						if(newUseItem->bCanPickUp)
							Character->Inventory->AddItemToInventory(newUseItem);
						else
							newUseItem->ConditionalBeginDestroy();
					}
				}

				// Then execute the on hit function
				OnPlayerHit();

				// Save the object that was collected
				SaveObjectUsed();
			}
		}
	}
}

void AWorldCollectable::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{

}

void AWorldCollectable::ObjectCollected()
{
	//Destroy Self if saved
	Destroy();
}


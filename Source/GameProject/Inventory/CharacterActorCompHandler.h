// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CharacterActorCompHandler.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEPROJECT_API UCharacterActorCompHandler : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCharacterActorCompHandler();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Construct actor comp from class ref
	UFUNCTION(BlueprintCallable, Category="Components")
		UActorComponent* ConstructFromClass(TSubclassOf<class UActorComponent> component);

	// Remove actor comp from actor
	UFUNCTION(BlueprintCallable, Category = "Components")
		void RemoveActorComponent(UActorComponent* component);
		
};

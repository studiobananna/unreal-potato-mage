// (C) Copyright Studio Bananna, all rights reserved


#include "Inventory/SavableCollectedObject.h"

// Sets default values
ASavableCollectedObject::ASavableCollectedObject()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void ASavableCollectedObject::BeginPlay()
{
	Super::BeginPlay();
	
	// Check and see if this object should spawn in used from save
	if (bisSaveCollected)
	{
		// Check if object exists in the game instance
		UPotatoGameInstance* GameInst = (UPotatoGameInstance*)GetGameInstance();

		// Get the Level Name to Save
		FString scene = GetWorld()->GetMapName();
		scene.RemoveFromStart(GetWorld()->StreamingLevelsPrefix);

		// Get the object's name, then append the level name
		OBJ_SAVE_NAME = UKismetSystemLibrary::GetObjectName(this) + scene;

		if (GameInst->PL_MAP_CollectedItems.Contains(OBJ_SAVE_NAME))  
		{
			// Do something if already collected the object
			ObjectCollected();
		}

	}
}

// Called every frame
void ASavableCollectedObject::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void ASavableCollectedObject::SaveObjectUsed()
{
	// Add the item to list of collected items if needed
	if (bisSaveCollected)
	{
		// Get the Level Name to Save
		FString scene = GetWorld()->GetMapName();
		scene.RemoveFromStart(GetWorld()->StreamingLevelsPrefix);

		// Get the object's name, then append the level name
		OBJ_SAVE_NAME = UKismetSystemLibrary::GetObjectName(this) + scene;

		// Save this name to the game instance
		UPotatoGameInstance* GameInst = (UPotatoGameInstance*)GetGameInstance();
		GameInst->PL_MAP_CollectedItems.Add(OBJ_SAVE_NAME, true);

		// Print object saved
		if(GEngine) GEngine->AddOnScreenDebugMessage(100, 1.0f, FColor::Yellow, FString::Printf(TEXT("Collected - %s"), *FString(OBJ_SAVE_NAME)), true);
	}
}

void ASavableCollectedObject::ObjectCollected()
{
	// This could be overrided...
	ObjectCollected_BP();

	if (bCanDestroyUponLoaded)
		Destroy();
}

void ASavableCollectedObject::ManuallySaveObjectUsed()
{
	SaveObjectUsed();
}


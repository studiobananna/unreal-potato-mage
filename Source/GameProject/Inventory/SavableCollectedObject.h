// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Kismet/GameplayStatics.h"
#include "PotatoGameInstance.h"
#include "SavableCollectedObject.generated.h"

UCLASS()
class GAMEPROJECT_API ASavableCollectedObject : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASavableCollectedObject();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

protected:
	//------SAVING ITEMS-----
	// Save the object's unique name to prevent respawns when collected
	UPROPERTY()
		FString OBJ_SAVE_NAME;
	// If we should save this object's state in the first place
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Saving", meta = (ExposeOnSpawn = "true"))
		bool bisSaveCollected = true;
	// If this object is loaded from a save should it be destroyed?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Saving", meta = (ExposeOnSpawn = "true"))
		bool bCanDestroyUponLoaded = true;

public:
	// Saving the items
	UFUNCTION(BlueprintCallable)
		void SaveObjectUsed();
	UFUNCTION(BlueprintCallable)
		virtual void ObjectCollected();
	UFUNCTION(BlueprintImplementableEvent, Category = "Events")
		void ObjectCollected_BP();
	UFUNCTION(BlueprintCallable)
		void ManuallySaveObjectUsed();

};

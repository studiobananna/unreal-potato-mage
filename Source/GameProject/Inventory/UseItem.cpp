// (C) Copyright Studio Bananna, all rights reserved


#include "UseItem.h"
#include "mythorChar/BaseCharacter.h"

// Sets default values for this component's properties
UUseItem::UUseItem()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

// Called when the game starts
void UUseItem::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UUseItem::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	cooldown > 0.f ? cooldown -= (1.f * DeltaTime) : cooldown = 0.f;
	// ...
}

void UUseItem::SubtractCount()
{
	ItemCount--;

	if (ItemCount == 0)
	{
		if (OwningCharacter)
		{
			if (OwningCharacter->Inventory)
			{
				// Remove from Inventory
				OwningCharacter->Inventory->InvItemArry.Remove(this);

				// Log
				UE_LOG(LogTemp, Warning, TEXT("%s Removed from inventory for out of stock!"), *FString(GetClass()->GetName()));

				// Find the index of the item in the magic array
				int32 findIndex = OwningCharacter->Inventory->MagicArry.Find(this);

				if (findIndex != INDEX_NONE)
					// Set null in magic array
					OwningCharacter->Inventory->MagicArry[findIndex] = nullptr;

				// Destroy
				DestroyComponent();
			}
		}
	}
}
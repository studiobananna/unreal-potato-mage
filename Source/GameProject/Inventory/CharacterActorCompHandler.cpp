// (C) Copyright Studio Bananna, all rights reserved


#include "Inventory/CharacterActorCompHandler.h"

// Sets default values for this component's properties
UCharacterActorCompHandler::UCharacterActorCompHandler() {
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UCharacterActorCompHandler::BeginPlay() {
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UCharacterActorCompHandler::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

UActorComponent* UCharacterActorCompHandler::ConstructFromClass(TSubclassOf<class UActorComponent> component) {
	// Create a New Object
	UActorComponent* newComponent = NewObject<UActorComponent>(this, component);

	// Then Attach the component to this
	if (newComponent) {
		newComponent->RegisterComponent();

		UE_LOG(LogTemp, Warning, TEXT("Class Contructed: %s"), *FString(newComponent->GetClass()->GetName()));
	}

	return newComponent; // Return it to blueprints

}

void UCharacterActorCompHandler::RemoveActorComponent(UActorComponent* Component) {
	if (Component)
		Component->DestroyComponent();
}

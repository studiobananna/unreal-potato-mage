// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "mythorChar/Weapons/BaseAbility.h"
#include "UseItem.generated.h"


UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAMEPROJECT_API UUseItem : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUseItem();

	// Inventory Stuff
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Item")
		class UTexture2D* InvItemThumbnail;

	// The display name for this item in the inventory
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
		FString InvItemName;

	// An optional description for the item
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item", meta = (MultiLine = true))
		FText InvItemDesc;

	// This count is only for items that can stack (Not Weapons or Magics)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		int ItemCount = 1;

	// See if item can stack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		bool bCanStack = true;

	// See if item can be picked up
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		bool bCanPickUp = true;

	// See if item can be equipped on multiple slots
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		bool bCanMultiEquip = true;

	// See if item can be used
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		bool bisUseable = true;

	// The character that owns the item
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character")
		class ABaseCharacter* OwningCharacter;

	// The shop item associated with this item
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Shop")
		TSubclassOf<class AWorldCollectable> m_ShopItem;

	UFUNCTION(BlueprintImplementableEvent)
		void OnUse(class ABaseCharacter* Character);

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void OnEquip();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void OnUnequip();

	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable)
		void OnPickUp();

	UFUNCTION(BlueprintCallable)
		void SubtractCount();

	// The name to use for checking how this item is casted or read from the Magic (Hotbar) Array (ONLY FOR MAGICS AND ITEMS)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
		FString HashName;

	// The item to spawn when used from the hotbar (FOR ITEMS ONLY)
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Item")
		TSubclassOf<ABaseAbility> ItemSpawn;

	// Item Cooldown
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		float cooldown = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item")
		float cooldownMax = 1.25f;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};

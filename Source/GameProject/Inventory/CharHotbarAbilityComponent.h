// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../mythorChar/Magic/UseMagic.h"
#include "CharHotbarAbilityComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEPROJECT_API UCharHotbarAbilityComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UCharHotbarAbilityComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Associates a string with an integer in order to use it with a switch statement (Sucks since C++ doesn't have string switches :( )
	UPROPERTY()
		TMap<FString, int32> toHotbarFunctionMap;

	UFUNCTION(BlueprintCallable, Category = "Hotbar")
		virtual void BindHotbarAbilityToID() {};
	UFUNCTION(BlueprintCallable, Category = "Hotbar")
		virtual void ExecuteSelectedAbility(FString abilityHashName, UUseMagic* magicObj) {};

		
};

// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../mythorChar/Magic/UseMagic.h"
#include "Inventory/UUseQuest.h"
#include "mythorChar/Weapons/UseWeapon.h"
#include "mythorChar/Weapons/BaseWeapon.h"
#include "PotatoGameInstance.h"
#include "PlayerInventory.generated.h"


UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAMEPROJECT_API UPlayerInventory : public UActorComponent 
{
	GENERATED_BODY()

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Sets default values for this component's properties
	UPlayerInventory();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Get Set Functions
	
	// GLOBAL GAME VARIABLES
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Global Game")
		UPotatoGameInstance* GameInst;

	//---------------------Base Stats---------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats") // What level determined by skill points (Mainly for player)
		int32 Level = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats") // Skill points (Mainly for player)
		int32 Skillpoints = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Leveling Stats")
		int32 vitality = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Leveling Stats")
		int32 haste = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Leveling Stats")
		int32 dexterity = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Leveling Stats")
		int32 intelligence = 1;

	// Level Up EXP
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float expPtsMax = 120.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float expPts = 0.f;

	// Currency
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Currency")
		int32 shungite_CUR = 0;

	//---------------------Magics---------------------
	// All collected Magics in inventory
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		TArray<UUseMagic*> InvMagicArry;

	// Equipped HOTBAR ITEMS (I know the name really sucks :p)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		TArray<UUseItem*> MagicArry;

	//---------------------Weapon Equips---------------------
	// All collected Weapons in inventory
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		TArray<UUseWeapon*> InvWeaponArry;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		TArray<UUseWeapon*> EquipWeaponArry;

	//---------------------Items---------------------
	// All collected Items in inventory
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		TArray<UUseItem*> InvItemArry;
	//*----------Getting Items----------*/
	UFUNCTION(BlueprintCallable, Category = "Items")
		void AddItemToInventory(UActorComponent* item);
		
	//---------------------Quests---------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quests")
		TArray<UUUseQuest*> QuestArry;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quests")
		UUUseQuest* TrackedQuest;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quests")
		UUserWidget* QuestTracker;

	// Character
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parent")
		ABaseCharacter* Character;

	UFUNCTION(BlueprintCallable)
		void PushInventoryToInstance();
};

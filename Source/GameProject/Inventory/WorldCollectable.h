// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "UseItem.h"
#include "Components/SphereComponent.h"
#include "mythorChar/BaseCharacter.h"
#include "Inventory/SavableCollectedObject.h"
#include "Components/ActorComponent.h"
#include "WorldCollectable.generated.h"

UCLASS()
class GAMEPROJECT_API AWorldCollectable : public ASavableCollectedObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWorldCollectable();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

public:
	// Collision Component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		USphereComponent* Collision;

	// Items to give
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items", meta = (ExposeOnSpawn = "true"))
		TMap<TSubclassOf<UUseItem>, int32> items;

	// Can this item be collected?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Items")
		bool m_CanCollectItem = true;

	// Collision Functions
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintImplementableEvent, Category = "Hit")
		void OnPlayerHit();

	UFUNCTION(BlueprintImplementableEvent, Category = "Item")
		void AddItemToCharacter_BP(UUseItem* item);

		virtual void ObjectCollected() override;

};

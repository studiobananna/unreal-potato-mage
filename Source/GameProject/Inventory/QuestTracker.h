// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UUseQuest.h"
#include "QuestTracker.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API UQuestTracker : public UUserWidget
{
	GENERATED_BODY()
private:

	
public:
	// Construction / Tick Functions
	UQuestTracker(const FObjectInitializer& ObjectInitializer);
	virtual void NativePreConstruct();
	virtual void NativeConstruct();
	virtual void NativeDestruct();
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime);

	// Vars
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quests")
		UUUseQuest* ActiveQuest;
};

// (C) Copyright Studio Bananna, all rights reserved


#include "PlayerInventory.h"
#include "GameProject/mythorChar/BaseCharacter.h"

// Sets default values for this component's properties
UPlayerInventory::UPlayerInventory() {
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	// ...

	// Initialize Magic Array and Wep Equip Array
	MagicArry.Init(nullptr, 6);
	EquipWeaponArry.Init(nullptr, 2);
}


// Called when the game starts
void UPlayerInventory::BeginPlay() {
	Super::BeginPlay();
	// ...
	
	// Get Character Owner
	Character = (ABaseCharacter*) GetOwner();
}


// Called every frame
void UPlayerInventory::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Calculate Leveling Up and execute necessary functions upon doing so...
	if (expPts >= expPtsMax) {
		expPtsMax *= 1.5f;
		Skillpoints += 1;

		if (Character) {
			Character->OnLevelUp();
		}
		expPts = 0.f;
	}
}

void UPlayerInventory::PushInventoryToInstance()
{
	if (Character) {
		//Get Game Instance when needed
		GameInst = Character->GameInst;

		if (GameInst) {
			// Copy Object Arrays & Stringify ALL objects to be saved

			// Inventory Items
			for (int i = 0; i < InvItemArry.Num(); i++) {
				if (InvItemArry[i]) {
					// Get the item count then store into a string
					FString ItemCount = FString::FromInt(InvItemArry[i]->ItemCount);
					// If less than 10, then fill in the tens place with 0
					if (InvItemArry[i]->ItemCount < 10) ItemCount = "0" + FString::FromInt(InvItemArry[i]->ItemCount);

					GameInst->PL_InvItemArry.Push(":"+ ItemCount + ":" + InvItemArry[i]->GetClass()->GetName());
					UE_LOG(LogTemp, Warning, TEXT("%s pushed to Game State!"), *FString(":" + ItemCount + ":" + InvItemArry[i]->GetClass()->GetName()));
				}
				else {
					InvItemArry[i] = nullptr;
				}
			}

			// Inventory Magic
			for (int i = 0; i < InvMagicArry.Num(); i++) {
				if (InvMagicArry[i]) {
					GameInst->PL_InvMagicArry.Push(InvMagicArry[i]->GetClass()->GetName());
					UE_LOG(LogTemp, Warning, TEXT("%s pushed to Game State!"), *FString(InvMagicArry[i]->GetClass()->GetName()));
				}
				else {
					InvMagicArry[i] = nullptr;
				}
			}

			// Magic Array Hotbar
			for (int i = 0; i < MagicArry.Num(); i++)
				if (MagicArry[i]) {
					// Check if this is a Magic
					if (MagicArry[i]->GetClass()->IsChildOf(UUseMagic::StaticClass())) {
						GameInst->PL_MagicArry.Push(MagicArry[i]->GetClass()->GetName());
						UE_LOG(LogTemp, Warning, TEXT("%s pushed to Hotbar Game State!"), *FString(MagicArry[i]->GetClass()->GetName()));
					}
					// If not then it's an item
					else {
						// Get the item count then store into a string
						FString ItemCount = FString::FromInt(MagicArry[i]->ItemCount);
						// If less than 10, then fill in the tens place with 0
						if (MagicArry[i]->ItemCount < 10) ItemCount = "0" + FString::FromInt(MagicArry[i]->ItemCount);

						GameInst->PL_MagicArry.Push(":" + ItemCount + ":" + MagicArry[i]->GetClass()->GetName());
						UE_LOG(LogTemp, Warning, TEXT("%s pushed to Hotbar Game State!"), *FString(":" + ItemCount + ":" + MagicArry[i]->GetClass()->GetName()));
					}
				}
				else {
					MagicArry[i] = nullptr;
					GameInst->PL_MagicArry.Push(":0:NULL");
				}

			// Inventory Weapon Array
			for (int i = 0; i < InvWeaponArry.Num(); i++)
				if (InvWeaponArry[i]) {
					GameInst->PL_InvWeaponArry.Push(InvWeaponArry[i]->GetClass()->GetName());
					UE_LOG(LogTemp, Warning, TEXT("%s pushed to Game State!"), *FString(InvWeaponArry[i]->GetClass()->GetName()));
				}
				else {
					InvWeaponArry[i] = nullptr;
				}

			// Equipped Weapons
			for (int i = 0; i < EquipWeaponArry.Num(); i++)
				if (EquipWeaponArry[i]) {
					GameInst->PL_EquipWeaponArry.Push(EquipWeaponArry[i]->GetClass()->GetName());
				}
				else {
					EquipWeaponArry[i] = nullptr;
					GameInst->PL_EquipWeaponArry.Push("NULL");
				}

			// Quests
			for (int i = 0; i < QuestArry.Num(); i++)
				if (QuestArry[i])
					GameInst->PL_QuestArry.Push(QuestArry[i]->GetClass()->GetName());
				else
					QuestArry[i] = nullptr;

			// Current Tracked Quest
			if (TrackedQuest)
				GameInst->PL_TrackedQuest = TrackedQuest->GetClass()->GetName();
		}
	}
}

void UPlayerInventory::AddItemToInventory(UActorComponent* item) {

	// Adding Items to Inventory from an Actor Component (MAKE SURE TO CONSTRUCT THE CLASS FIRST)
	if (item && Character) {
		/*-------------TO MAGIC ARRAY-------------*/
		if (item->GetClass()->IsChildOf(UUseMagic::StaticClass())) // To Magic Array if Subclass of Magic
		{
			UUseMagic* mag = (UUseMagic*)item; // Convert ActorComp to UserMagic
			bool foundClass = false;			// See if we found the class

			for (int i = 0; i < InvMagicArry.Num(); i++) {
				if (InvMagicArry[i])
				{
					if (InvMagicArry[i]->GetClass() == mag->GetClass())
					{
						foundClass = true;
						UE_LOG(LogTemp, Warning, TEXT("Item %s DESTROYED from already in Inventory!"), *FString(item->GetName()));
						item->ConditionalBeginDestroy();	// If FOUND then DESTROY UOBJECT
						break;
					}
				}
			}

			if (!foundClass) {	// If not found in array, then add it
				UE_LOG(LogTemp, Warning, TEXT("Item %s Added to Magic Inventory!"), *FString(item->GetName()));
				InvMagicArry.Add(mag);

				// Set Owner to this character
				mag->OwningCharacter = Character;
			}
		}
		/*-------------TO WEAPON ARRAY-------------*/
		 // To Weapon Array
		else if (item->GetClass()->IsChildOf(UUseWeapon::StaticClass())) {
			UUseWeapon* wep = (UUseWeapon*)item;	// Get the weapon
			bool foundClass = false;				// See if the class was found

			for (int i = 0; i < InvWeaponArry.Num(); i++) {
				if (InvWeaponArry[i]) {
					if (InvWeaponArry[i]->GetClass() == wep->GetClass()) {
						foundClass = true;
						UE_LOG(LogTemp, Warning, TEXT("Item %s DESTROYED from already in Inventory!"), *FString(item->GetName()));
						item->ConditionalBeginDestroy(); // If FOUND then DESTROY UOBJECT
						break;
					}
				}
			}

			if (!foundClass) {	// If not found in array, then add it
				UE_LOG(LogTemp, Warning, TEXT("Item %s Added to Weapon Inventory!"), *FString(item->GetName()));
				InvWeaponArry.Add(wep);

				// Set Owner to this character
				wep->OwningCharacter = Character;
			}
		}
		/*-------------TO ITEMS ARRAY-------------*/
		// To Item Array
		else if (item->GetClass()->IsChildOf(UUseItem::StaticClass())) {
			UUseItem* invItem = (UUseItem*)item;	// Get the item
			bool foundClass = false;				// See if the class was found

			for (int i = 0; i < InvItemArry.Num(); i++) {

				if (InvItemArry[i]) {
					if (InvItemArry[i]->GetClass() == invItem->GetClass()) {
						foundClass = true;
						UE_LOG(LogTemp, Warning, TEXT("Item %s DESTROYED from already in Inventory!"), *FString(item->GetName()));
						item->ConditionalBeginDestroy();	// If FOUND then DESTROY UOBJECT

						// Unequip to refresh
						InvItemArry[i]->OnUnequip();

						// Increase Count Stack Instead
						if (invItem->bCanStack)
						{
							InvItemArry[i]->ItemCount += invItem->ItemCount;
						}

						// Then reequip to refresh
						InvItemArry[i]->OnEquip();
						break;
					}
				}
			}

			if (!foundClass) {	// If not found in array, then add it
				UE_LOG(LogTemp, Warning, TEXT("Item %s Added to Weapon Inventory!"), *FString(item->GetName()));
				InvItemArry.Add(invItem);

				// Set Owner to this character
				invItem->OwningCharacter = Character;
			}
		}
	}
}
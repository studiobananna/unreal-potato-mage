// (C) Copyright Studio Bananna, all rights reserved


#include "Inventory/HotbarComponent.h"
#include "mythorChar/mythorChar.h"

// Sets default values for this component's properties
UHotbarComponent::UHotbarComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UHotbarComponent::BeginPlay()
{
	Super::BeginPlay();
	// Get Character
	Character = (ABaseCharacter*)GetOwner();

	if (Character) {
		Character->HotbarComponent = this;
	}
}

void UHotbarComponent::BeginDestroy()
{
	Super::BeginDestroy();
}


// Called every frame
void UHotbarComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UHotbarComponent::CycleHotbarRowUp() {
	if (hotbarArrayType == 1) {
		hotbarArrayType -= 1;
		hotbarArrayType = FMath::Clamp(hotbarArrayType, 0, 1);
	}
	else {
		hotbarArrayType = 1;
	}
}

void UHotbarComponent::CycleHotbarRowDown() {
	if (hotbarArrayType == 0) {
		hotbarArrayType += 1;
		hotbarArrayType = FMath::Clamp(hotbarArrayType, 0, 1);
	}
	else {
		hotbarArrayType = 0;
	}
}


// -----------START HERE WHEN LOOKING FOR MAGICS OR IMPLEMENTATION-----------
void UHotbarComponent::UseHotbarItemStart(int inputType) {

	if (inputType < GetOwnerCharacter()->Inventory->MagicArry.Num()) {
		if (GetOwnerCharacter()->Inventory->MagicArry[inputType]) {
			// First check and see if the hotbar item is a Magic
			if (GetOwnerCharacter()->Inventory->MagicArry[inputType]->GetClass()->IsChildOf(UUseMagic::StaticClass())) {
				//Print Magic we are using
				UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(GetOwnerCharacter()->Inventory->MagicArry[inputType]->HashName));


				//Do the magic (UNFORTUNATELY THERE IS NO SWITCH STATEMENT WITH STRINGS in c++ :( )
				FString magicHash = GetOwnerCharacter()->Inventory->MagicArry[inputType]->HashName;

				// Get our associated function from the string we pass into the map
				if (GetOwnerCharacter()->MagicAbilityComponent->toHotbarFunctionMap.Contains(magicHash)) {
					// Execute the selected ability's function action
					GetOwnerCharacter()->MagicAbilityComponent->ExecuteSelectedAbility
						(GetOwnerCharacter()->Inventory->MagicArry[inputType]->HashName, (UUseMagic*)GetOwnerCharacter()->Inventory->MagicArry[inputType]);
				}

			}
			// Then check and see if the hotbar item is an Item
			else if (GetOwnerCharacter()->Inventory->MagicArry[inputType]->GetClass()->IsChildOf(UUseItem::StaticClass()))
			{
				GetOwnerCharacter()->UsingItems(inputType); // Use the item
			}
		}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Hotbar Array Out of Bounds!"));
	}
}

void UHotbarComponent::UseHotbarItem000() {
	// Get Hotbar '0' Based off of Hotbar Array Type 
	UE_LOG(LogTemp, Warning, TEXT("Hotbar Item 0 Pressed!"));

	switch (hotbarArrayType) {
	case 0:UseHotbarItemStart(0); break;
	case 1:UseHotbarItemStart(3); break;
	default:
		UseHotbarItemStart(0);
	}
}

void UHotbarComponent::UseHotbarItem001() {
	// Get Hotbar '1' Based off of Hotbar Array Type 
	UE_LOG(LogTemp, Warning, TEXT("Hotbar Item 1 Pressed!"));

	switch (hotbarArrayType) {
	case 0:UseHotbarItemStart(1); break;
	case 1:UseHotbarItemStart(4); break;
	default:
		UseHotbarItemStart(1);
	}
}

void UHotbarComponent::UseHotbarItem002() {
	// Get Hotbar '2' Based off of Magic Array Type 
	UE_LOG(LogTemp, Warning, TEXT("Hotbar Item 2 Pressed!"));

	switch (hotbarArrayType) {
	case 0:UseHotbarItemStart(2); break;
	case 1:UseHotbarItemStart(5); break;
	default:
		UseHotbarItemStart(2);
	}
}

void UHotbarComponent::UseHotbarItem003() {
	// Get Hotbar '3' Based off of Magic Array Type 
	UE_LOG(LogTemp, Warning, TEXT("Hotbar Item 3 Pressed!"));

	switch (hotbarArrayType) {
	case 0:UseHotbarItemStart(3); break;
	case 1:UseHotbarItemStart(6); break;
	default:
		UseHotbarItemStart(3);
	}
}
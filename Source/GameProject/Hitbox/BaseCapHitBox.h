// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/CapsuleComponent.h"
#include "BaseCapHitBox.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API UBaseCapHitBox : public UCapsuleComponent
{
	GENERATED_BODY()
	
};

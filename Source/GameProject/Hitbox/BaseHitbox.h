// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/CapsuleComponent.h"
#include "Effects/HitboxEffect.h"
#include "StatusEffectComponent.h"
#include "BaseHitbox.generated.h"

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEPROJECT_API UBaseHitbox : public UCapsuleComponent, public IHitboxEffect
{
	GENERATED_BODY()

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Sets default values for this component's properties
	UBaseHitbox();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

public:
	//Stats
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitStats")
		float baseDamage = 10.f;
	UPROPERTY(BlueprintReadWrite, Category = "HitStats")
		float damage = baseDamage;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitStats")
		float armorDamage = 10.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitStats")
		float force = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitStats")
		float angle = 0.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitStats")
		float constantHitRate = 0.25f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitStats")
		AActor* parent = NULL;

	//Type of hit
	// Damage Over Time Hitbox
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitType")
		bool isConstant = false;
	// Only hits grounded targets
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitType")
		bool isGroundedOnly = false;

	//Damage Types
	// Physical Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitType")
		bool bIsPhysical = true;
	// Fire Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitType")
		bool bIsFire = false;
	// Magic Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitType")
		bool bIsMagic = false;
	// Dark Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitType")
		bool bIsDark = false;
	// Nature Damage
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HitType")
		bool bIsNature = false;

	//Status Ailment Tsubclass Type (OVERRIDED BY BASE ABILITY ARRAY)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox", meta = (ExposeOnSpawn = "true"))
		TArray<TSubclassOf<UStatusEffectComponent>> StatusEffects;

public:	

	//Get hit actors
	UPROPERTY()
		int32 AttackDealt = -1;
	UPROPERTY()
		TArray<int32> HitArry;
	// If the hitbox is already active
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacking")
		bool bIsActiveHit = false;
	// If we want to constantly clear the hit array (this will allow the hitbox to be always active for any victim as the hit array prevents damage taken until manually reset)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacking")
		bool bConstantClearHitArray = false;
	// Has a character parent
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parent")
		bool hasCharParent = false;
	UPROPERTY()
		FVector CharParentCoords;
	// If the hitbox is disabled entirely
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacking")
		bool bIsDisableHitbox = false;
	// If the attack is able to be dodged (e.g. from a dodge roll that gives invuln)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacking")
		bool bIsDodgeable = true;

	// If the hitbox can use status effects (OVERRIDED BY BASE ABILITY BOOLEAN)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacking")
		bool CanUseStatusEffects = true;

	//Attacks / Active Hitbox
	// The direction the hitbox will send the victim toward
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacking")
		FVector HitDirection;
	// The Faction of the hitbox (player is usually 0 and enemies are 1)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacking")
		int32 HitFaction = 0;

	//Attack types
	UFUNCTION(BlueprintCallable, Category = "Collisions")
		void ClearHitArry();

	//Get Set Functions for Vars
	UFUNCTION()
		float GetDamage() { return round(damage); }
	UFUNCTION()
		void SetDamage(float DamageAmount);
	UFUNCTION()
		float GetForce() { return force; }
	UFUNCTION()
		void SetForce(float ForceAmount);
	UFUNCTION()
		float GetBaseDamage() { return baseDamage; }
	UFUNCTION()
		void SetBaseDamage(float BaseAmount);
	UFUNCTION()
		float GetAngle() { return angle * 0.065f; }
	UFUNCTION()
		void SetAngle(float AngleAmount);
	UFUNCTION()
		bool isConst() { return isConstant; }

	//Parent functions
	UFUNCTION()
		FVector GetParentCoords();
	UFUNCTION()
		void SetParent(AActor* ParentActor);
	UFUNCTION()
		AActor* GetParent() { return parent; }
	UFUNCTION()
		void DoEffect();

		
};

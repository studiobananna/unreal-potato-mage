// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Sound/SoundCue.h"
#include "NiagaraFunctionLibrary.h"
#include "NiagaraComponent.h"
#include "StatusEffectComponent.generated.h"


UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAMEPROJECT_API UStatusEffectComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UStatusEffectComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Variables

	// The Higher the Chance, the less chance to proc (Set to 1 for guaranteed chance on hit)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		float Chance = 1.f;

	// The amount of resistance to ignore the status effect (1 = 100%, 0.5 = 50%)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		float ResistThreshold = 1.f;

	// Damage is damage it does in percentage of max health (So 0.0085 is 0.85% of max health per tick)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		float DamagePercent = 0.0085f;
	//Damage on PLAYER ONLY
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		float DamagePercentPlayer = 0.0160f;

	// Time between each effect tick/proc
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		float ProcTimeInterval = 1.f;

	// Maximum amt of ticks (Set to -1 for infinite ticks)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		float ProcTickMaxAmount = 10.f;

	// The current tick out of the max
	UPROPERTY()float ProcTickCurrentAmount = 10.f;

	// During ticks, set the chance to proc or not (Mainly for paralysis) (Set to 0 for guaranteed ticks)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		int ChanceToProcDuringTick = 0;

	// The effect can have a force as well for knockback (MAINLY FOR PARALYSIS)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		float HurtForce = 0.f;

	// For enemies with super armor, it can deteriorate it
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		float armorDamage = 0.f;

	// Character to do things on
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects", meta = (ExposeOnSpawn = "true"))
		class ABaseCharacter* Character = nullptr;

	// The Effect Type (SEE GetStatusChanceToProc() for the map of effects and to make new resistances)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		FString StatusType = "NONE";

	// The Effect Proc Sound
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		USoundCue* StatusSoundCue;
	// The Effect VFX
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		UNiagaraSystem* StatusProcVFX;
	// Spawn VFX
	UNiagaraComponent* NiagaraComp;

	// If the Debuff can stack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		bool bCanStack = false;

	UPROPERTY()
		bool bStatusActive = false;
	UPROPERTY()
		bool bisOnPlayer = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		UTexture2D* BuffIcon;

	// Per damage tick timer
	UPROPERTY()FTimerHandle TickTimerHandle;

	UFUNCTION(BlueprintCallable, Category = "Status Effect")
		void StatusProcDamageTick();
	UFUNCTION(BlueprintCallable, Category = "Status Effect")
		float GetStatusChanceToProc();
	UFUNCTION(BlueprintCallable, Category = "Status Effect")
		void ExecuteStatusEffect();
	UFUNCTION(BlueprintCallable, Category = "Status Effect")
		void RemoveStatusEffect();
		
};

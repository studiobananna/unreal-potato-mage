// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Hitbox/BaseHitbox.h"
#include "CrookedCrux_Hitbox.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API UCrookedCrux_Hitbox : public UBaseHitbox
{
	GENERATED_BODY()
	
};

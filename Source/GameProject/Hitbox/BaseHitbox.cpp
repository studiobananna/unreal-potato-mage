// (C) Copyright Studio Bananna, all rights reserved


#include "BaseHitbox.h"

// Sets default values for this component's properties
UBaseHitbox::UBaseHitbox()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
	InitCapsuleSize(22, 61);
}

void UBaseHitbox::ClearHitArry()
{
	HitArry.Empty();
}

// Called when the game starts
void UBaseHitbox::BeginPlay()
{
	Super::BeginPlay();
	parent = GetOwner();
	(!bIsActiveHit) ? SetCollisionEnabled(ECollisionEnabled::NoCollision) :
		SetCollisionEnabled(ECollisionEnabled::QueryOnly);
}


// Called every frame
void UBaseHitbox::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Turn the collision on or off
	(!bIsActiveHit) ? SetCollisionEnabled(ECollisionEnabled::NoCollision) :
		SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	// If we want to constantly clear the hit array
	if (bConstantClearHitArray && HitArry.Num() > 0) {
		ClearHitArry();
	}
}

FVector UBaseHitbox::GetParentCoords()
{
	if(!parent){ parent = GetOwner(); }

	if (!hasCharParent) {
		return parent->GetActorLocation();
	}
	else {
		return CharParentCoords;
	}
}

void UBaseHitbox::SetParent(AActor* ParentActor)
{parent = ParentActor;}

void UBaseHitbox::DoEffect()
{
	IHitboxEffect* TheInterface = Cast<IHitboxEffect>(GetOwner());
	if (TheInterface)
	{
		// Empty the hit array if there are elements when constant
		if (isConstant && HitArry.Num() != 0) {
			HitArry.Empty();
			SetDamage(GetBaseDamage());
		}

		// Tell Owner to Execute the Hit Effect and On Hit Functions to Parents
		TheInterface->Execute_SpawnHitEffect(GetOwner());
		TheInterface->Execute_OnHitEffect(GetOwner());
	}

}

void UBaseHitbox::SetDamage(float DamageAmount)
{damage = DamageAmount;}

void UBaseHitbox::SetForce(float ForceAmount)
{force = ForceAmount;}

void UBaseHitbox::SetBaseDamage(float BaseAmount)
{baseDamage = BaseAmount;}

void UBaseHitbox::SetAngle(float AngleAmount)
{angle = AngleAmount;}


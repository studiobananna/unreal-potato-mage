// (C) Copyright Studio Bananna, all rights reserved

#include "Hitbox/StatusEffectComponent.h"
#include "mythorChar/BaseCharacter.h"

// Sets default values for this component's properties
UStatusEffectComponent::UStatusEffectComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UStatusEffectComponent::BeginPlay()
{
	Super::BeginPlay();

	// Set maximum ticks
	ProcTickCurrentAmount = ProcTickMaxAmount;
}

void UStatusEffectComponent::ExecuteStatusEffect()
{
	if (Character) {
		UClass* itemClass = GetClass();
		int32 itemIndex = 0;

		// First check and see if this component exists on the character if stackable
		for (auto Item : Character->CharHitboxDamageComp->StatusEffects) {
			if (Item) {
				if (Item->IsA(GetClass()) && !bCanStack) {
					// Print Success
					UE_LOG(LogTemp, Warning, TEXT("%s Duplicate Found! And can't stack! Destroying..."), *FString(GetClass()->GetName()));
					DestroyComponent();
					return;
				}
			}
		}

		// Calculate the proc chance and see if the effect is even possible
		float procChance = GetStatusChanceToProc();

		// Check if possible
		if (procChance != -1) {
			// If chance succeed, then do status effect
			if (FMath::RandRange(1, FMath::RoundToInt(procChance)) == 1) {
				// Set Active
				bStatusActive = true;

				// Add to character status array
				Character->CharHitboxDamageComp->StatusEffects.Add(this);

				// Status Proc VFX
				if (StatusProcVFX) {
					if (NiagaraComp)
						NiagaraComp->DestroyComponent();

					NiagaraComp = UNiagaraFunctionLibrary::SpawnSystemAttached(StatusProcVFX, Character->GetRootComponent(), NAME_None, FVector(0.f), FRotator(0.f), EAttachLocation::Type::KeepRelativeOffset, true);
					NiagaraComp->SetRelativeScale3D(FVector(2.f));
				}

				// Print Success
				UE_LOG(LogTemp, Warning, TEXT("%s Proc'ed Successfully"), *FString(GetClass()->GetName()));
			}
			else {
				bStatusActive = false; // Didn't proc lol
			}
		}
		else {
			bStatusActive = false;	// Not Possible, resistance too high
		}
	}

	// See if owner is player
	if (Character)
		if (Character->IsPlayerControlled())
			bisOnPlayer = true;

	// Set Timer to Proc
	GetWorld()->GetTimerManager().SetTimer(TickTimerHandle, this, &UStatusEffectComponent::StatusProcDamageTick, ProcTimeInterval, true, 0.f);
}

void UStatusEffectComponent::RemoveStatusEffect()
{
	// Remove from character status array
	if (Character->CharHitboxDamageComp->StatusEffects.Find(this) != INDEX_NONE)
		Character->CharHitboxDamageComp->StatusEffects.Remove(this);

	DestroyComponent();
}


// Called every frame
void UStatusEffectComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UStatusEffectComponent::StatusProcDamageTick() 
{
	// Deal the tick dmg
	if (bStatusActive)
	{

		if (FMath::RandRange(0, ChanceToProcDuringTick) == 0)
		{
			if(DamagePercent > 0.f)
				if(!bisOnPlayer) // If damaging an enemys
					Character->CharHitboxDamageComp->DealDamage(Character->healthMax * DamagePercent, armorDamage, HurtForce, FVector(0.f, 0.f, 0.f));
				else // If damaging the player
					Character->CharHitboxDamageComp->DealDamage(Character->healthMax * DamagePercentPlayer, armorDamage, HurtForce, FVector(0.f, 0.f, 0.f));

			// Play Status Effect Proc Sound
			if (StatusSoundCue)
				UAudioComponent* AudioComponent = UGameplayStatics::SpawnSoundAtLocation(this, StatusSoundCue, Character->GetActorLocation(), FRotator::ZeroRotator,
					0.55f, 1.25f, 0.0f, nullptr, nullptr, true);

			// On Status Effect End
			if (ProcTickMaxAmount != -1) 
			{
				if (--ProcTickCurrentAmount <= 0 || Character->health <= 0.f)
				{
					// Clear Timer
					GetWorld()->GetTimerManager().ClearTimer(TickTimerHandle);
					UE_LOG(LogTemp, Warning, TEXT("%s Destroyed Tick Ended!"), *FString(GetClass()->GetName()));

					if (NiagaraComp)
						NiagaraComp->DestroyComponent();

					// Remove the effect
					RemoveStatusEffect();
				}
			}
		}
	}
	else
	{
		// If the Status is still not active when it ticks, then destroy it
		UE_LOG(LogTemp, Warning, TEXT("%s Destroyed Not Active!"), *FString(GetClass()->GetName()));
		DestroyComponent();
	}
}

float UStatusEffectComponent::GetStatusChanceToProc()
{
	TMap<FString, float> StatusMap;

	// Get the effects resistance type
	StatusMap.Add("NONE",		0.f);
	StatusMap.Add("POISON",		Character->CharHitboxDamageComp->poisonResist);
	StatusMap.Add("PARALYSIS",	Character->CharHitboxDamageComp->paralysisResist);
	StatusMap.Add("BURN",		Character->CharHitboxDamageComp->fireResist);
	StatusMap.Add("SLOW",		Character->CharHitboxDamageComp->slowResist);

	// Then get the status effect and calculate shit with it
	if (StatusMap.Contains(StatusType)) {
		float resistAmt = StatusMap[StatusType];

		// Completely Resist
		if (resistAmt >= ResistThreshold) {
			return -1;
		}
		// Calculate the chance to proc if not fully resist
		else {		
			return Chance / (1.f - FMath::Clamp(resistAmt, 0.f, 0.9f));
		}
	}

	// Log that the status has no resistances and return 1 to guarantee proc
	UE_LOG(LogTemp, Warning, TEXT("%s doesn't have any resistances!!"), *FString(StatusType));
	return 1;
}

// (C) Copyright Studio Bananna, all rights reserved


#include "CallbackComponent.h"
#include "UObject/Interface.h"

// Sets default values for this component's properties
UCallbackComponent::UCallbackComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;
}


void UCallbackComponent::BeginPlay()
{
	Super::BeginPlay();

}

void UCallbackComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

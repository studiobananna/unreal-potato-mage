// (C) Copyright Studio Bananna, all rights reserved


#include "MyPlayerController.h"

AMyPlayerController::AMyPlayerController()
{
	bIsUsingGamepad = false;
	bResetGamepadDetectionAfterNoInput = true;
	GamepadTimeout = 5.f;
}

bool AMyPlayerController::InputAxis(FKey Key, float Delta, float DeltaTime, int32 NumSamples, bool bGamepad)
{
	bool ret = Super::InputAxis(Key, Delta, DeltaTime, NumSamples, bGamepad);
	//UpdateGamepad(bGamepad);
	return ret;
}

bool AMyPlayerController::InputKey(FKey Key, EInputEvent EventType, float AmountDepressed, bool bGamepad)
{
	bool ret = Super::InputKey(Key, EventType, AmountDepressed, bGamepad);
	UpdateGamepad(bGamepad);
	return ret;
}
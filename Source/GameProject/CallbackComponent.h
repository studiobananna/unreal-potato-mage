// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "CallbackComponent.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnDamageDealt, float, damage, class ABaseCharacter*, attackerCharacter, class ABaseCharacter*, damagedCharacter);

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAMEPROJECT_API UCallbackComponent : public UActorComponent
{
	GENERATED_BODY()

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Sets default values for this component's properties
	UCallbackComponent();

	// == COMPONENT FOR VARIOUS CALLBACKS ==
	UPROPERTY(BlueprintAssignable, BlueprintReadWrite, Category = "Callbacks")
		FOnDamageDealt C_OnDamageDealt;

	UPROPERTY()
		int32 number = 69;
};
/**
 *
 */
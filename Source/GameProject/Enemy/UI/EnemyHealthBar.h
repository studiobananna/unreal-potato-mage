// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "Runtime/Engine/Classes/Kismet/KismetMathLibrary.h"
#include "Enemy/UI/EnemyHealthWidget.h"
#include "ConstructorHelpers.h"
#include "EnemyHealthBar.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API UEnemyHealthBar : public UWidgetComponent
{
	GENERATED_BODY()

public:
	UEnemyHealthBar();

	virtual void BeginPlay() override;

	//Get Widget Class for HUD
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HUDWidgetClass", Meta = (BlueprintProtected = "true"))
		TSubclassOf<class UEnemyHealthWidget> HUDWidgetClass;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category="CurrentWidget")
		UEnemyHealthWidget* CurrentWidget;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parent")
		class ABaseCharacter* ActorChar;

	UFUNCTION()
		void SetHealthBarParent();
};

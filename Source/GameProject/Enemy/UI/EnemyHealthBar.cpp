// (C) Copyright Studio Bananna, all rights reserved


#include "EnemyHealthBar.h"
#include "Blueprint/UserWidget.h"
#include "mythorChar/BaseCharacter.h"

UEnemyHealthBar::UEnemyHealthBar() {
	static ConstructorHelpers::FClassFinder<UUserWidget> WidgetBPClass(TEXT("/Game/UI/Enemies/BP_EnmHealth"));
	HUDWidgetClass = WidgetBPClass.Class;
	SetWidgetClass(HUDWidgetClass);
	SetWidgetSpace(EWidgetSpace::Screen);
	SetDrawSize(FVector2D(500.f, 500.f));
	SetPivot(FVector2D(0.5f, 0.5f));
}

void UEnemyHealthBar::BeginPlay()
{
	Super::BeginPlay();
}

void UEnemyHealthBar::SetHealthBarParent()
{
	if (HUDWidgetClass != nullptr) {
		UEnemyHealthWidget* HUDWidget = (UEnemyHealthWidget*)GetUserWidgetObject();
		HUDWidget->ParentActor = ActorChar;
	}
}

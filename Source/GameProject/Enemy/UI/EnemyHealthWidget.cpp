// (C) Copyright Studio Bananna, all rights reserved


#include "EnemyHealthWidget.h"

UEnemyHealthWidget::UEnemyHealthWidget(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {

}

void UEnemyHealthWidget::NativePreConstruct()
{
	Super::NativePreConstruct();
}

void UEnemyHealthWidget::NativeConstruct()
{
	Super::NativeConstruct();
}

void UEnemyHealthWidget::NativeDestruct()
{
	Super::NativeDestruct();
}

void UEnemyHealthWidget::NativeTick(const FGeometry & MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);
}

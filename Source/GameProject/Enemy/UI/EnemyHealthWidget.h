// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "EnemyHealthWidget.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API UEnemyHealthWidget : public UUserWidget
{
	GENERATED_BODY()
private:

public:
	// Construction / Tick Functions
	UEnemyHealthWidget(const FObjectInitializer& ObjectInitializer);
	virtual void NativePreConstruct();
	virtual void NativeConstruct();
	virtual void NativeDestruct();
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime);

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Parent")
		class ABaseCharacter* ParentActor;
};

// (C) Copyright Studio Bananna, all rights reserved


#include "EnmFloaty01.h"

AEnmFloaty01::AEnmFloaty01()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AEnmFloaty01::BeginPlay()
{
	Super::BeginPlay();
}

void AEnmFloaty01::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	if (isAttacking && GetCharacterMovement()->IsMovingOnGround()) {
		GetCharacterMovement()->AddImpulse(GetActorForwardVector() * DeltaTime * FMath::Pow(10, 5.5f));
	}
}

void AEnmFloaty01::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}

void AEnmFloaty01::AttackingStart_Implementation()
{
	//Attack();
}

void AEnmFloaty01::Attack()
{
	m_ActionState = 1;
	Attack001();
}

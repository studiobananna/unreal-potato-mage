// (C) Copyright Studio Bananna, all rights reserved


#include "EnmBossAlt.h"

AEnmBossAlt::AEnmBossAlt()
{
	PrimaryActorTick.bCanEverTick = true;
}

void AEnmBossAlt::ResetWepHitbox()
{
	if (Wep1 && Wep1->Hitbox->bIsActiveHit) {
		Wep1->Hitbox->HitArry.Empty();
	}
}

void AEnmBossAlt::BeginPlay()
{
	Super::BeginPlay();

	//Get Location to Init Spawn
	FVector Location = this->HitboxCapsule->GetComponentLocation();
	FRotator Rotation(0.0f, 0.0f, 0.0f);
	FActorSpawnParameters SpawnInfo;

	//Left Weapon
	Wep1 = GetWorld()->SpawnActor<ABaseWeapon>(WepSpawn, Location, Rotation, SpawnInfo);
	FName fnWeaponSocket01 = TEXT("Hand_LSocket");
	Wep1->AttachToComponent(this->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, fnWeaponSocket01);
	Wep1->SetParent(this);
	Wep1->Character = this;
}

void AEnmBossAlt::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (isAttacking) {
		GetCharacterMovement()->AddImpulse(GetActorForwardVector() * DeltaTime * FMath::Pow(10, 5.2f));
		//Weapon Attack Stat Modifiers
		if (Wep1) { Wep1->SetActiveHitbox(true); }
	}
	else {
		if (Wep1 && Wep1->Hitbox->bIsActiveHit) {
			Wep1->SetActiveHitbox(false);
			Wep1->Hitbox->HitArry.Empty();
		}
	}
}

void AEnmBossAlt::SetupPlayerInputComponent(UInputComponent * PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
}
// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Enemy/BaseEnemy.h"
#include "EnmFloaty01.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API AEnmFloaty01 : public ABaseEnemy
{
	GENERATED_BODY()
public:
	// Sets default values for this character's properties
	AEnmFloaty01();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Attacking Functions
	virtual void AttackingStart_Implementation() override;
	virtual void Attack() override;
	UFUNCTION(BlueprintImplementableEvent, Category = "Attacks")
		void Attack001();
	
};

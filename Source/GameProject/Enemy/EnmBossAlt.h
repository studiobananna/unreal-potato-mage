// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Enemy/BaseEnemy.h"
#include "mythorChar/Weapons/BaseWeapon.h"
#include "EnmBossAlt.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API AEnmBossAlt : public ABaseEnemy
{
	GENERATED_BODY()
public:
	// Sets default values for this character's properties
	AEnmBossAlt();

	//Weapon
	UPROPERTY(EditAnywhere)
		TSubclassOf<ABaseWeapon> WepSpawn;
	UPROPERTY(BlueprintReadOnly, Category = "Weapon")
		class ABaseWeapon* Wep1 = NULL;
	UFUNCTION(BlueprintCallable, Category = "Weapon")
		void ResetWepHitbox();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
};

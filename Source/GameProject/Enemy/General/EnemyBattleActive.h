// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Enemy/BaseEnemy.h"
#include "Inventory/SavableCollectedObject.h"
#include "EnemyBattleActive.generated.h"

UCLASS()
class GAMEPROJECT_API AEnemyBattleActive : public ASavableCollectedObject
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AEnemyBattleActive();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// The enemy array, all enemies need to be defeated in order for this event to deactivate (USED WIH BATTLE EVENT INTERFACE)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battle", meta = (ExposeOnSpawn = "true"))
		TArray<ABaseEnemy*> EnemyArray;

	// All of these Actors need to be interacted with in order for this event to deactivate (USED WITH BATTLE EVENT INTERFACE)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable", meta = (ExposeOnSpawn = "true"))
		TArray<AActor*> InteractableArray;

	// The spawn location of enemies (USED INTERNALLY)
	UPROPERTY(BlueprintReadWrite, Category = "Battle")
		TArray<FVector> LocationArray;

	// Used for Event Actors that respond to this event activating and deactivating
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battle", meta = (ExposeOnSpawn = "true"))
		TArray<AActor*> EventActors;

	// Used for battle events that are linked with this event that also want to deactivate
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Interactable", meta = (ExposeOnSpawn = "true"))
		TArray<AEnemyBattleActive*> DeactivateBattleEventArray;

	// Upon all enemies defeated
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Battle")
		void OnDefeatedEnemies();
};

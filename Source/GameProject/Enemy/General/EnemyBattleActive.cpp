// (C) Copyright Studio Bananna, all rights reserved


#include "EnemyBattleActive.h"

// Sets default values
AEnemyBattleActive::AEnemyBattleActive()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

}

// Called when the game starts or when spawned
void AEnemyBattleActive::BeginPlay()
{
	Super::BeginPlay();
	for (int i = 0; i < EnemyArray.Num(); i++) {
		if(i < EnemyArray.Num() && EnemyArray[i])
			LocationArray.Add(EnemyArray[i]->GetActorLocation());
	}
	
}

// Called every frame
void AEnemyBattleActive::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


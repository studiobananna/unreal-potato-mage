// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "mythorChar/BaseCharacter.h"
#include "Components/BoxComponent.h"
#include "BaseEnemy.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API ABaseEnemy : public ABaseCharacter
{
	GENERATED_BODY()
private:
	//Health Bar
		//UPROPERTY()
			//UEnemyHealthBar* HealthBar;

	UPROPERTY() FTimerHandle RespawnTimer;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Sets default values for this character's properties
	ABaseEnemy();

	// On Destroy
	UFUNCTION(BlueprintCallable, Category = "States")
		void OnDestroyed(bool bUseRealDestroyed = true);
	UFUNCTION(BlueprintImplementableEvent, Category = "States")
		void OnDestroyed_BP();

	UFUNCTION()
		void OnOverlapBeginInBounds(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEndInBounds(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY()
		AController* PawnController;

	UPROPERTY(EditAnywhere)
		UBoxComponent* InBoundsBox;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "States")
		FVector spawnLocation = FVector(0.f, 0.f, 0.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "States")
		FRotator spawnRotation = FRotator(0.f, 0.f, 0.f);
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "States", meta = (ExposeOnSpawn = "true"))
		bool isRespawn = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "States", meta = (ExposeOnSpawn = "true"))
		float RespawnCD = 120.f;

	UPROPERTY(BlueprintReadOnly, Category = "States")
		bool bisDead = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "States")
		float MatAlphaMax = 1.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "States", meta = (ExposeOnSpawn = "true"))
		bool bisActive = false;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Battle")
		class AEnemyBattleActive* eventBattleActor;

	UFUNCTION(BlueprintCallable, Category = "States")
		void SetActorActive(bool active, bool allowMove, AEnemyBattleActive* eventActor = NULL);

	UFUNCTION(BlueprintCallable, Category = "States")
		void RespawnEnemy();

	UFUNCTION(BlueprintCallable, Category = "States")
		void SetMatAlpha(float alpha);

	UFUNCTION(BlueprintImplementableEvent, Category = "States")
		void OnActivated();
	UFUNCTION(BlueprintImplementableEvent, Category = "States")
		void OnDeactivated();

	// ------------Defeat Drops----------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drops", meta = (ExposeOnSpawn = "true"))
		float expPtsDrop = 20.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drops", meta = (ExposeOnSpawn = "true"))
		TArray<TSubclassOf<AActor>> DropsArry;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Drops", meta = (ExposeOnSpawn = "true"))
		int32 numOfDrops = 1;

	UPROPERTY()
		TMap<FString, int32> m_shapePrimitivesCollision;
};

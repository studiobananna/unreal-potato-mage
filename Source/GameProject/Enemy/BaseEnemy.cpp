// (C) Copyright Studio Bananna, all rights reserved


#include "BaseEnemy.h"
#include "Kismet/GameplayStatics.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "Enemy/General/EnemyBattleActive.h"


ABaseEnemy::ABaseEnemy()
{
	PrimaryActorTick.bCanEverTick = true;
	//Movement
	GetCharacterMovement()->bUseSeparateBrakingFriction = true;
	GetCharacterMovement()->BrakingFrictionFactor = defaultFriction;
	GetCharacterMovement()->BrakingFriction = 4.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 1.f;

	// Stop Inventory Ticking
	Inventory->PrimaryComponentTick.bCanEverTick = false;

	// Sphere for being able to tick, when in bounds the actor is rendered and ticks, when not in bounds it does neither
	InBoundsBox = CreateDefaultSubobject<UBoxComponent>(TEXT("InBoundsBox"));
	InBoundsBox->InitBoxExtent(FVector(0.f, 0.f, 0.f));
	InBoundsBox->SetupAttachment(RootComponent);
	InBoundsBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	InBoundsBox->SetCollisionResponseToAllChannels(ECR_Ignore);
	InBoundsBox->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	InBoundsBox->SetCollisionObjectType(ECC_PhysicsBody);
	InBoundsBox->SetVisibility(false);
	InBoundsBox->OnComponentBeginOverlap.AddDynamic(this, &ABaseEnemy::OnOverlapBeginInBounds);
	InBoundsBox->OnComponentEndOverlap.AddDynamic(this, &ABaseEnemy::OnOverlapEndInBounds);
	InBoundsBox->SetCanEverAffectNavigation(false);

	//Faction (Not player's faction)
	Faction = 1;
}
void ABaseEnemy::BeginPlay()
{
	Super::BeginPlay();

	spawnLocation = GetActorLocation();
	spawnRotation = GetActorRotation();

	health = healthMax;

	if(CharDynamicMaterialComp)
		CharDynamicMaterialComp->matAlpha = 0.f;

	SetActorActive(bisActive, AllowMovement);

	if (CharDynamicMaterialComp)
		SetMatAlpha(CharDynamicMaterialComp->matAlpha);

	if (Target == NULL) {
		Target = (ABaseCharacter*)UGameplayStatics::GetPlayerCharacter(GetWorld(), 0);
	}

	// Set bounds box size
	if(InBoundsBox)
		InBoundsBox->SetBoxExtent(FVector(8000.f, 8000.f, 2500.f));
	// Get initial controller when spawning in
	if(GetController())
		PawnController = GetController();
}

void ABaseEnemy::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Moving on Ground or off ground settings
	if (!GetCharacterMovement()->IsMovingOnGround()) {
		GetCharacterMovement()->BrakingFriction = 2.f;
		GetCharacterMovement()->BrakingDecelerationWalking = 0.6f;
	}
	else {
		GetCharacterMovement()->BrakingFriction = 5.f;
		GetCharacterMovement()->BrakingDecelerationWalking = 1.f;
	}

	// Dead, can respawn mainly for overworld stuff
	if (health <= 0 && bCanDie) 
	{
		AllowMovement = false;
		CharDynamicMaterialComp->matAlpha -= 1.25f * DeltaTime;
		bisDead = true;
		SetMatAlpha(CharDynamicMaterialComp->matAlpha);

		if (CharDynamicMaterialComp->matAlpha <= 0 && isRespawn == false)		// If can't respawn then destroy
			OnDestroyed(true);
		else if (CharDynamicMaterialComp->matAlpha <= 0.02f && isRespawn == true && bisActive)	// If CAN respawn then set the timer to respawn
		{
			if (!GetWorld()->GetTimerManager().IsTimerActive(RespawnTimer)) {
				SetActorActive(false, false);
				OnDestroyed(false);
				GetWorld()->GetTimerManager().SetTimer(RespawnTimer, this, &ABaseEnemy::RespawnEnemy, RespawnCD, false);
			}
		}
	}

	// If active FADE IN OR OUT
	if (CharDynamicMaterialComp) {
		if (bisActive) {
			if (CharDynamicMaterialComp->matAlpha < MatAlphaMax) {
				CharDynamicMaterialComp->matAlpha += 0.5 * DeltaTime;
				SetMatAlpha(CharDynamicMaterialComp->matAlpha);
			}
		}
		else {
			if (CharDynamicMaterialComp->matAlpha > 0) {
				CharDynamicMaterialComp->matAlpha -= 0.5 * DeltaTime;
				SetMatAlpha(CharDynamicMaterialComp->matAlpha);

				// If close to being non-visible, then just hide and stop entirely
				if (CharDynamicMaterialComp->matAlpha <= 0.01f) {
					// Disable once faded
					SetActorActive(false, false);
				}
			}
		}
	}
}

void ABaseEnemy::SetActorActive(bool active, bool allowMove, AEnemyBattleActive* eventActor)
{
	AllowMovement = allowMove;

	if(eventActor) this->eventBattleActor = eventActor;

	// Get Array of our primitive components collision sets
	TArray<UShapeComponent*> Primitives;
	GetComponents<UShapeComponent>(Primitives, false);
	if (m_shapePrimitivesCollision.Num() == 0)
	{
		for (UShapeComponent* obj : Primitives)
		{
			m_shapePrimitivesCollision.Add(obj->GetName(), obj->GetCollisionEnabled());
		}
	}

	// If Actor is Active
	if (active) 
	{
		//if (GEngine) GEngine->AddOnScreenDebugMessage(69, 2.0f, FColor::Yellow, FString::Printf(TEXT("ACTOR: %s - Set Active", GetName())));
		SetActorHiddenInGame(false);
		SetActorTickEnabled(true);
		bisActive = true;
		GetCharacterMovement()->GravityScale = 3.f;
		OnActivated();

		// Set Collisions ENABLED
		// Get ALL Shape Components, and ENABLE them if there are any that exist BP's
		for (UShapeComponent* obj : Primitives)
		{
			// Set collision based on what's in the initial collision settings array
			obj->SetCollisionEnabled( (ECollisionEnabled::Type)m_shapePrimitivesCollision[obj->GetName()] );
		}
	}

	// If Actor is NOT active (ie out of range, not enabled yet...)
	else 
	{
		//if (GEngine) GEngine->AddOnScreenDebugMessage(69, 2.0f, FColor::Yellow, FString::Printf(TEXT("ACTOR: %s - Set NOT Active", GetName())));
		SetActorHiddenInGame(true);
		SetActorTickEnabled(false);
		bisActive = false;
		GetCharacterMovement()->GravityScale = 0.f;
		OnDeactivated();

		// Set Collisions DISABLED
		// Get ALL Shape Components, and DISABLE them if there are any that exist BP's
		for (UShapeComponent* obj : Primitives)
		{
			obj->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		}

		// Then set the In Bounds Component to Enable because the previous disabled all
		if(InBoundsBox)
			InBoundsBox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}

	// If the actor does not respawn then disable this box all-together
	if (InBoundsBox && !isRespawn)
		InBoundsBox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
}

void ABaseEnemy::RespawnEnemy()
{
	health = healthMax;
	SetActorLocation(spawnLocation + FVector(0.f, 0.f, 10.f));
	SetActorRotation(spawnRotation);
	SetActorActive(true, true);
	bisDead = false;
	// Display Text Debug
	UE_LOG(LogTemp, Warning, TEXT("%s Has Respawned!"), *FString(GetName()));
}

void ABaseEnemy::SetMatAlpha(float alpha)
{
	for (int i = 0; i < GetMesh()->GetMaterials().Num(); i++) {
		CharDynamicMaterialComp->matArray[i]->SetScalarParameterValue(TEXT("Opacity"), alpha);
	}
}

void ABaseEnemy::OnDestroyed(bool bUseRealDestroyed) {

	// Tell Player, the Enemy was Defeated
	ABaseCharacter* MyCharacter = Cast<ABaseCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));

	if (MyCharacter) {
		MyCharacter->OnDefeatDelegate.Broadcast(QuestTargName);
		MyCharacter->Inventory->expPts += expPtsDrop;
	}

	// Tell any event actors to remove self
	if (eventBattleActor) {
		for (int i = 0; i < eventBattleActor->EnemyArray.Num(); i++) {
			if (eventBattleActor->EnemyArray[i] == this) 
			{
				eventBattleActor->EnemyArray.RemoveAt(i);
				eventBattleActor->LocationArray.RemoveAt(i);
				if (eventBattleActor->InteractableArray.Num() == 0 && eventBattleActor->EnemyArray.Num() == 0) 
				{
					// Trigger the Ending Result of the Event Actor
					eventBattleActor->OnDefeatedEnemies();
				}
			}
		}
	}

	// Spawn any drops
	FActorSpawnParameters SpawnInfo;

	for (int j = 0; j < numOfDrops; j++) {
		int32 i = FMath::RandRange(0, DropsArry.Num() - 1);

		if (i < DropsArry.Num()) {
			if (DropsArry[i])
				GetWorld()->SpawnActor<AActor>(DropsArry[i], GetActorLocation(), FRotator(0.f, 0.f, 0.f), SpawnInfo);
		}
	}

	// Call BP Function
	OnDestroyed_BP();

	// Then destroy the object if can't respawn
	if (bUseRealDestroyed)
		Destroy();
}

void ABaseEnemy::OnOverlapBeginInBounds(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor == UGameplayStatics::GetPlayerPawn(this, 0) && OtherActor != this)
	{
		if(Target && health >= 1.f && isRespawn)
			SetActorActive(true, true);
	}
}

void ABaseEnemy::OnOverlapEndInBounds(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	if (OtherActor == UGameplayStatics::GetPlayerPawn(this, 0) && OtherActor != this)
	{
		if (isRespawn)
			bisActive = false;
	}
}

// (C) Copyright Studio Bananna, all rights reserved

#include "BaseCharacter.h"
#include "AnimationUtils.h"
#include "Kismet/KismetMathLibrary.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"
#include "Magic/UseMagic.h"
#include "mythorChar/Weapons/UseWeapon.h"
#include "Runtime/Engine/Classes/Engine/Engine.h"

/*-----------------Sets default values-----------------*/
ABaseCharacter::ABaseCharacter()
{
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Create Spring Arm (For Camera to be attached)
	SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("Spring Arm"));

	//Keep spring arm from rotating with the character
	SpringArm->bUsePawnControlRotation = false;
	SpringArm->SetUsingAbsoluteRotation(true);

	//define the distance between the character and camera 
	SpringArm->TargetArmLength = cameraZoom;

	//Attach the SpringArm to the RootComponent(Character)
	SpringArm->SetupAttachment(RootComponent);

	//Create Camera
	Camera = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));

	//Give it a larger FOV of 110 degrees
	Camera->FieldOfView = 110.f;

	//Attach Camera to Spring Arm
	Camera->SetupAttachment(SpringArm);

	//Set up our forward arrow component (Keeps forward vector/direction)
	ForwardDirection = CreateDefaultSubobject<UArrowComponent>(TEXT("Forward Direction"));
	//Attach the arrow to the root component
	ForwardDirection->SetupAttachment(RootComponent);

	//turn off collison for our raycasts on our mesh
	GetMesh()->SetCollisionResponseToChannel(ECollisionChannel::ECC_Destructible, ECollisionResponse::ECR_Ignore);

	//set default value for bIsFiring
	bIsFiring = false;

	//ensure our character is always facing the direction he's moving
	GetCharacterMovement()->bOrientRotationToMovement = true;
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = false;

	//Allows the character to walk up stairs
	GetCharacterMovement()->SetWalkableFloorAngle(50.f);
	GetCharacterMovement()->MaxStepHeight = 45.f;

	//Changes force mode to kg against physics objs (setting to true will make things kinda weird...)
	GetCharacterMovement()->bPushForceScaledToMass = false;

	//Movement Settings!!!
	//Speed and Accel
	GetCharacterMovement()->GroundFriction = 10;
	GetCharacterMovement()->MaxWalkSpeed = 700.f;
	GetCharacterMovement()->MaxAcceleration = 1200.f;
	//Friction
	GetCharacterMovement()->bUseSeparateBrakingFriction = true;
	GetCharacterMovement()->BrakingFrictionFactor = defaultFriction;
	GetCharacterMovement()->BrakingFriction = 15.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 1.f;
	//Rotations
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
	GetCharacterMovement()->RotationRate += FRotator(150, 150, 150);
	//Off Ground
	GetCharacterMovement()->GravityScale = 4.f;
	GetCharacterMovement()->JumpZVelocity = FMath::Pow(10, 3.05);

	//Collision Trigger Capsule----------------------------
	HitboxCapsule = CreateDefaultSubobject<UCapsuleComponent>(TEXT("HitboxCapsule"));
	HitboxCapsule->InitCapsuleSize(34.0f, 88.0f);
	HitboxCapsule->SetCollisionProfileName(TEXT("Trigger"));
	HitboxCapsule->SetRelativeLocation(FVector(0, 0, 0));
	HitboxCapsule->SetupAttachment(RootComponent);
	HitboxCapsule->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	HitboxCapsule->CanCharacterStepUp(false);

	//Setup Hitbox Collision Damage Component, handles anything damage related
	CharHitboxDamageComp = CreateDefaultSubobject<UCharHitboxCollisionComp>(TEXT("CharacterHitboxDamageComp"));
	CharHitboxDamageComp->SetOwnerCharacter(this);
	// ----->WHEN ADDING DYNAMICS, MAKE SURE TO PUT THEM IN THE CHAR HITBOX COLLISION COMPONENT<-----

	//Get Targetting Sphere----------------------------
	TargetSphere = CreateDefaultSubobject<USphereComponent>(TEXT("TargetSphere"));
	TargetSphere->InitSphereRadius(TargetRadius);
	TargetSphere->SetRelativeLocation(FVector(0, 0, 0));
	TargetSphere->SetCollisionProfileName(TEXT("Trigger"));
	TargetSphere->SetupAttachment(RootComponent);
	TargetSphere->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	TargetSphere->SetVisibility(false);
	TargetSphere->SetCollisionObjectType(ECollisionChannel::ECC_PhysicsBody);
	TargetSphere->bVisibleInRayTracing = false;
	TargetSphere->CanCharacterStepUp(false);
	TargetSphere->SetCanEverAffectNavigation(false);
	TargetSphere->SetCollisionResponseToChannel(ECC_WorldDynamic, ECR_Ignore);
	TargetSphere->SetCollisionResponseToChannel(ECC_WorldStatic, ECR_Ignore);

	//Setup Inventory
	// Set Inventory to InventoryComp to fix weird UE4 nullptr bug
	InventoryComp = CreateDefaultSubobject<UPlayerInventory>(TEXT("InventoryComponent"));
	Inventory = InventoryComp;

	//Setup Targetting Health Bars
	TargetHealthBar = CreateDefaultSubobject<UEnemyHealthBar>(TEXT("TargetHealthBar"));
	TargetHealthBar->SetupAttachment(RootComponent);

	// ---Armor Break Widget---
	static ConstructorHelpers::FClassFinder <UArmorBreakWidgetComponent>sArmorBreak (TEXT("WidgetBlueprint'/Game/UI/Enemies/BP_UIComponent_ArmorBreak'"));
	// Store its class
	ArmorBreakWidgetClass = sArmorBreak.Class;
	ArmorBreakWidget = Cast<UArmorBreakWidgetComponent>(CreateDefaultSubobject(TEXT("ArmorBreakWidget"), ArmorBreakWidgetClass, ArmorBreakWidgetClass, false, false));
	ArmorBreakWidget->SetupAttachment(RootComponent);

	//---Setup Actor Comp. Handler---
	CharActorCompHandler = CreateDefaultSubobject<UCharacterActorCompHandler>(TEXT("CharacterActorCompHandler"));

	//---Setup Material Dynamic Gen Comp.---
	CharDynamicMaterialComp = CreateDefaultSubobject<UCharDynamicMaterialGenComp>(TEXT("CharacterDynamicMaterialComp"));

	// ---Setup Actor Push Component---
	ActorPushComponent = CreateDefaultSubobject<UActorPushComponent>(TEXT("ActorPushComponent"));
	ActorPushComponent->TraceObjectTypes.Add(UEngineTypes::ConvertToObjectType(ECC_Pawn));			// Trace for pawns by default
	ActorPushComponent->bKeepInNavigation = true;
}

//Collision Overlap Events
void ABaseCharacter::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Display Text Debug
	//UE_LOG(LogTemp, Warning, TEXT("%s was overlapped!"), *FString(GetName()));
}
/*-----------------Called when the game starts or when spawned----------------*/
void ABaseCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Set Inventory to InventoryComp to fix weird UE4 nullptr bug
	Inventory = InventoryComp;

	// Set Character's Health Bar's Parent to this object
	if (TargetHealthBar) {
		TargetHealthBar->ActorChar = this;
		TargetHealthBar->SetHealthBarParent();
	}

	//Get Game Instance when spawned
	GameInst = (UPotatoGameInstance*)GetGameInstance();

	//Get Game Mode when spawned
	GameMode = (AGameProjectGameModeBase*)UGameplayStatics::GetGameMode(this);

	// Get level scale from game inst
	if (GameInst) {
		UpdateLevelScale(GameInst->LevelScale);
	}

	//Activate the camera on start
	Camera->Activate();

	// Set Default Checkpoint Coords
	LastCheckPointCoords = GetActorLocation();

	// Set Camera Rot
	SpringArm->SetWorldRotation(GetActorRotation());

	HitboxCapsule->SetCollisionEnabled(ECollisionEnabled::QueryOnly);

	// Load from game instance
	LoadCharacterFromInstance();

	// Set new health and mana from skillpoints
	if (UseHealthManaScale) {
		LoadSkillStats();
	}

	// PLAYER ONLY CHECKS
	if (IsPlayerControlled()) {
		// Save player coords occasionally (for falling...)
		GetWorld()->GetTimerManager().SetTimer(SaveCoordsTimerHandle, this,
			&ABaseCharacter::SaveLastCoords, 1.f, true, 0.f);

		// Game Inst checks
		if (GameInst) {
			// Set player respawn location
			GetWorld()->GetTimerManager().SetTimer(SetSpawnLocationHandle, this,
				&ABaseCharacter::SetSpawnPoint, 0.1f, false, 0.f);
		}

		// Set current scene in game inst as we spawn into a level
		if (GameInst) {
			if (GameInst->SET_newLevelSpawn) {
				GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Green, GameInst->SCENE_MAP_NAME);
				GameInst->SCENE_MAP_NAME = GetWorld()->GetMapName();
				GameInst->SCENE_MAP_NAME.RemoveFromStart(GetWorld()->StreamingLevelsPrefix);
			}
		}
	}

}

void ABaseCharacter::UpdateLevelScale( float scale )
{
	// level scale
	LevelScale = scale;

	// Set Level Scaling Factors
	healthMax *= LevelScale;
	health = healthMax;

	manaMax *= LevelScale;
	mana = manaMax;

	armorMax *= LevelScale;
	armor = armorMax;
}

void ABaseCharacter::CharacterStateTick(float DeltaTime)
{
	// For handling all character states
	if (GetCharacterMovement()->IsMovingOnGround()) {
		if (AllowMovement) {
			if (GetCharacterMovement()->Velocity.Size() > 0.f) {
				characterState = Status::Moving;
			}
			else if (GetCharacterMovement()->Velocity.Size() > 300.f) {
				characterState = Status::Running;
			}
			else {
				characterState = Status::Idle;
			}
		}
		else if(isAttacking) {
			characterState = Status::Attacking;
		}
		else if(bIsEventLocked) {
			characterState = Status::EventLock; // This is if we're forced in an event lock
		}
		else{
			characterState = Status::StuckIGuess; // This is if we just don't have control
		}
	}
	else {
		if(!isAttacking)
			characterState = Status::Aerial;
		else
			characterState = Status::AerialAttacking;
	}
}

// SAVE Variables to Game Instance for GENERAL PLAYER STATS
void ABaseCharacter::LevelChangeInst()
 {
	// SAVING
	if (GameInst && IsPlayerControlled() && bCanSaveStats) { // Check if this obj is controlled by the player and can save
		GameInst->PL_health       = health;			// Save all variables that need to be saved into the Game Instance
		GameInst->PL_mana         = mana;
		GameInst->PL_vitality     = Inventory->vitality;
		GameInst->PL_haste        = Inventory->haste;
		GameInst->PL_dexterity    = Inventory->dexterity;
		GameInst->PL_intelligence = Inventory->intelligence;
		GameInst->PL_Skillpoints  = Inventory->Skillpoints;
		GameInst->PL_Level		  = Inventory->Level;
		GameInst->PL_EXP		  = Inventory->expPts;
		GameInst->PL_EXP_MAX	  = Inventory->expPtsMax;
		GameInst->PL_shungite_CUR = Inventory->shungite_CUR;

		
		// Empty the saved Inventory Arrays
		GameInst->EmptySavedInventory();

		// Add Items in Inventory to GameInst
		Inventory->PushInventoryToInstance();

	}
}

// LOAD Variables from Game Instance for GENERAL PLAYER STATS
void ABaseCharacter::LoadCharacterFromInstance()
{
	// LOADING
	if (GameInst && IsPlayerControlled() && bCanSaveStats) { // Check if this obj is controlled by the player
		if (GameInst->PL_health > 0) health = GameInst->PL_health;
		if (GameInst->PL_mana > 0)   mana = GameInst->PL_mana;

		Inventory->vitality = GameInst->PL_vitality;
		Inventory->haste = GameInst->PL_haste;
		Inventory->dexterity = GameInst->PL_dexterity;
		Inventory->intelligence = GameInst->PL_intelligence;
		Inventory->Skillpoints = GameInst->PL_Skillpoints;
		Inventory->Level = GameInst->PL_Level;
		Inventory->expPts = GameInst->PL_EXP;
		Inventory->expPtsMax = GameInst->PL_EXP_MAX;
		Inventory->shungite_CUR = GameInst->PL_shungite_CUR;
	}
}

/*-----------------Called every frame----------------*/
void ABaseCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// Increase Target Sphere Radius when shown
	if(!IsHidden())
		if (TargetRadius < TargetMaxRadius) { TargetRadius+=100; TargetSphere->SetSphereRadius(TargetRadius); }

	// Clamp Health and Mana
	health = FMath::Clamp(health, 0.f, healthMax);
	mana = FMath::Clamp(mana, 0.f, manaMax);

	// Interp Rotation to Default after getting hit
	FRotator rot = GetMesh()->GetRelativeRotation();
	if (rot.Pitch != 0.f) {
		rot.Pitch = FMath::FInterpTo(rot.Pitch, 0.f, DeltaTime, 4.f);
		GetMesh()->SetRelativeRotation(rot);
	}

	// Rolling / Attacking Forces
	//First check friction so we don't launch the character
	if (GetCharacterMovement()->BrakingFriction != 15.f && GetCharacterMovement()->BrakingDecelerationWalking != 1.f) {
		GetCharacterMovement()->BrakingFriction = 15.f;
		GetCharacterMovement()->BrakingDecelerationWalking = 1.f;
	}

	// Then check for attack and rolling forces
	if (m_AllowAttackForce)
	{
		if (GetCharacterMovement()->BrakingFriction == 15.f && GetCharacterMovement()->BrakingDecelerationWalking == 1.f)
		{
			if (GetCharacterMovement()->IsMovingOnGround()) {
				GetCharacterMovement()->AddForce(GetActorForwardVector() * 2.5f * FMath::Pow(10, m_AttackForceFactor) * DeltaTime);
			}
		}
	}
	else if (m_AllowRollingForce)
	{
		if (GetCharacterMovement()->BrakingFriction == 15.f && GetCharacterMovement()->BrakingDecelerationWalking == 1.f)
		{
			if (GetCharacterMovement()->IsMovingOnGround()) //3.1f, 4.3f at 1.25f friction Player->isAttacking
			{
				if (isRolling) {
					GetCharacterMovement()->AddForce(GetActorForwardVector() * 2.5f * FMath::Pow(10, m_RollForceFactor) * DeltaTime);
				}
			}
			else {
				if (isRolling)
					GetCharacterMovement()->AddForce((GetActorUpVector() * 0.4f + GetActorForwardVector()) * 2.5f * FMath::Pow(10, m_AirRollForceFactor));
			}
		}
		else {
			GetCharacterMovement()->BrakingFriction = 15.f;
			GetCharacterMovement()->BrakingDecelerationWalking = 1.f;
		}
	}

	// -----------------PLAYER ONLY METHODS-----------------
	if (IsPlayerControlled()) {
		// Draw Player Coords
		if (CanDrawDebugVars) {
			if (GEngine) GEngine->AddOnScreenDebugMessage(69, 1.0f, FColor::Yellow, FString::Printf(TEXT("Position: <X:%f  Y:%f  Z:%f>"),
				GetActorLocation().X, GetActorLocation().Y, GetActorLocation().Z));
			if (GEngine) GEngine->AddOnScreenDebugMessage(70, 1.0f, FColor::Yellow, FString::Printf(TEXT("Rotation: <Roll:%f  Pitch:%f  Yaw:%f>"),
				GetActorRotation().Roll, GetActorRotation().Pitch, GetActorRotation().Yaw));
			if (GEngine) GEngine->AddOnScreenDebugMessage(71, 1.0f, FColor::Yellow, FString::Printf(TEXT("Scale:    <ScaleX:%f  ScaleY:%f  ScaleZ:%f>"),
				GetActorScale3D().X, GetActorScale3D().Y, GetActorScale3D().Z));

			// Action stuff
			if (GEngine) GEngine->AddOnScreenDebugMessage(72, 1.0f, FColor::Yellow, FString::Printf(TEXT("Is Attacking %b>"),
				isAttacking));
			if (GEngine) GEngine->AddOnScreenDebugMessage(73, 1.0f, FColor::Yellow, FString::Printf(TEXT("Is Rolling %b>"),
				isRolling));
			if (GEngine) GEngine->AddOnScreenDebugMessage(74, 1.0f, FColor::Yellow, FString::Printf(TEXT("Allow Movement %b>"),
				AllowMovement));
		}

		// Camera Zoom During and Out of combat
		if (bInCombat) {
			// Zoom In Combat
			if (SpringArm->TargetArmLength < combatZoom)
				SpringArm->TargetArmLength = FMath::FInterpTo(SpringArm->TargetArmLength, combatZoom, DeltaTime, 2.f);
		}
		else {
			// Zoom Out of Combat
			if (SpringArm->TargetArmLength > cameraZoom)
				SpringArm->TargetArmLength = FMath::FInterpTo(SpringArm->TargetArmLength, cameraZoom, DeltaTime, 2.f);
		}
	}

	// Handle Character States
	CharacterStateTick(DeltaTime);
}

/*-----------------Called to bind functionality to input----------------*/
void ABaseCharacter::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	//Bind action mappings
	InputComponent->BindAction("Jump", EInputEvent::IE_Pressed, this, &ABaseCharacter::Jump);
	InputComponent->BindAction("Attack", EInputEvent::IE_Pressed, this, &ABaseCharacter::AttackingStart_Implementation);

	//bind the axis mappings to our function
	InputComponent->BindAxis("ChangeCameraHeight", this, &ABaseCharacter::ChangeCameraHeight);
	InputComponent->BindAxis("RotateCamera", this, &ABaseCharacter::RotateCamera);
	InputComponent->BindAxis("MoveForward", this, &ABaseCharacter::MoveForward);
	InputComponent->BindAxis("MoveRight", this, &ABaseCharacter::MoveRight);
	//InputComponent->BindAxis("CameraZoom", this, &ABaseCharacter::CameraZoom);

	SetupPlayerInputHotbarComponent();

}

void ABaseCharacter::SetupPlayerInputHotbarComponent()
{
	// Setup Hotbar Inputs to component
	InputComponent->BindAction("HotbarArrayUp", EInputEvent::IE_Pressed, this, &ABaseCharacter::CycleHotbarRowUp);
	InputComponent->BindAction("HotbarArrayDown", EInputEvent::IE_Pressed, this, &ABaseCharacter::CycleHotbarRowDown);
	InputComponent->BindAction("Hotbar000", EInputEvent::IE_Pressed, this, &ABaseCharacter::UseHotbarItem000);
	InputComponent->BindAction("Hotbar001", EInputEvent::IE_Pressed, this, &ABaseCharacter::UseHotbarItem001);
	InputComponent->BindAction("Hotbar002", EInputEvent::IE_Pressed, this, &ABaseCharacter::UseHotbarItem002);
	InputComponent->BindAction("Hotbar003", EInputEvent::IE_Pressed, this, &ABaseCharacter::UseHotbarItem003);
}

void ABaseCharacter::SetSpawnPoint()
{
	if (GEngine) GEngine->AddOnScreenDebugMessage(69, 2.0f, FColor::Yellow, FString::Printf(TEXT("SPAWN POINT SET!")));
	GameInst->SCENE_PLAYER_LOCATION = GetActorLocation();
}

void ABaseCharacter::LoadSkillStats()
{
	//Calculate Stats based off of Inventory stats
	healthMax = 50.f + (15.f * Inventory->vitality);
	manaMax = 35.f + (7.f * Inventory->intelligence);

	//Load any skills stuff
	BP_LoadSkillStats();
}

/*-----------------Moving Forward----------------*/
void ABaseCharacter::MoveForward(float amount)
{
	//Get Direction from Camera
	Direction = (SpringArm->GetForwardVector() * FVector(1.f, 1.f, 0.f)).GetSafeNormal();

	//Ensure our character controller is valid and that our amount is not 0
	if (Controller && amount && AllowMovement) 
	{
		//add movement in the camera 'forward' direction
		AddMovementInput(Direction, amount);
	}
}

/*-----------------Moving Right----------------*/
void ABaseCharacter::MoveRight(float amount)
{
	//Get Direction from Camera
	Direction = SpringArm->GetRightVector();

	//Ensure our character controller is valid and that our amount is not 0
	if (Controller && amount && AllowMovement) 
	{
		//add movement in the camera 'right' direction
		AddMovementInput(Direction, amount);
	}
}

/*---------------------CAMERA---------------------*/
/**Rotate Camera Based on Mouse X**/
void ABaseCharacter::RotateCamera(float amount)
{
	//Ensure our character controller is valid and that our amount is not 0
	if (Controller && amount) {
		//Get current spring arm rotation as a vector
		FVector rot = SpringArm->GetComponentRotation().Euler();

		//add the amount to the vector
		rot += FVector(0, 0, amount * FMath::Pow(8, 2.f)) * GetWorld()->GetDeltaSeconds();

		//Apply the new rotation to the spring arm
		SpringArm->SetWorldRotation(FQuat::MakeFromEuler(rot));
	}
}

/**Rotate Camera Based on Mouse Y**/
void ABaseCharacter::ChangeCameraHeight(float amount)
{
	//Ensure our character controller is valid and that our amount is not 0
	if (Controller && amount) {
		//Get current rotation as vector
		FVector rot = SpringArm->GetComponentRotation().Euler();

		//separate the height out into its own variable
		float newHeight = rot.Y;
		//add the desired rotation
		newHeight += amount * FMath::Pow(8, 2.f) * GetWorld()->GetDeltaSeconds();
		//clamp the height to comfortable levels
		newHeight = FMath::Clamp(newHeight, -80.f, 20.f);

		//rebuild the rotation vector
		rot = FVector(0, newHeight, rot.Z);

		//apply the new rotation vector to the spring arm
		SpringArm->SetWorldRotation(FQuat::MakeFromEuler(rot));
	}
}

/**Zoom Camera**/
void ABaseCharacter::CameraZoom(float amount) {
	//Ensure our character controller is valid and that our amount is not 0
	if (Controller && amount) {
		SpringArm->TargetArmLength += amount*2;
	}
}

void ABaseCharacter::AttackingStart_Implementation() {
	AttackingStart();
}

/*-----------------Jumping Function----------------*/
void ABaseCharacter::Jump() {
	if (AllowMovement) {
		ACharacter::Jump();
	}
}

void ABaseCharacter::OnLevelUp() {
	OnLevelUp_BP();
}

void ABaseCharacter::Landed(const FHitResult& Hit) {
}

void ABaseCharacter::LoadLastCoords()
{
	// Set the coords
	TeleportTo(LastFootingCoords, LastFootingRotation, false, false);
	GetCharacterMovement()->Velocity = FVector(0.f, 0.f, 0.f);
}

void ABaseCharacter::SaveLastCoords()
{
	if (IsPlayerControlled() && !bDeathFalling) {
		// Save last coords when falling
		//Declare hit result
		FHitResult outHit;
		FVector start = GetActorLocation() + FVector(0.f, 0.f, 0.f);
		FVector end = GetActorLocation() + FVector(0.f, 0.f, -150.f);
		//do the raycast to check if landed object is valid
		GetWorld()->LineTraceSingleByObjectType(outHit, start, end, ECollisionChannel::ECC_WorldStatic);

		// Then save coords if we did
		if (outHit.bBlockingHit && GetCharacterMovement()->IsMovingOnGround()) {
			//Save Last Coords landed
			LastFootingCoords = GetActorLocation() + FVector(0.f, 0.f, 135.f);
			//Save Last Rotation landed
			LastFootingRotation = GetActorRotation();

			// Draw Last Valid Footing Coords
			if (CanDrawDebugVars) {
				FColor color = FColor::MakeRandomColor();
				DrawDebugLine(GetWorld(), start, outHit.ImpactPoint, color, true, 1.f, 0, 12.333);
			}
		}
	}
}

bool ABaseCharacter::GetInputEnabled() {
	return InputEnabled();
}


void ABaseCharacter::OnAttackHit(UBaseHitbox* Hitbox) 
{

}

void ABaseCharacter::CancelAction()
{
	CancelAction_BP();

	isAttacking = false;
	m_ActionState = 0;
	AllowMovement = true;
	bIsEventLocked = false;

	// Set default friction
	GetCharacterMovement()->BrakingFriction = 15.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 1.f;
	GetCharacterMovement()->BrakingFrictionFactor = defaultFriction;
}

void ABaseCharacter::CycleHotbarRowUp(){
	if (HotbarComponent)
		HotbarComponent->CycleHotbarRowUp();
}

void ABaseCharacter::CycleHotbarRowDown(){
	if (HotbarComponent)
		HotbarComponent->CycleHotbarRowDown();
}

void ABaseCharacter::UseHotbarItem000(){
	if (HotbarComponent)
		HotbarComponent->UseHotbarItem000();
}

void ABaseCharacter::UseHotbarItem001(){
	if (HotbarComponent)
		HotbarComponent->UseHotbarItem001();
}

void ABaseCharacter::UseHotbarItem002(){
	if (HotbarComponent)
		HotbarComponent->UseHotbarItem002();
}

void ABaseCharacter::UseHotbarItem003(){
	if (HotbarComponent)
		HotbarComponent->UseHotbarItem003();
}

// (C) Copyright Studio Bananna, all rights reserved


#include "mythorChar.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"

// Sets default values
AmythorChar::AmythorChar() {
 	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Movement Settings!!!
	//Speed and Accel
	GetCharacterMovement()->GroundFriction = 10;
	GetCharacterMovement()->MaxWalkSpeed = 700.f;
	GetCharacterMovement()->MaxAcceleration = 1200.f;
	//Friction
	defaultFriction = 1.75f;
	GetCharacterMovement()->bUseSeparateBrakingFriction = true;
	GetCharacterMovement()->BrakingFrictionFactor = defaultFriction;
	GetCharacterMovement()->BrakingFriction = 5.f;
	GetCharacterMovement()->BrakingDecelerationWalking = 1.f;
	//Rotations
	GetCharacterMovement()->bUseControllerDesiredRotation = true;
	GetCharacterMovement()->RotationRate = FRotator(150, CharacterBaseZRotationRate, 150);
	//Off Ground
	GetCharacterMovement()->GravityScale = 3.f;
	GetCharacterMovement()->JumpZVelocity = FMath::Pow(10, 3.05);
}

// Called when the game starts or when spawned
void AmythorChar::BeginPlay() {
	Super::BeginPlay();

	// Get game instance
	GameInst = (UPotatoGameInstance*)GetGameInstance();

	// ...
}

// LOAD Variables from Game Instance for MYTHOR PLAYER STATS
void AmythorChar::LoadCharacterFromInstance() {
	Super::LoadCharacterFromInstance();

	if (GameInst && UGameplayStatics::GetPlayerPawn(this, 0) == this) {
		// Assign game inst just incase it's not loaded in the component yet
		WepAttackingComponent->GameInst = GameInst;
		WepAttackingComponent->LoadCharacterSpawnWeapons();
	}
}

// SAVE Variables to Game Instance for MYTHOR PLAYER STATS
void AmythorChar::LevelChangeInst() {
	Super::LevelChangeInst();

	if (GameInst && UGameplayStatics::GetPlayerPawn(this, 0) == this) { // Check if this obj is controlled by the player
		GameInst->MYTH_wep = WepAttackingComponent->m_WeaponCycleSpawn;
	}
}

// SETTING SKILLS TREE STATS
void AmythorChar::LoadSkillStats() {
	Super::LoadSkillStats();
}

// Called every frame
void AmythorChar::Tick(float DeltaTime) {
	Super::Tick(DeltaTime);

	/*------------------------~ATTACKS AND AIR ATTACKS~------------------------*/
	/*------------------------------------------------------------------------*/
	//Set defaults when Not Attacking (Is Grounded)
	if (!isAttacking) {
		GetCharacterMovement()->GravityScale = 3.f;

		// Speed based friction
		if (!m_AllowAttackForce && !m_AllowRollingForce) // If no forced are applied
		{
			if (GetCharacterMovement()->Velocity.Size() > 200.f && Cast<UMythorMagicComponent>(MagicAbilityComponent)->MagicType == 0)
				GetCharacterMovement()->BrakingFrictionFactor = defaultFriction * 0.1f; // if above certain speed have less friction
			else
				GetCharacterMovement()->BrakingFrictionFactor = defaultFriction;		// if below then increase friction
		}
	}
	//If Attacking
	else {
		GetCharacterMovement()->BrakingFrictionFactor = defaultFriction;

		//Allow character to rotate when attacking /(DeltaTime * 100)
		if (AllowRot) {
			AddMovementInput(ForwardDirection->GetForwardVector(), 1);

			//If targeting
			(TargetLockOn && TargetLocked && !bIsEventLocked && !isRolling) ? // /(DeltaTime * 100)
				AddMovementInput((TargetLockOn->GetActorLocation() - GetActorLocation()), 0.06f) : TargetLocked = TargetLocked;
		}
	}

	//For Air Movement (NOT Grounded)
	if (!GetCharacterMovement()->IsMovingOnGround()) {
		//Keep in Air for attacks
		if (WepAttackingComponent->m_AttackGrav) {
			GetCharacterMovement()->AddForce(GetActorUpVector() * 1 * FMath::Pow(10, 4.9f));
		}
		if (isAttacking && !InHurtAnim) {
			GetCharacterMovement()->AddForce(GetActorForwardVector() * 1 * FMath::Pow(10, 5.2f));
		}

		// Slow down when hit away so you don't fly so far
		if (InHurtAnim) 
			FMath::VInterpConstantTo(GetCharacterMovement()->Velocity, FVector(0.f, 0.f, GetCharacterMovement()->Velocity.Z), DeltaTime, 3.f);

		//Friction Reduced
		GetCharacterMovement()->BrakingFriction = 1.f;
		GetCharacterMovement()->BrakingDecelerationWalking = 0.2f;
		GetCharacterMovement()->BrakingFrictionFactor = defaultFriction;
	}
	//else {
		// Default Friction
		//GetCharacterMovement()->BrakingFriction = 15.f;
		//GetCharacterMovement()->BrakingDecelerationWalking = 1.f;
	//}

	/*-----------------------------~WEAPON COMBO METERS~------------------------------*/

	if (GameInst->PL_B_UNLOCKED_SKILLS) {
		(m_ComboMeter > 0.f) ? m_ComboMeter -= 2.f * DeltaTime : m_ComboMeter = 0.f;
		if (m_InfiniteCombo) m_ComboMeter += 50.f * DeltaTime; // Infinite Meter

		m_ComboMeter = FMath::Clamp(m_ComboMeter, 0.f, 100.f);
	}
	else {
		m_ComboMeter = 0;
	}

	/*-----------------------------~MOVEMENT~------------------------------*/
	// Locked Controls / No Input
	if (bIsEventLocked) {
		AllowMovement = false;
		isAttacking = true;
	}

	//*-----------------------------DEBUG-----------------------------*/
	if (CanDrawDebugVars) {
		if (GEngine) GEngine->AddOnScreenDebugMessage(430, 1.0f, FColor::Yellow, FString::Printf(TEXT("< VELOCITY SIZE: %f >"),
			GetCharacterMovement()->Velocity.Size()));
		if (GEngine) GEngine->AddOnScreenDebugMessage(420, 1.0f, FColor::Yellow, 
			FString::Printf(TEXT("< ROATION RATE YAW(Z): %f >"), GetCharacterMovement()->RotationRate.Yaw));
	}

	// Turning tilt
	if (!bIsEventLocked || !GetController()->bBlockInput) 
	{
		if (GetInputAxisValue("MoveRight") != 0.f)
			MoveLeanPercent = FMath::FInterpTo(MoveLeanPercent, MaxLeanAmount * GetInputAxisValue("MoveRight") * GetInputAxisValue("MoveForward"), DeltaTime, 4.f);
		else
			if (MoveLeanPercent != 0)
				MoveLeanPercent = FMath::FInterpTo(MoveLeanPercent, 0.f, DeltaTime, 10.f);
	}

}

// Called to bind functionality to input
void AmythorChar::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent) {
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	//InputComponent->BindAction("DrawWeapon000", EInputEvent::IE_Pressed, this, &AmythorChar::SpawnWeapon001);
	//InputComponent->BindAction("DrawWeapon001", EInputEvent::IE_Pressed, this, &AmythorChar::SpawnWeapon002);
	InputComponent->BindAction("CycleWep",		EInputEvent::IE_Pressed, this, &AmythorChar::CycleWeapons);
	InputComponent->BindAction("AttackSP",		EInputEvent::IE_Pressed, this, &AmythorChar::SpecialAttack_Input);
	InputComponent->BindAction("DodgeRoll",		EInputEvent::IE_Pressed, this, &AmythorChar::DodgeRoll);
}

void AmythorChar::CancelAction() {
	Super::CancelAction();
	isRolling = false;

	if (WepAttackingComponent) {
		WepAttackingComponent->m_AttackType = 0;
		WepAttackingComponent->m_AttackQueue = 0;
		WepAttackingComponent->ResetWeaponToDefaults();
		WepAttackingComponent->SetWepIfAttacking(false, false);
		WepAttackingComponent->m_AttackComboType = 1;
	}

	if (WepSpecialAttackComponent) {
		WepSpecialAttackComponent->SPAttackType = 0;
	}

	if (MagicAbilityComponent){
		Cast<UMythorMagicComponent>(MagicAbilityComponent)->MagicType = 0;
		Cast<UMythorMagicComponent>(MagicAbilityComponent)->isCasting = false;

		// Cancel Magic Casts
		GetWorldTimerManager().ClearTimer(Cast<UMythorMagicComponent>(MagicAbilityComponent)->PotatoTimerHandle);
}
}

/*-----------------Moving Forward----------------*/
void AmythorChar::MoveForward(float amount) {
	//Get Direction from Camera
	Direction = (SpringArm->GetForwardVector() * FVector(1.f, 1.f, 0.f)).GetSafeNormal();

	//Ensure our character controller is valid and that our amount is not 0
	if (Controller && amount && AllowMovement) {
		//add movement in the camera 'forward' direction
		AddMovementInput(Direction, amount);
	}
	(AllowRot) ? AddMovementInput(Direction, amount * 0.03) : amount = amount;
}

/*-----------------Moving Right----------------*/
void AmythorChar::MoveRight(float amount) {
	//Get Direction from Camera
	Direction = SpringArm->GetRightVector();

	//Ensure our character controller is valid and that our amount is not 0
	if (Controller && amount && AllowMovement) {
		//add movement in the camera 'right' direction
		AddMovementInput(Direction, amount);
	}
	(AllowRot) ? AddMovementInput(Direction, amount * 0.03) : amount = amount;
}

void AmythorChar::AttackingStart_Implementation() {
	WepAttackingComponent->AttackingStart();
}

void AmythorChar::OnAttackHit(UBaseHitbox* Hitbox) {
	Super::OnAttackHit(Hitbox);
	WepAttackingComponent->OnAttackHit(Hitbox);
}

void AmythorChar::DodgeRoll() {
	if (!isRolling &&!isAttacking && !InHurtAnim)
	{
		CancelAction();
		isRolling = true;
		isAttacking = true;
		GetCharacterMovement()->RotationRate = FRotator(-1, -1, -1);
		//if (!InHurtAnim) GetCharacterMovement()->Velocity = FVector(0.f, 0.f, 0.f);
		DodgeRoll_BP();
		AddMovementInput(GetActorForwardVector(), 1);

		//Move weapons to sheathe
		WepAttackingComponent->MoveWeapon(false);
	}
	else if (Cast<UMythorMagicComponent>(MagicAbilityComponent)->isCasting) {
		// CANCEL Casting
		CancelAction();
	}
}

// ----------------------------Items----------------------------
void AmythorChar::UsingItems(int itemIndex) {
	//Store item
	UUseItem* item = Inventory->MagicArry[itemIndex];

	// Check Item Cooldown
	if (item->cooldown <= 0 && item->bisUseable)
	{
		if (!isAttacking && Cast<UMythorMagicComponent>(MagicAbilityComponent)->MagicType == 0 
			&& GetCharacterMovement()->IsMovingOnGround()) {
			//Stop All Attacks
			CancelAction();
			WepAttackingComponent->MoveWeapon(false);

			//Set is attacking and disable movement
			AllowRot = false;
			isAttacking = true;
			AllowMovement = false;
			Cast<UMythorMagicComponent>(MagicAbilityComponent)->MagicType = 1337;

			//Set Cooldown to a small amt
			item->cooldown = item->cooldownMax;

			//Stop Current Velocity
			FVector Vel = GetCharacterMovement()->Velocity;
			GetCharacterMovement()->Velocity = FVector(Vel.X * 0.0f, Vel.Y * 0.0f, Vel.Z);

			//Print Item we are using
			UE_LOG(LogTemp, Warning, TEXT("%s"), *FString(item->HashName));

			// Set the item we are going to use
			ItemToUse = item;

			//Use the item
			UsingItems_BP(item);
		}
	}
	else if (item->cooldown > 0) {
		HotbarComponent->OnCoolDown();
	}

}

void AmythorChar::CycleWeapons() {
	// Special Attack Input
	WepAttackingComponent->CycleWeapons();
}


void AmythorChar::SpecialAttack_Input() {
	// Special Attack Input
	WepSpecialAttackComponent->SpecialAttack_Start();
}


void AmythorChar::Landed(const FHitResult & Hit) {
	Super::Landed(Hit);
	//Magic
	Cast<UMythorMagicComponent>(MagicAbilityComponent)->ResetMagicVariables();

	// Special Attacks
	WepSpecialAttackComponent->ResetWeaponSpecialVars();

	// If attacking or rolling, then stop any movement
	if (isAttacking || isRolling) {GetCharacterMovement()->Velocity = FVector(0.f, 0.f, 0.f);}

	// Cancel attacks but not magic
	if (WepAttackingComponent->m_AttackType != 12 && !isRolling && 
		Cast<UMythorMagicComponent>(MagicAbilityComponent)->MagicType == 0 && !InHurtAnim) {
		//Cancel Attack
		CancelAction();
	}

	Landed_BP();
}

void AmythorChar::Jump() {
	if (AllowMovement && !isRolling && GetCharacterMovement()->IsMovingOnGround()) {
		ACharacter::Jump();
		//CancelAttack();

		//Move weapons to sheathe
		WepAttackingComponent->MoveWeapon(false);
	}
}
// (C) Copyright Studio Bananna, all rights reserved


#include "MythorWepSpecialAttackComponent.h"
#include "../mythorChar.h"

// Sets default values for this component's properties
UMythorWepSpecialAttackComponent::UMythorWepSpecialAttackComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMythorWepSpecialAttackComponent::BeginPlay()
{
	Super::BeginPlay();

	// Get Character
	Character = (AmythorChar*)GetOwner();

	if (Character) {
		Character->WepSpecialAttackComponent = this;
	}
	
}


// Called every frame
void UMythorWepSpecialAttackComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// Weapon Cooldowns (Might move this later)
	daggerCD > 0.f ? daggerCD -= (1.f * DeltaTime) : daggerCD = 0.f;
	GreatCD > 0.f ? GreatCD -= (1.f * DeltaTime) : GreatCD = 0.f;

	/*-----------------------------~SPECIAL ATTACKS~------------------------------*/
	switch (SPAttackType) {
		case 1: SPDagger_MistStep_Tick(DeltaTime); break;
		case 2: SPGreatsword_SpinCyclone_Tick(DeltaTime); break;
		default: break;
	}

	// Material Fade Effect Undo
	if ((SPAttackType == 0 || stopFadeEffect) && Character->CharDynamicMaterialComp->matAlpha < 1) {
		Character->CharDynamicMaterialComp->matAlpha += 1.9f * DeltaTime;
		Character->WepAttackingComponent->SetWepOpacity(Character->CharDynamicMaterialComp->matAlpha);
	}

	// ...
}

void UMythorWepSpecialAttackComponent::ResetWeaponSpecialVars() {
	// Special Attacks
	daggerFrcAmt = 1.25f;
	greatFrcAmt = 1.25f;
}

bool UMythorWepSpecialAttackComponent::CheckIfSpecialAttackUseable()
{
	if (SPAttackType != 0) return false;
	float givenCost = 0.f, givenCD = 0.f;

	switch (Character->WepAttackingComponent->m_Wep1->WeaponType) {
		case 1:
			givenCost = daggerSPCost;
			givenCD = daggerCD;
			break;
		case 2:
			givenCost = GreatSPCost;
			givenCD = GreatCD;
			break;
		default:
			givenCost = daggerSPCost;
			givenCD = daggerCD;
			break;
	}

	if (Character->mana >= givenCost && givenCD <= 0) {
		return true;
	}
	else if(Character->mana < givenCost) {
		Character->HotbarComponent->NotEnoughMana();
		return false;
	}
	else if (givenCD > 0) {
		Character->HotbarComponent->OnCoolDown();
		return false;
	}

	return false;
}

//Special Attacks
//
//
void UMythorWepSpecialAttackComponent::SpecialAttack_Start() {
	// First see if we unlocked them first
	if (!Character->GameInst->PL_B_UNLOCKED_SKILLS) return;

	//Start Attack
	if (Character->isAttacking || !Character->WepAttackingComponent->m_Wep1 || Character->InHurtAnim) return;
	
	// Stop fade effects
	stopFadeEffect = false;

	//In Battle
	Character->WepAttackingComponent->m_bisBattle = true;
	Character->BattleStance();

	//Move Weapons to hands
	Character->WepAttackingComponent->MoveWeapon(true);

	//Stop All Attacks
	Character->CancelAction();

	UE_LOG(LogTemp, Warning, TEXT("Special attack type %d"), Character->WepAttackingComponent->m_Wep1->WeaponType);

	//-------------------------Execute Special Attack-------------------------
	switch (Character->WepAttackingComponent->m_Wep1->WeaponType) {
		case 1: SpecialAttackDagger_MistStep(); break;
		case 2: SpecialAttackGreatsword_SpinCyclone(); break;
		default: break;
	}
}

void UMythorWepSpecialAttackComponent::SpecialAttackDagger_MistStep()
{
	// Check if usable
	if(!CheckIfSpecialAttackUseable()) return;

	//Set Cooldown
	daggerCD = daggerCDMax;

	//Set is attacking and disable movement
	Character->AllowRot = true;
	Character->isAttacking = true;
	Character->AllowMovement = false;
	Character->WepAttackingComponent->m_AttackType = 0;
	SPAttackType = 1;

	//Stop Current Velocity
	FVector Vel = Character->GetCharacterMovement()->Velocity;
	Character->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.05f, Vel.Y * 0.05f, Vel.Z * 0.f);

	//Blueprint Counterpart
	DaggerMistStepSP_BP();

	//Reduce Mana
	Character->mana -= daggerSPCost;

	//Set movement
	daggerLaunch = true;

	if (Character->GetCharacterMovement()->IsMovingOnGround()) {
		//Weapon Attack Stat Modifiers
		float DamageMod = 2.f; float ForceMod = 10.f; float Angle = 2.f;
		Character->WepAttackingComponent->SetAttackStats(DamageMod, DamageMod * 0.5, ForceMod, ForceMod * 0, Angle);
		bDaggerGround = true;
	}
	else if (!Character->GetCharacterMovement()->IsMovingOnGround()) {
		//Weapon Attack Stat Modifiers
		float DamageMod = 0.7f; float ForceMod = 5.5f; float Angle = 9.f;
		Character->WepAttackingComponent->SetAttackStats(DamageMod, DamageMod * 0.5, ForceMod, ForceMod, Angle);
		//Set Force Penalty
		daggerFrcAmt = FMath::Max(daggerFrcAmt - 0.25f, 0.f);
		bDaggerGround = false;
	}

	//Print
	UE_LOG(LogTemp, Warning, TEXT("You Special Attacked!"));
}

void UMythorWepSpecialAttackComponent::SPDagger_MistStep_Tick(float DeltaTime)
{
	//Check Special Dagger Attack
	if (SPAttackType == 1 && daggerLaunch && !Character->bIsEventLocked) {
		// Launch only if friction is defaulted
		if (Character->GetCharacterMovement()->BrakingFrictionFactor == Character->defaultFriction) {
			//Set Friction
			Character->GetCharacterMovement()->BrakingFriction = 5.f;
			Character->GetCharacterMovement()->BrakingDecelerationWalking = 1.f;
			Character->GetCharacterMovement()->GravityScale = 1.f;

			//Add Impulses
			Character->GetCharacterMovement()->IsMovingOnGround() ||
				bDaggerGround ? Character->GetCharacterMovement()->AddForce(Character->GetActorForwardVector() * FMath::Pow(10, 6.4f)) : // Grounded

				Character->GetCharacterMovement()->AddForce((Character->GetActorUpVector() + Character->GetActorForwardVector())  // Aerial
					* daggerFrcAmt * FMath::Pow(10, 5.65f));
		}

		// Material Fade Effect
		if (Character->CharDynamicMaterialComp->matAlpha > 0.f) {
			Character->CharDynamicMaterialComp->matAlpha -= 1.9f * DeltaTime;
			Character->WepAttackingComponent->SetWepOpacity(Character->CharDynamicMaterialComp->matAlpha);
		}
	}
}

void UMythorWepSpecialAttackComponent::SpecialAttackGreatsword_SpinCyclone()
{
	// Check if usable
	if (!CheckIfSpecialAttackUseable()) return;

	//Set Cooldown
	GreatCD = GreatCDMax;

	//Set is attacking and disable movement
	Character->AllowRot = true;
	Character->isAttacking = true;
	Character->AllowMovement = false;
	SPAttackType = 2;
	Character->WepAttackingComponent->m_AttackType = 0;

	//Stop Current Velocity and add Impulse
	FVector Vel = Character->GetCharacterMovement()->Velocity;
	Character->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.01f, Vel.Y * 0.01f, Vel.Z * 0.f);
	Character->GetCharacterMovement()->AddForce((Character->GetActorUpVector() + Character->GetActorForwardVector())
		* greatFrcAmt * FMath::Pow(10, 5.8f));
	greatFrcAmt -= 0.25;

	//Blueprint Counterpart
	GreatSwordCycloneSP_BP();

	//Reduce Mana
	Character->mana -= GreatSPCost;

	//Weapon Attack Stat Modifiers
	float DamageMod = 4.5f; float ForceMod = 10.f; float Angle = 6.f;
	Character->WepAttackingComponent->SetAttackStats(DamageMod, DamageMod * 0.5, ForceMod, ForceMod * 0, Angle);

	//Set Timer
	GSLaunch = true;
	GetWorld()->GetTimerManager().SetTimer(GreatSpTimerHandle, this, &UMythorWepSpecialAttackComponent::GreatSpecialFalse, 0.25f, false);

	//Print
	UE_LOG(LogTemp, Warning, TEXT("You Special Attacked!"));
}

void UMythorWepSpecialAttackComponent::SPGreatsword_SpinCyclone_Tick(float DeltaTime)
{
	// Special GS Attack
	if (!GSLaunch || SPAttackType != 2 || Character->bIsEventLocked) return;

	//Set Velocity Constant
	Character->GetCharacterMovement()->AddForce((Character->GetActorUpVector() * 1.55 + Character->GetActorForwardVector() * 1.5)
		* greatFrcAmt * FMath::Pow(10, 5.6f));
	Character->AllowRot = true;
	Character->isAttacking = true;
}

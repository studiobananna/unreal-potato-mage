// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../../Inventory/CharHotbarAbilityComponent.h"
#include "MythorMagicComponent.generated.h"

UENUM(BlueprintType)
enum EMagic{
	NoneMagic		UMETA(DisplayName = "NADA"),
	Ember			UMETA(DisplayName = "FIRE"),					// Also has 100 and 101
	LightningSpear	UMETA(DisplayName = "LIGHTNING/PROJ_MAG"),		// Also has 200 reserved
	Solanumn		UMETA(DisplayName = "POTATO"),					// Also has 300 and 301 
	PoisonClaw		UMETA(DisplayName = "POISON/CLAW_MAG"),			// Also has 400 and 4000 reserved
	GrappleHook		UMETA(DisplayName = "GRAPPLEHOOK"),
	Ember002		UMETA(DisplayName = "FIRE002"),					// Also has 600 reserved
	FireBall		UMETA(DisplayName = "FIREBALL"),				// Also has 70 & 700 reserved
};

UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAMEPROJECT_API UMythorMagicComponent : public UCharHotbarAbilityComponent
{
	GENERATED_BODY()

private:
	// Character pointer
	AmythorChar* Character;

	//Magic Fire
	UPROPERTY(EditAnywhere) bool AttackMagic;
	UPROPERTY() FTimerHandle FireTimerHandle;
	UPROPERTY() bool fireLaunch = false;

public:	
	// Sets default values for this component's properties
	UMythorMagicComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	//Magic
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Jog/Run")
		bool bisSprinting = false;

	UPROPERTY(BlueprintReadWrite, Category = "Magic")
		bool isCasting = false;

	UPROPERTY(BlueprintReadWrite, Category = "Attacks")
		int32 MagicType = 0;

	// Magic general casting
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		UUseMagic* magicUsedRef;

	virtual void BindHotbarAbilityToID() override;
	virtual void ExecuteSelectedAbility(FString abilityHashName, UUseMagic* magicObj) override;

	// General Magic functions
	UFUNCTION(BlueprintCallable, Category = "Magic")
		bool CheckIfMagicUseable(UUseMagic* magic);
	// Generally used for setting up casting the magic
	UFUNCTION(BlueprintCallable, Category = "Magic")
		void SetAttackingAndDisableMovement();
	// 
	UFUNCTION(BlueprintCallable, Category = "Magic")
		void ResetMagicVariables();

	//FIRE
	UFUNCTION(BlueprintCallable, Category = "Magic")
		void MagicFire(UUseMagic* magic);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Magic")
		void MagicFire_BP(UUseMagic* magic);
	UFUNCTION()
		void MagicFireFalse() { fireLaunch = false; MagicType = 0; }
	UPROPERTY(BlueprintReadOnly, Category = "Magic")
		float fireFrcAmt = 1.f;

	//Ember002
	UFUNCTION(BlueprintCallable, Category = "Magic")
		void MagicFire002(UUseMagic* magic);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Magic")
		void MagicFire002_BP(UUseMagic* magic);

	//FIREBALL
	UFUNCTION(BlueprintCallable, Category = "Magic")
		void MagicFireBall(UUseMagic* magic);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Magic")
		void MagicFireBall_BP(UUseMagic* magic);

	//GRAPPLING HOOK
	UFUNCTION(BlueprintCallable, Category = "Magic")
		void MagicGrapplingHook(UUseMagic* magic);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Magic")
		void MagicGrapplingHook_BP(UUseMagic* magic);

	//LIGHTNING
	UFUNCTION(BlueprintCallable, Category = "Magic")
		void MagicLightning(UUseMagic* magic);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Magic")
		void MagicLightning_BP(UUseMagic* magic);

	//POTATO HEAL
	UFUNCTION(BlueprintCallable, Category = "Magic")
		void MagicPotato(UUseMagic* magic);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Magic")
		void MagicPotato_BP();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Magic")
		void MagicPotato_Init_BP(UUseMagic* magic);
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Magic")
		FTimerHandle PotatoTimerHandle;
	UFUNCTION() // Casting magic after timer
		void MagicPotatoCast();

	// POISON CLAW
	UFUNCTION(BlueprintCallable, Category = "Magic")
		void MagicPoisonClaw(UUseMagic* magic);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Magic")
		void MagicPoisonClaw_BP(UUseMagic* magic);

	// Pointer to Character functions
	UFUNCTION(BlueprintCallable)
		AmythorChar* GetOwnerCharacter() { return Character; }
	UFUNCTION(BlueprintCallable)
		void SetOwnerCharacter(AmythorChar* inCharacter) { Character = inCharacter; }
};

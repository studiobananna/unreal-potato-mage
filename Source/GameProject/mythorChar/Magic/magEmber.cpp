// (C) Copyright Studio Bananna, all rights reserved


#include "magEmber.h"

// Sets default values
AmagEmber::AmagEmber()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

// Called when the game starts or when spawned
void AmagEmber::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void AmagEmber::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}


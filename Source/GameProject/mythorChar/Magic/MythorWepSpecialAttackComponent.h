// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "MythorWepSpecialAttackComponent.generated.h"


UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAMEPROJECT_API UMythorWepSpecialAttackComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	// Character pointer
	AmythorChar* Character;

	//DaggerSpecial
	UPROPERTY() bool bDaggerGround = true;
	UPROPERTY() float daggerFrcAmt = 1.25f;
	//GS Special
	UPROPERTY() float greatFrcAmt = 1.25f;
	UPROPERTY() bool GSLaunch = false;
	UPROPERTY() FTimerHandle GreatSpTimerHandle;

public:	
	// Sets default values for this component's properties
	UMythorWepSpecialAttackComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	//---------------------Special Attacks---------------------
	//Dagger
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WepSpecials")
		int32 daggerSPCost = 15;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WepSpecials")
		float daggerCDMax = 1.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WepSpecials")
		float daggerCD = 0.f;
	//GreatSword
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WepSpecials")
		int32 GreatSPCost = 25;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WepSpecials")
		float GreatCDMax = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "WepSpecials")
		float GreatCD = 0.f;

	UPROPERTY(BlueprintReadWrite, Category = "WepSpecials")
		int32 SPAttackType = 0;
	UPROPERTY(BlueprintReadWrite, Category = "WepSpecials")
		bool daggerLaunch = false;
	UPROPERTY(BlueprintReadWrite, Category = "WepSpecials")
		bool stopFadeEffect = false;

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	UFUNCTION()bool CheckIfSpecialAttackUseable();

	UFUNCTION(BlueprintCallable, Category = "Attacks")
		void SpecialAttack_Start();
	
	UFUNCTION()void SpecialAttackDagger_MistStep();
	UFUNCTION()void SPDagger_MistStep_Tick(float DeltaTime);

	UFUNCTION()void SpecialAttackGreatsword_SpinCyclone();
	UFUNCTION()void SPGreatsword_SpinCyclone_Tick(float DeltaTime);

	//Special Attack Weapon BP's
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "WepSpecials")
		void DaggerMistStepSP_BP();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "WepSpecials")
		void GreatSwordCycloneSP_BP();
	UFUNCTION() void GreatSpecialFalse() { GSLaunch = false; }

	UFUNCTION(BlueprintCallable)
		void ResetWeaponSpecialVars();

	// Pointer to Character functions
	UFUNCTION(BlueprintCallable)
		AmythorChar* GetOwnerCharacter() { return Character; }
	UFUNCTION(BlueprintCallable)
		void SetOwnerCharacter(AmythorChar* inCharacter) { Character = inCharacter; }
};

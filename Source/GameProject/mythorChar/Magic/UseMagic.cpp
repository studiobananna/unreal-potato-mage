// (C) Copyright Studio Bananna, all rights reserved


#include "UseMagic.h"

UUseMagic::UUseMagic()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}

void UUseMagic::BeginPlay()
{
	Super::BeginPlay();

	// ...

}

// Called every frame
void UUseMagic::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
}

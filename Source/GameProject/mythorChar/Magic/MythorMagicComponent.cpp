// (C) Copyright Studio Bananna, all rights reserved


#include "MythorMagicComponent.h"
#include "../mythorChar.h"

// Sets default values for this component's properties
UMythorMagicComponent::UMythorMagicComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMythorMagicComponent::BeginPlay()
{
	Super::BeginPlay();

	// Bind Abilities to ID's in order to use 'switch'
	BindHotbarAbilityToID();

	// Get Character
	Character = (AmythorChar*)GetOwner();
	
	if (Character) {
		Character->MagicAbilityComponent = this;
	}
	// ...
	
}

void UMythorMagicComponent::BindHotbarAbilityToID() {
	// Create Map which points Strings to Functions
	toHotbarFunctionMap.Add("FIRE",			EMagic::Ember);
	toHotbarFunctionMap.Add("LIGHTNING",	EMagic::LightningSpear);
	toHotbarFunctionMap.Add("POTATO",		EMagic::Solanumn);
	toHotbarFunctionMap.Add("POISON",		EMagic::PoisonClaw);
	toHotbarFunctionMap.Add("FIRE002",		EMagic::Ember002);
	toHotbarFunctionMap.Add("FIREBALL",		EMagic::FireBall);
	toHotbarFunctionMap.Add("GRAPPLEHOOK",	EMagic::GrappleHook);
}

void UMythorMagicComponent::ExecuteSelectedAbility(FString abilityHashName, UUseMagic* magicObj) {
	// Associate functions with selected integer ID values of each magic
	switch (toHotbarFunctionMap[abilityHashName]) {
		case EMagic::Ember:				MagicFire(magicObj);		break;
		case EMagic::LightningSpear:	MagicLightning(magicObj);	break;
		case EMagic::Solanumn:			MagicPotato(magicObj);		break;
		case EMagic::PoisonClaw:		MagicPoisonClaw(magicObj);	break;
		case EMagic::Ember002:			MagicFire002(magicObj);		break;
		case EMagic::FireBall:			MagicFireBall(magicObj);	break;
		case EMagic::GrappleHook:		MagicGrapplingHook(magicObj); break;
	}
}


// Called every frame
void UMythorMagicComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// -------PASSIVES-------
	// 
	//Magic Passive Regen
	if (GetOwnerCharacter()->GetCharacterMovement()->Velocity != FVector(0, 0, 0)) {
		(GetOwnerCharacter()->mana < GetOwnerCharacter()->manaMax) ? GetOwnerCharacter()->mana +=
			(8.f + (GetOwnerCharacter()->Inventory->haste * 0.45f) + (GetOwnerCharacter()->m_ComboMeter * 0.02f)) * DeltaTime
			: GetOwnerCharacter()->mana = GetOwnerCharacter()->manaMax;
	}
	else {
		(GetOwnerCharacter()->mana < GetOwnerCharacter()->manaMax) ? 
			GetOwnerCharacter()->mana += (14.f + (GetOwnerCharacter()->Inventory->haste * 0.45f) 
				+ (GetOwnerCharacter()->m_ComboMeter * 0.02f)) * DeltaTime
				: GetOwnerCharacter()->mana = GetOwnerCharacter()->manaMax;
	}

	//Wind Magic (SPRINT PASSIVE ABILITY)
	if (GetOwnerCharacter()->GameInst){
		if (GetOwnerCharacter()->GameInst->PL_B_UNLOCKED_SPRINT) {
			// STOP MAGIC
			if ((GetOwnerCharacter()->GetCharacterMovement()->Velocity.Size() <= 525.f && !GetOwnerCharacter()->isRolling) || (GetOwnerCharacter()->mana <= 1.f)) {
				GetOwnerCharacter()->GetCharacterMovement()->MaxWalkSpeed = 700.f;
				bisSprinting = false;
			}
			// START MAGIC
			else if (GetOwnerCharacter()->GetCharacterMovement()->Velocity.Size() >= 500.f && bisSprinting) {
				if (!GetOwnerCharacter()->WepAttackingComponent->m_isSheathed && !GetOwnerCharacter()->isAttacking)
					GetOwnerCharacter()->WepAttackingComponent->MoveWeapon(false);

				// Slightly Drain Mana from Wind Magic
				if (UGameplayStatics::GetGlobalTimeDilation(GetWorld()) == 1.f)
					GetOwnerCharacter()->mana -= 12.f * DeltaTime;
			}
		}
	}

	// -------ABILITIES-------
	// 
	//Ember Magic Launch
	if (fireLaunch && MagicType == EMagic::Ember) {
		//Set Velocity Constant
		GetOwnerCharacter()->GetCharacterMovement()->Velocity = 
			(((GetOwnerCharacter()->GetActorUpVector() * 0.95f) + (GetOwnerCharacter()->GetActorForwardVector() * 1.15f)) * FMath::Pow(10, 3.f) * fireFrcAmt);
		//Increase tempurature glow effect
		GetOwnerCharacter()->CharDynamicMaterialComp->matGlowTemp += 5000.f * DeltaTime;
		GetOwnerCharacter()->AllowRot = true;
		GetOwnerCharacter()->isAttacking = true;
	}
	else {
		// Decrease tempurature glow effect
		(GetOwnerCharacter()->CharDynamicMaterialComp->matGlowTemp > -100.f) ?
			GetOwnerCharacter()->CharDynamicMaterialComp->matGlowTemp -= 1500.f * DeltaTime : 
			GetOwnerCharacter()->CharDynamicMaterialComp->matGlowTemp = -100.f;
	}
}

bool UMythorMagicComponent::CheckIfMagicUseable(UUseMagic* magic)
{
	if (!GetOwnerCharacter()->isAttacking) {
		if (GetOwnerCharacter()->mana >= magic->magCost && magic->cooldown <= 0) {
			return true;
		}
		else if (GetOwnerCharacter()->mana < magic->magCost) {
			GetOwnerCharacter()->HotbarComponent->NotEnoughMana();
			return false;
		}
		else if (magic->cooldown > 0) {
			GetOwnerCharacter()->HotbarComponent->OnCoolDown();
			return false;
		}
		else if (GetOwnerCharacter()->SuperMeterComponent->m_superMeter < magic->superCost) {
			GetOwnerCharacter()->HotbarComponent->NotEnoughMana();
			return false;
		}
	}

	return false;
}

void UMythorMagicComponent::SetAttackingAndDisableMovement()
{
	//Move weapons to sheathe
	GetOwnerCharacter()->WepAttackingComponent->MoveWeapon(false);

	//Set is attacking and disable movement
	GetOwnerCharacter()->AllowRot = true;
	GetOwnerCharacter()->isAttacking = true;
	GetOwnerCharacter()->AllowMovement = false;
}

void UMythorMagicComponent::ResetMagicVariables() {
	// Reset Ember Force amount
	fireFrcAmt = 1.f;
}

// EMBER001 CAST
void UMythorMagicComponent::MagicFire(UUseMagic* magic)
{
	// First check if we can cast 
	if (!CheckIfMagicUseable(magic)) return;
	if (MagicType != 0) return;

	//Stop All Attacks
	GetOwnerCharacter()->CancelAction();

	//Set Cooldown
	magic->cooldown = magic->cooldownMax;

	//Reduce Mana and do BP conterpart
	GetOwnerCharacter()->mana -= magic->magCost;
	//Do BP Part
	MagicFire_BP(magic);
	//Set Gravity
	GetOwnerCharacter()->GetCharacterMovement()->GravityScale = 3.f;
	//Set the magic type
	MagicType = EMagic::Ember;

	// Set magic we are gonna cast variable
	magicUsedRef = magic;

	//Stop Current Velocity
	FVector Vel = GetOwnerCharacter()->GetCharacterMovement()->Velocity;
	GetOwnerCharacter()->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.25f, Vel.Y * 0.25f, Vel.Z * 0.f);
	//Set Force Penalty
	fireFrcAmt = FMath::Max(fireFrcAmt - 0.25f, -1.f);
	//Allow Attack's Velocity to take affect
	GetOwnerCharacter()->GetCharacterMovement()->AddImpulse((GetOwnerCharacter()->GetActorUpVector() 
		+ GetOwnerCharacter()->GetActorForwardVector() * 0.75) * fireFrcAmt * FMath::Pow(10, 5.1f));
	//Set Timer
	fireLaunch = true;
	GetWorld()->GetTimerManager().SetTimer(FireTimerHandle, this, &UMythorMagicComponent::MagicFireFalse, 0.35f, false);
}

//EMBER002 CAST
void UMythorMagicComponent::MagicFire002(UUseMagic* magic)
{
	// First check if we can cast 
	if (!CheckIfMagicUseable(magic)) return;
	if (MagicType != 0) return;

	// Stop all other actions and set the magic type
	GetOwnerCharacter()->CancelAction();
	SetAttackingAndDisableMovement();
	MagicType = EMagic::Ember002;

	// Set magic we are gonna cast variable
	magicUsedRef = magic;

	//Do BP Part
	MagicFire002_BP(magic);

	//Stop Current Velocity
	FVector Vel = GetOwnerCharacter()->GetCharacterMovement()->Velocity;
	GetOwnerCharacter()->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.55f, Vel.Y * 0.55f, Vel.Z * 0.25f);
	if (!GetOwnerCharacter()->GetCharacterMovement()->IsMovingOnGround())GetOwnerCharacter()->GetCharacterMovement()->GravityScale = 1.f;
}

//LIGHTNING CAST
void UMythorMagicComponent::MagicLightning(UUseMagic* magic)
{
	// First check if we can cast 
	if (!CheckIfMagicUseable(magic)) return;

	if (MagicType == 0) {
		// Stop all other actions and set the magic type
		GetOwnerCharacter()->CancelAction();
		SetAttackingAndDisableMovement();
		MagicType = EMagic::LightningSpear;

		// Set magic we are gonna cast variable
		magicUsedRef = magic;

		//Do BP Part
		MagicLightning_BP(magic);
	}
	else if (MagicType == EMagic::LightningSpear * 100) {	// We multiply our enum value by 100 for secondary attacks
		//Do BP Part
		MagicLightning_BP(magic);
		//Set magic type
		SetAttackingAndDisableMovement();
		MagicType = EMagic::LightningSpear * 100;

		// Set magic we are gonna cast variable
		magicUsedRef = magic;

		//Stop Current Velocity
		FVector Vel = GetOwnerCharacter()->GetCharacterMovement()->Velocity;
		GetOwnerCharacter()->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.55f, Vel.Y * 0.55f, Vel.Z * 0.25f);
		if (!GetOwnerCharacter()->GetCharacterMovement()->IsMovingOnGround())GetOwnerCharacter()->GetCharacterMovement()->GravityScale = 1.f;
	}
}

//FIREBALL CAST
void UMythorMagicComponent::MagicFireBall(UUseMagic* magic)
{
	// First check if we can cast 
	if (!CheckIfMagicUseable(magic)) return;

	if (MagicType == 0) {
			// Stop all other actions and set the magic type
			GetOwnerCharacter()->CancelAction();
			SetAttackingAndDisableMovement();
			MagicType = EMagic::FireBall;

			// Set magic we are gonna cast variable
			magicUsedRef = magic;

			//Do BP Part
			MagicFireBall_BP(magic);
	}
	else if (MagicType == EMagic::FireBall * 100) {			// Secondary attack
		//Do BP Part
		MagicFireBall_BP(magic);
		// Stop all other actions and set the magic type
		SetAttackingAndDisableMovement();
		MagicType = EMagic::FireBall * 100;

		// Set magic we are gonna cast variable
		magicUsedRef = magic;
	}

	//Stop Current Velocity
	FVector Vel = GetOwnerCharacter()->GetCharacterMovement()->Velocity;
	GetOwnerCharacter()->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.55f, Vel.Y * 0.55f, Vel.Z * 0.25f);
	if (!GetOwnerCharacter()->GetCharacterMovement()->IsMovingOnGround())GetOwnerCharacter()->GetCharacterMovement()->GravityScale = 1.f;
}

// GRAPPLING HOOK CAST
void UMythorMagicComponent::MagicGrapplingHook(UUseMagic* magic)
{
	// First check if we can cast 
	if (!CheckIfMagicUseable(magic)) return;
	if (MagicType != 0) return;

	// Subtract mana
	GetOwnerCharacter()->mana -= magic->magCost;

	// Stop all other actions and set the magic type
	GetOwnerCharacter()->CancelAction();
	SetAttackingAndDisableMovement();
	MagicType = EMagic::GrappleHook;

	// Set magic we are gonna cast variable
	magicUsedRef = magic;

	//Do BP Part
	MagicGrapplingHook_BP(magic);

	//Stop Current Velocity
	FVector Vel = GetOwnerCharacter()->GetCharacterMovement()->Velocity;
	GetOwnerCharacter()->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.f, Vel.Y * 0.f, Vel.Z * 0.f);
	if (!GetOwnerCharacter()->GetCharacterMovement()->IsMovingOnGround())GetOwnerCharacter()->GetCharacterMovement()->GravityScale = 1.f;
}


//SOLANUM HEAL CAST
void UMythorMagicComponent::MagicPotato(UUseMagic* magic)
{
	// First check if we can cast 
	if (!CheckIfMagicUseable(magic)) return;
	if (MagicType != 0) return;
	if (!GetOwnerCharacter()->GetCharacterMovement()->IsMovingOnGround()) return;

	// Stop all other actions and set the magic type
	GetOwnerCharacter()->CancelAction();
	GetOwnerCharacter()->AllowRot = false;
	GetOwnerCharacter()->isAttacking = true;
	GetOwnerCharacter()->AllowMovement = false;
	MagicType = EMagic::Solanumn;

	//Move weapons to sheathe
	GetOwnerCharacter()->WepAttackingComponent->MoveWeapon(false);

	// Set magic we are gonna cast variable
	magicUsedRef = magic;

	//Set Cooldown to a small amt
	magic->cooldown = 0.2f;

	// Set Cast Timer
	GetWorld()->GetTimerManager().SetTimer(PotatoTimerHandle, this, &UMythorMagicComponent::MagicPotatoCast, magic->magCast, false);

	// Change Animation
	MagicPotato_Init_BP(magic);

	//Stop Current Velocity
	FVector Vel = GetOwnerCharacter()->GetCharacterMovement()->Velocity;
	GetOwnerCharacter()->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.0f, Vel.Y * 0.0f, Vel.Z);
}

// SOLANUM HEAL CAST AFTER CAST TIME
void UMythorMagicComponent::MagicPotatoCast()
{
	if (isCasting == true) {
		//Set Cooldown
		magicUsedRef->cooldown = magicUsedRef->cooldownMax;

		MagicPotato_BP();
		isCasting = false;

		//Reduce Mana and do BP conterpart
		GetOwnerCharacter()->mana -= magicUsedRef->magCost;
	}
}

// POISON CLAW CAST
void UMythorMagicComponent::MagicPoisonClaw(UUseMagic* magic)
{
	// First check if we can cast 
	if (!CheckIfMagicUseable(magic)) return;

	if (MagicType == 0) {
		GetOwnerCharacter()->CancelAction();
		SetAttackingAndDisableMovement();
		MagicType = EMagic::PoisonClaw;

		// Set magic we are gonna cast variable
		magicUsedRef = magic;

		//Do BP Part
		MagicPoisonClaw_BP(magic);

		//Stop Current Velocity
		FVector Vel = GetOwnerCharacter()->GetCharacterMovement()->Velocity;
		GetOwnerCharacter()->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.55f, Vel.Y * 0.55f, Vel.Z * 0.25f);
		if (!GetOwnerCharacter()->GetCharacterMovement()->IsMovingOnGround())GetOwnerCharacter()->GetCharacterMovement()->GravityScale = 1.f;
	}
	else if (MagicType == EMagic::PoisonClaw * 100) {	// Secondary Attack
		//Do BP Part
		MagicPoisonClaw_BP(magic);
		SetAttackingAndDisableMovement();
		MagicType = EMagic::PoisonClaw * 100;

		//Stop Current Velocity
		FVector Vel = GetOwnerCharacter()->GetCharacterMovement()->Velocity;
		GetOwnerCharacter()->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.55f, Vel.Y * 0.55f, Vel.Z * 0.25f);
		if (!GetOwnerCharacter()->GetCharacterMovement()->IsMovingOnGround())GetOwnerCharacter()->GetCharacterMovement()->GravityScale = 1.f;
	}
}
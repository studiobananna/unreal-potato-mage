// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Inventory/UseItem.h"
#include "UseMagic.generated.h"

/**
 *
 */
UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAMEPROJECT_API UUseMagic : public UUseItem
{
	GENERATED_BODY()

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:

	UUseMagic();

	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// Magic Properties
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		int32 magCost = 70;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		float magCast = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Magic")
		int32 superCost = 0;
};

// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "magEmber.generated.h"

UCLASS()
class GAMEPROJECT_API AmagEmber : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AmagEmber();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

};

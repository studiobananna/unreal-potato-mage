// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Runtime/Engine/Classes/GameFramework/SpringArmComponent.h"
#include "Runtime/Engine/Classes/Camera/CameraComponent.h"
#include "Runtime/Engine/Classes/Components/ArrowComponent.h"
#include "Runtime/Engine/Classes/GameFramework/CharacterMovementComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/SphereComponent.h"
#include "DrawDebugHelpers.h"
#include "GameFramework/Character.h"
#include "Hitbox/BaseHitbox.h"
#include "../Inventory/PlayerInventory.h"
#include "PotatoGameInstance.h"
#include "Kismet/GameplayStatics.h"
#include "Enemy/UI/EnemyHealthBar.h"
#include "Event/UseTalkEvent.h"
#include "Inventory/UseItem.h"
#include "Misc/CharDynamicMaterialGenComp.h"
#include "Inventory/CharacterActorCompHandler.h"
#include "../Inventory/HotbarComponent.h"
#include "Misc/CharHitboxCollisionComp.h"
#include "../Enemy/General/BattleEventInterface.h"
#include "../Character_General/SuperMeterComponent.h"
#include "ActorPushComponent.h"
#include "../Character_General/ArmorBreakWidgetComponent.h"
#include "../CallbackComponent.h"
#include "GameProjectGameModeBase.h"
#include "BaseCharacter.generated.h"

//State Machine
UENUM()
enum Status
{
	Idle				UMETA(DisplayName = "Idle"),
	EventLock			UMETA(DisplayName = "EventLock"),
	StuckIGuess			UMETA(DisplayName = "StuckIGuess"),
	Moving				UMETA(DisplayName = "Moving"),
	Running				UMETA(DisplayName = "Running"),
	Attacking			UMETA(DisplayName = "Attacking"),
	Aerial				UMETA(DisplayName = "Aerial"),
	AerialAttacking     UMETA(DisplayName = "AerialAttacking"),
};


DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FDefeatedDelegate, FString, EnmTarget);

UCLASS()
class GAMEPROJECT_API ABaseCharacter : public ACharacter, public IUseTalkEvent, public IBattleEventInterface
{
	GENERATED_BODY()

private:


public:
	// Sets default values for this character's properties
	ABaseCharacter();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UFUNCTION()
		virtual void CharacterStateTick(float DeltaTime);

public:	

	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

	/*----------Save stuff to the game instance----------*/
	UFUNCTION(BlueprintCallable, Category = "Game State")
		virtual void LevelChangeInst();

	/*----------Load stuff from the game instance----------*/
	UFUNCTION(BlueprintCallable, Category = "Loadgame")
		virtual void LoadCharacterFromInstance();

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	// Called to bind functionality to  for hotbar
	void SetupPlayerInputHotbarComponent();

	//Custom Character Declarations
public:

	/*----------PROPERTIES / VARIABLES-------------------------------------*/

	//Remove this soon...
	virtual void UsingItems(int itemIndex) {};

	UPROPERTY(BlueprintReadOnly, Category = "States")
		int32 attackingState = 0;

	UPROPERTY(BlueprintReadOnly, Category = "States")
		int32 characterState = 0;

	//Used for character states
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Status)
		TEnumAsByte<Status> status;

	// Timers General
	UPROPERTY()
		FTimerHandle SaveCoordsTimerHandle;

	UPROPERTY() 
		FTimerHandle SetSpawnLocationHandle;

	UPROPERTY()
		FTimerHandle RollTimer;

	// GLOBAL GAME VARIABLES
	UPROPERTY(BlueprintReadWrite, Category = "Global Game")
		UPotatoGameInstance* GameInst;

	UPROPERTY(BlueprintReadWrite, Category = "Global Game")
		AGameProjectGameModeBase* GameMode;

	//NEED THESE COMPONENTS FOR CHARACTERS
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Base Character")
		USpringArmComponent *SpringArm; //Holds camera and gives it collision

	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "Base Character")
		UCameraComponent *Camera; //Allows the camera to be used and viewed

	UPROPERTY(EditDefaultsOnly, Category = "Base Character")
		UArrowComponent *ForwardDirection; //Keeps track of our forward vector

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Trigger Capsule")
		UCapsuleComponent *HitboxCapsule;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Targetting")
		USphereComponent* TargetSphere;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Targetting")
		float TargetRadius = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Targeting")
		float TargetMaxRadius = 3200.f;


	UPROPERTY(BlueprintReadOnly, Category = "HUD")
		class UPlayerInventory* Inventory;
	// Weird UE4 bug which I had to fix by creating a new variable to set 'Inventory' to in order to avoid it being set to NULL
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "HUD")
		class UPlayerInventory* InventoryComp;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		class UUseItem* ItemToUse;

	// UNUSED TARGET ARRAY (I THINK)
	UPROPERTY(BlueprintReadWrite, Category = "Targetting") 
		TArray<ABaseCharacter*> Targets;

	// Used for determining which character a character targets to attack or do stuff to
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Targetting", meta = (ExposeOnSpawn = "true"))
		class ABaseCharacter* Target = NULL;

	// For any Events or NPCs to interact with
	UPROPERTY(BlueprintReadWrite, Category = "Targeting")
		class AActor* EventUseTarget = NULL;

	// Show Target Health Bar
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "HUD")
		class UEnemyHealthBar* TargetHealthBar;

	// Armor Break
	UPROPERTY()
		TSubclassOf<UArmorBreakWidgetComponent> ArmorBreakWidgetClass;
	UPROPERTY()
		UArmorBreakWidgetComponent* ArmorBreakWidget;

	// The Player HUD (FOR PLAYER)
	UPROPERTY(BlueprintReadWrite, Category = "HUD")
		class UUserWidget* PlayerHUDWidget;

	// Character Actor Component Handler for handling actor components
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		UCharacterActorCompHandler* CharActorCompHandler;

	// Character Dynamic Material Gen Component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		UCharDynamicMaterialGenComp* CharDynamicMaterialComp;

	// Character Hitbox Collision Handler Component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		UCharHitboxCollisionComp* CharHitboxDamageComp;

	// Magic Component that stores all magic/ability related stuff that can be used on the hotbar
	UPROPERTY(BlueprintReadOnly)
		class UCharHotbarAbilityComponent* MagicAbilityComponent;

	// Hotbar Component for hotbar related actions
	UPROPERTY(BlueprintReadOnly)
		class UHotbarComponent* HotbarComponent;

	// Super Meter Component
	UPROPERTY(BlueprintReadOnly)
		USuperMeterComponent* SuperMeterComponent;

	// Actor Push Component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Components")
		UActorPushComponent* ActorPushComponent;

	/*-------Health, Mana, Misc.-------*/

	// The modified scaling of all stats based on this (THIS WILL CHANGE ALL STATS), BASED ON CURRENT LEVEL ROOM SCALE
	UPROPERTY(BlueprintReadOnly, Category = "Stats")
		float LevelScale = 1;

	// OVERRIDE modified scaling of all stats based on this (THIS WILL CHANGE ALL STATS)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (ExposeOnSpawn = "true"))
		float LevelScaleOverride = 1;

	// The modified scaling of health and mana from skillpoints (MAINLY FOR PLAYER)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		bool UseHealthManaScale = false;
	// Health
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float healthMax = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float health = healthMax;
	// Mana
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float manaMax = 100;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float mana = manaMax;
	// SET ARMOR MAX >0 TO USE ARMOR MECHANIC
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float armorMax = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float armor = armorMax;

	/*------DEBUG------*/
	UPROPERTY(BlueprintReadWrite, Category = "Base Character") //Trash from the tutorial
		bool CanDrawDebugVars = false;
	UPROPERTY(BlueprintReadWrite, Category = "Base Character") //Trash from the tutorial
		bool bIsFiring;

	/*-------Attacks-------*/
	UPROPERTY(BlueprintReadWrite, Category = "Attacks")
		bool isAttacking = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Faction")
		int32 Faction = 0;
	UPROPERTY(BlueprintReadWrite, Category = "Attacks")
		int32 m_ActionState = 0;

	/*-------Events & Movement-------*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Events")
		bool AllowMovement = true;
	UPROPERTY(BlueprintReadWrite, Category = "Events")
		bool AllowRot = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Events")
		float AccelerationScale = 2.0f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Events")
		float defaultFriction = 1.25f;
	UPROPERTY(BlueprintReadOnly, Category = "Events")
		FVector LastFootingCoords;
	UPROPERTY(BlueprintReadOnly, Category = "Events")
		FRotator LastFootingRotation;
	UPROPERTY(BlueprintReadWrite, Category = "Events")
		bool bInCombat = false;
	UPROPERTY(BlueprintReadOnly, Category = "Events")
		FVector LastCheckPointCoords;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera") // Zoom During Combat
		float combatZoom = 450.f;
	UPROPERTY(BlueprintReadWrite, Category = "Events")
		bool m_AllowRollingForce = false;
	UPROPERTY(BlueprintReadWrite, Category = "Events")
		bool m_AllowAttackForce = false;
	UPROPERTY(BlueprintReadWrite, Category = "Events")
		float m_RollForceFactor = 8.45f;
	UPROPERTY(BlueprintReadWrite, Category = "Events")
		float m_AirRollForceFactor = 5.35f;
	UPROPERTY(BlueprintReadWrite, Category = "Events")
		float m_AttackForceFactor = 8.7f;
	UPROPERTY(BlueprintReadWrite, Category = "Dodging")
		bool isRolling = false;

	/*----------Environment----------*/
	UPROPERTY(BlueprintReadWrite, Category = "Environment") // Water Sound
		bool inWater = false;

	UPROPERTY(BlueprintReadWrite, Category = "Environment") // If hit deathbox
		bool bDeathFalling = false;

	/*------UI------*/
	UPROPERTY(BlueprintReadWrite, Category = "UI")
		bool isLockedOn = false;

	/*------Camera-------*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Camera") // Zoom Out of Combat
		float cameraZoom = 250.f;

	/*----------Movement Physics----------*/
	UPROPERTY()
		FVector Direction;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "States", meta = (ExposeOnSpawn = "true"))
		bool bCanDie = true;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		bool bIsEventLocked;

	/*----------FUNCTIONS----------------------------------------------------------------*/
	//----------GENERAL STATS----------------------
	//GENERAL
	UFUNCTION()
		void SetSpawnPoint();
	// Calculate Stats from Inventory Stats
	UFUNCTION(BlueprintCallable, Category = "Stats")
		virtual void LoadSkillStats();
	UFUNCTION(BlueprintImplementableEvent, Category = "Stats")
		void BP_LoadSkillStats();

	/*----------Input Bindings for Movement----------*/
	virtual void MoveForward(float amount);
	virtual void MoveRight(float amount);
	virtual void Jump();
	// On landing event
	UFUNCTION(BlueprintCallable, Category = "Movement")
		virtual void Landed(const FHitResult& Hit);
	// Loading coordinates when we fall
	UFUNCTION(BlueprintCallable, Category = "Attacks")
		void LoadLastCoords();
	UFUNCTION()
		void SaveLastCoords();
	// Input Enabled or not (For some reason BP's don't have this function LMAO)
	UFUNCTION(BlueprintPure, Category = "Movement")
		bool GetInputEnabled();

	/*----------Camera Rotations----------*/
	void RotateCamera(float amount);
	void ChangeCameraHeight(float amount);

	/*----------Camera Zoom----------*/
	void CameraZoom(float amount);

	/*----------Attacks----------*/
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Attacks")
		void AttackingStart();
		virtual void AttackingStart_Implementation();
	UFUNCTION(BlueprintCallable, Category = "Attacks")
		virtual void Attack() {};
	UFUNCTION(BlueprintCallable, Category = "Collisions")
		virtual void OnAttackHit(UBaseHitbox* Hitbox);

	// Cancels any actions/attacks
	UFUNCTION(BlueprintCallable, Category = "Battle")
		virtual void CancelAction();
	UFUNCTION(BlueprintImplementableEvent, Category = "Battle")
		void CancelAction_BP();

	//*----------Questing----------*/
	UPROPERTY(BlueprintReadWrite, Category = "Quests")
		FDefeatedDelegate OnDefeatDelegate;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Quests")
		FString QuestTargName = "QUEST_TARGET";

	//*----------Level Up EXP----------*/

	// Updates the level scale of the character
	UFUNCTION(BlueprintCallable, Category = "Leveling")
		void UpdateLevelScale(float scale);

	UFUNCTION(BlueprintCallable, Category = "Leveling")
		void OnLevelUp();

	UFUNCTION(BlueprintImplementableEvent, Category = "Leveling")
		void OnLevelUp_BP();

	//*----------Loading Stuff From Game Instance----------*/

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Saving")
		bool bCanSaveStats = false;


	//*----------Blueprint Implementable Events (Tried putting them into the DamageHitboxComponent, but they need to be here...)----------*/
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Collisions")
		void Dead();

	//*----------On Damaging----------*/
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Collisions")
		void DamageReaction();
		virtual void DamageReaction_Implementation(){};
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Collisions")
		void OnDamaged();

	// Hotbar Input Functions (Idk how to put these inside of the Hotbar component...so they're here unfortunately)
	UFUNCTION()void CycleHotbarRowUp();
	UFUNCTION()void CycleHotbarRowDown();
	UFUNCTION()void UseHotbarItem000();
	UFUNCTION()void UseHotbarItem001();
	UFUNCTION()void UseHotbarItem002();
	UFUNCTION()void UseHotbarItem003();
};

// (C) Copyright Studio Bananna, all rights reserved


#include "mythorChar/Misc/CharHitboxCollisionComp.h"
#include "Kismet/KismetMathLibrary.h"
#include "mythorChar/BaseCharacter.h"

// Sets default values for this component's properties
UCharHitboxCollisionComp::UCharHitboxCollisionComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// Set overlaps to true
	SetGenerateOverlapEvents(true);
}


// Called when the game starts
void UCharHitboxCollisionComp::BeginPlay()
{
	Super::BeginPlay();

	if (Character) {
		if (Character->HitboxCapsule) {
			Character->HitboxCapsule->OnComponentBeginOverlap.AddDynamic(this, &UCharHitboxCollisionComp::OnOverlapBegin);
			Character->HitboxCapsule->OnComponentEndOverlap.AddDynamic(this, &UCharHitboxCollisionComp::OnOverlapEnd);
		}
		if (Character->TargetSphere) {
			Character->TargetSphere->OnComponentBeginOverlap.AddDynamic(this, &UCharHitboxCollisionComp::OnOverlapBegin);
			Character->TargetSphere->OnComponentEndOverlap.AddDynamic(this, &UCharHitboxCollisionComp::OnOverlapEnd);
		}
	}
}


// Called every frame
void UCharHitboxCollisionComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	//Armor Break Regen Mechanic
	if (GetOwnerCharacter()->armorMax > 0) {
		// If armor broken
		if (bIsArmorBreak) {
			if (GetOwnerCharacter()->armor < GetOwnerCharacter()->armorMax) {
				GetOwnerCharacter()->armor += 0.05f * GetOwnerCharacter()->armorMax * DeltaTime;
			}
			else {
				GetOwnerCharacter()->armor = GetOwnerCharacter()->armorMax;
				bIsArmorBreak = false;
			}
		}
		else {		// If NOT armor broken
			(GetOwnerCharacter()->armor < GetOwnerCharacter()->armorMax) ? GetOwnerCharacter()->armor += 0.015f * 
				GetOwnerCharacter()->armorMax * DeltaTime : GetOwnerCharacter()->armor = GetOwnerCharacter()->armorMax;
		}
	}
	// ...
}

//Collision Overlap Events
void UCharHitboxCollisionComp::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	// Display Text Debug
	//UE_LOG(LogTemp, Warning, TEXT("%s was overlapped!"), *FString(GetName()));

	//If hit from any collision check
	if (OtherActor && OtherActor != GetOwnerCharacter() && OtherComp && OverlappedComp == GetOwnerCharacter()->HitboxCapsule) {
		//If it is type UBaseHitbox
		if (OtherComp->GetClass()->IsChildOf(UBaseHitbox::StaticClass()))
		{
			//Get Targeted Hitbox
			UBaseHitbox* HitBy = (UBaseHitbox*)OtherComp;

			//Check Faction
			if (HitBy->HitFaction != GetOwnerCharacter()->Faction && (!bIsInvincible || !HitBy->bIsDodgeable))
			{
				// Break if the attack only works on the ground and this is in the air
				if (HitBy->isGroundedOnly && !GetOwnerCharacter()->GetCharacterMovement()->IsMovingOnGround()) return;

				//Check if enemy is in hit list
				bool isHit = HitBy->HitArry.Contains(GetUniqueID());

				//Hit Event
				if (!isHit) {
					//Get the Launch Angle and Direction of the Attack
					float launchAngle = HitBy->GetAngle();
					FVector direction = UKismetMathLibrary::GetDirectionUnitVector(HitBy->GetParentCoords() * FVector(1, 1, 0), 
						GetOwnerCharacter()->GetActorLocation() * FVector(1, 1, 0));
					direction.Z = launchAngle;

					//Reset movement and increase Z when hit
					FVector Vel = GetOwnerCharacter()->GetCharacterMovement()->Velocity;
					if (!HitBy->isConst() && GetOwnerCharacter()->armor <= 0.f)GetOwnerCharacter()->GetCharacterMovement()->Velocity = FVector(0.f, 0.f, 400.0f);

					//Get Damage taken values
					damageTaken = HitBy->GetDamage();
					forceTaken = HitBy->GetForce() * 0.75f;
					directionTaken = direction;

					//Store (If Constant) and Deal the Damage from values
					if (HitBy->isConst()) {
						//Set Timer to repeat while getting hit
						constDamageTaken = damageTaken;
						GetWorld()->GetTimerManager().SetTimer(HurtTimerHandle, this, 
							&UCharHitboxCollisionComp::DealDmgTime, HitBy->constantHitRate, true, HitBy->constantHitRate);
					}

					// First Apply Status Effects
					damageTaken = CalculateStatuses(damageTaken, HitBy);

					// Then Deal the damage
					DealDamage(damageTaken, HitBy->armorDamage, forceTaken, directionTaken);

					// Broadcast Damage Dealt for passive components
					if (HitBy->parent != nullptr && HitBy->parent->GetClass()->IsChildOf(ABaseCharacter::StaticClass())) 
					{
						ABaseCharacter* AttackingChar = (ABaseCharacter*)HitBy->parent;

						if (AttackingChar && GetOwnerCharacter())
						{
							// < CALLBACK > -- Broadcast on damage dealt using global callback component
							UCallbackComponent* Callback = Cast<UCallbackComponent>(GetOwnerCharacter()->GameMode->CallbackComponent);

							if (Callback) {
								// Damage dealt callback to this character
								Callback->C_OnDamageDealt.Broadcast(damageTaken, AttackingChar, GetOwnerCharacter());
							}

							// Add to super meter to the attacking character (scales from hitbox base damage with no scaling)
							if (AttackingChar->SuperMeterComponent){
								AttackingChar->SuperMeterComponent->AddSuperMeter( HitBy->baseDamage * AttackingChar->SuperMeterComponent->m_superMeterHitGain );
							}
						}

						// Add super meter to character taking damage too
						if (GetOwnerCharacter()) {
							if (GetOwnerCharacter()->SuperMeterComponent)
								GetOwnerCharacter()->SuperMeterComponent->AddSuperMeter(
									HitBy->baseDamage * GetOwnerCharacter()->SuperMeterComponent->m_superMeterHurtGain);
						}
					}

					//Add enemy to hit array if not a constant hitbox (if constant then we can get hit multiple times)
					if (!HitBy->isConstant) HitBy->HitArry.Add(GetUniqueID());

					//Get Calculated Direction && Show Hit effect
					HitBy->HitDirection = direction;
					HitBy->DoEffect();

					//Check if launch angle...uh launches into air, then rotate from attack
					FRotator curRot;
					curRot.Roll = GetOwnerCharacter()->GetActorRotation().Roll; curRot.Pitch = GetOwnerCharacter()->GetActorRotation().Pitch;
					curRot.Yaw = direction.Rotation().Yaw + 180.f;
					if (launchAngle > 0.f && bIsArmorBreak) { GetOwnerCharacter()->SetActorRotation(curRot); }
				}
			}
		}
	}

	//If target character is in range
	if (OtherActor && OtherActor != GetOwnerCharacter() && OtherComp && OverlappedComp == GetOwnerCharacter()->TargetSphere) {
		//If it is type ABaseCharacter
		if (OtherActor->GetClass()->IsChildOf(ABaseCharacter::StaticClass()))
		{
			ABaseCharacter* CharHit = (ABaseCharacter*)OtherActor;
			GetOwnerCharacter()->Targets.Add(CharHit);
		}
	}
}

void UCharHitboxCollisionComp::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex)
{
	//If hit from any hitbox
	if (OtherActor && (OtherActor != GetOwnerCharacter()) && OtherComp
		&& OverlappedComp == GetOwnerCharacter()->HitboxCapsule) {
		//Check Class
		if (OtherComp->GetClass()->IsChildOf(UBaseHitbox::StaticClass())) {
			//Get Targeted Hitbox
			UBaseHitbox* HitBy = (UBaseHitbox*)OtherComp;

			//Check Faction
			if (HitBy->HitFaction != GetOwnerCharacter()->Faction) {
				//If hitbox is constant (attack multiple times)
				(HitBy->isConst()) ? GetWorld()->GetTimerManager().ClearTimer(HurtTimerHandle) : HurtTimerHandle = HurtTimerHandle;
			}
		}
	}

	//If target character is out of range
	if (OtherActor && OtherActor != GetOwnerCharacter() && OtherComp && OverlappedComp == GetOwnerCharacter()->TargetSphere) {
		//If it is type ABaseCharacter
		if (OtherActor->GetClass()->IsChildOf(ABaseCharacter::StaticClass())) {
			ABaseCharacter* CharHit = (ABaseCharacter*)OtherActor;
			GetOwnerCharacter()->Targets.Remove(CharHit);
		}
	}
}

void UCharHitboxCollisionComp::DealDamage(float damage, float armorDamage, float ForceAmount, FVector ForceDirection) 
{
	// Deal Damage Health and Execute Damage taken func
	Character->OnDamaged();
	GetOwnerCharacter()->health -= damage;

	// Do rotation damage effect
	if (ForceAmount >= 1.f) {
		FRotator rot = GetOwnerCharacter()->GetMesh()->GetRelativeRotation();
		rot.Pitch = 0.f;
		GetOwnerCharacter()->GetMesh()->SetRelativeRotation(rot);

		rot = GetOwnerCharacter()->GetMesh()->GetRelativeRotation();
		rot.Pitch = 8.f;
		GetOwnerCharacter()->GetMesh()->SetRelativeRotation(rot);
	}

	//Check Dead or not
	if (GetOwnerCharacter()->health <= 0 && GetOwnerCharacter()->bCanDie) Character->Dead();
	else if (GetOwnerCharacter()->health <= 0) GetOwnerCharacter()->health = 1;

	//Deal Armor Damage
	if (!bIsArmorBreak && GetOwnerCharacter()->armorMax > 0) {
		// If we have broken their armor
		if (GetOwnerCharacter()->armor <= 0) {
			OnArmorBreak();
		}
		// If we haven't broken their armor yet..
		else {
			GetOwnerCharacter()->armor -= armorDamage;
			GetOwnerCharacter()->armor = FMath::Clamp(GetOwnerCharacter()->armor, -20.f, GetOwnerCharacter()->armorMax);
		}
	}

	// Add amounts to history to display on Healthbar
	damageHistory += damage;
	forceHistory = ForceAmount;
	directionHistory = ForceDirection;

	// Erase History after time
	GetWorld()->GetTimerManager().SetTimer(DamageValTimer, this, &UCharHitboxCollisionComp::DamageHistReset, 2.f, true);

	//React to Damage if No Armor and has Force > 0
	if ((bIsArmorBreak || GetOwnerCharacter()->armor <= 0.f) && ForceAmount > 0.f) { Character->DamageReaction(); }
	else { ForceAmount *= 0.f; }

	// Move target backwards and damage
	if (ForceAmount != 0.f) {
		GetOwnerCharacter()->GetCharacterMovement()->AddImpulse(ForceDirection * ForceAmount * FMath::Pow(10, 4.4));

		// Add physics force
		if( GetOwnerCharacter()->GetRootComponent()->IsSimulatingPhysics() ) 
		{
			UPrimitiveComponent* rootPrimitive = (UPrimitiveComponent*) GetOwnerCharacter()->GetRootComponent();
			if (rootPrimitive) {
				rootPrimitive->AddForce(ForceDirection * ForceAmount * FMath::Pow(10, 7.4));
			}
		}
	}

	// Display Text Debug
	UE_LOG(LogTemp, Warning, TEXT("%s Takes %f damage!"), *FString(GetName()), damage);
}

void UCharHitboxCollisionComp::OnArmorBreak()
{
	if (GetOwnerCharacter()->armorMax > 0)
	{
		// Set Armor Break to true
		bIsArmorBreak = true;

		// Do stuff with the armor break component...
		// Play armor break splash
		auto ArmorBreakWidget = GetOwnerCharacter()->ArmorBreakWidget;

		if (ArmorBreakWidget) {
			ArmorBreakWidget->BP_PlaySplash();
		}
	}
}

float UCharHitboxCollisionComp::CalculateStatuses(float damage, UBaseHitbox* Hitby)
{
	// PHYSICAL
	if (Hitby->bIsPhysical) damage -= damage * (physicalResist + blockResist);
	// FIRE
	if (Hitby->bIsFire) damage -= damage * (fireResist + blockResist);
	// MAGIC
	if (Hitby->bIsMagic) damage -= damage * (magicResist + blockResist);
	// DARK
	if (Hitby->bIsDark) damage -= damage * (darkResist + blockResist);
	// NATURE
	if (Hitby->bIsNature) damage -= damage * (natureResist + blockResist);

	// GET STATUS EFFECTS ARRAY FROM HITBOX
	// First check if we are going to use status effects from the hitbox
	if (Hitby->CanUseStatusEffects) {
		for (int i = 0; i < Hitby->StatusEffects.Num(); i++) {
			if (Hitby->StatusEffects[i]) {
				// First get a default class object of the status effect to check if it can stack
				UStatusEffectComponent* DefaultActor = Cast<UStatusEffectComponent>(Hitby->StatusEffects[i]->GetDefaultObject(true));

				// Then if there are no other statuses of the same type OR it can can, then do so...
				if (!GetOwnerCharacter()->FindComponentByClass(Hitby->StatusEffects[i]) || DefaultActor->bCanStack) {
					// Then construct them and assign to this Character, then execute the status's effect
					UStatusEffectComponent* NewStatusEffect =
						(UStatusEffectComponent*)GetOwnerCharacter()->CharActorCompHandler->ConstructFromClass(Hitby->StatusEffects[i]);
					NewStatusEffect->Character = GetOwnerCharacter();
					NewStatusEffect->ExecuteStatusEffect();
				}
			}
		}
	}

	// Take less damage if have armor
	if(GetOwnerCharacter()->armor > 0 && !bIsArmorBreak)
		damage -= damage * 0.2f;

	// Return final damage
	return damage;
}
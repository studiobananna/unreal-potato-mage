// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Hitbox/BaseHitbox.h"
#include "Components/PrimitiveComponent.h"
#include "CharHitboxCollisionComp.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEPROJECT_API UCharHitboxCollisionComp : public UPrimitiveComponent
{
	GENERATED_BODY()

private:
	ABaseCharacter* Character;
public:	
	// Damage History Values
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
		float damageHistory = 0.f;
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
		float forceHistory;
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
		FVector directionHistory;
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
		float damageTaken;
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
		float constDamageTaken;
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
		float forceTaken;
	UPROPERTY(BlueprintReadOnly, Category = "Damage")
		FVector directionTaken;

	/*----------Damaging PROPERTIES----------*/
	UPROPERTY()FTimerHandle HurtTimerHandle;
	UPROPERTY()FTimerHandle DamageValTimer;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		bool bIsInvincible = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage")
		bool bIsArmorBreak = false;

	/*----------Damaging RESISTANCES (Damage Reduced by Resistance %) (- damage * resistAmt)----------*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Resist")
		float physicalResist = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Resist")
		float fireResist = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Resist")
		float magicResist = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Resist")
		float darkResist = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Resist")
		float natureResist = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Resist")
		float poisonResist = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Resist")
		float paralysisResist = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Resist")
		float slowResist = 0.f;

	/*----------Damaging BONUSES FROM SKILLS (% Increase Based, 0.1 == 10%)----------*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Bonus")
		float physicalBNS = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Bonus")
		float fireBNS = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Bonus")
		float magicBNS = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Bonus")
		float darkBNS = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Bonus")
		float natureBNS = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Bonus")
		float poisonBNS = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Bonus")
		float paralysisBNS = 0.f;

	// Blocking Resistance (AFFECTS ALL RESISTANCES)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Damage Resist")
		float blockResist = 0.f;

	// Status effects from hitboxes
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Status Effects")
		TArray<class UStatusEffectComponent*> StatusEffects;

	// Sets default values for this component's properties
	UCharHitboxCollisionComp();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	/*----------Collision Triggers----------*/
	//Overlap Functions (Overlap anything) i.e taking damage
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor,
			UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	//*----------Damaging FUNCTIONS----------*/
	UFUNCTION(BlueprintCallable, Category = "Collisions")
		virtual void DealDamage(float damage, float armorDamage, float ForceAmount, FVector ForceDirection);
	UFUNCTION()
		void DealDmgTime() { DealDamage(constDamageTaken, 0.f, 0.f, FVector(0.f, 0.f, 0.f)); }
	UFUNCTION()
		void DamageHistReset() { damageHistory = 0; }

	// Damage Functions against Hitboxes
	UFUNCTION(BlueprintCallable, Category = "Collisions")
		float CalculateStatuses(float damage, UBaseHitbox* Hitby);
	UFUNCTION()
		void OnArmorBreak();

	// Returns owner character
	UFUNCTION(BlueprintCallable)
		ABaseCharacter* GetOwnerCharacter() { return Character; }
	UFUNCTION(BlueprintCallable)
		void SetOwnerCharacter(ABaseCharacter* inCharacter) { Character = inCharacter; }

};

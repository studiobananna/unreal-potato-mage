// (C) Copyright Studio Bananna, all rights reserved


#include "mythorChar/Misc/CharDynamicMaterialGenComp.h"

// Sets default values for this component's properties
UCharDynamicMaterialGenComp::UCharDynamicMaterialGenComp()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = false;

	// ...
}


// Called when the game starts
void UCharDynamicMaterialGenComp::BeginPlay()
{
	Super::BeginPlay();
	// Get Character
	Character = (ACharacter*) GetOwner();

	// Dynamic Materials
	GenerateDynamicMaterials();

	// ...
	
}


// Called every frame
void UCharDynamicMaterialGenComp::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UCharDynamicMaterialGenComp::GenerateDynamicMaterials()
{
	if (Character->GetMesh())
	{
		// Fill the dynamic material array with materials
		for (int i = 0; i < Character->GetMesh()->GetMaterials().Num(); i++)
		{
			// Replace with dynamic materials
			UMaterialInstanceDynamic* DynMaterial = UMaterialInstanceDynamic::Create(Character->GetMesh()->GetMaterial(i), this);
			Character->GetMesh()->SetMaterial(i, DynMaterial);
			matArray.Add(DynMaterial);
		}
	}
}

void UCharDynamicMaterialGenComp::ReplaceMaterialAndMakeDynamic(int32 MatIndex, UMaterialInterface* MatReplace)
{
	if (Character->GetMesh())
	{
		if (Character->GetMesh()->GetMaterial(MatIndex))
		{
			// Replace the material with the new
			Character->GetMesh()->SetMaterial(MatIndex, MatReplace);

			// Replace with dynamic material
			UMaterialInstanceDynamic* DynMaterial = UMaterialInstanceDynamic::Create(Character->GetMesh()->GetMaterial(MatIndex), this);
			Character->GetMesh()->SetMaterial(MatIndex, DynMaterial);

			// Insert into same place on mat array
			matArray.Insert(DynMaterial, MatIndex);

		}
	}
}
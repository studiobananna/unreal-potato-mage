// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Materials/MaterialInstanceDynamic.h"
#include "GameFramework/Character.h"
#include "CharDynamicMaterialGenComp.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAMEPROJECT_API UCharDynamicMaterialGenComp : public UActorComponent
{
	GENERATED_BODY()

private:
	class ACharacter* Character;

public:	
	// Sets default values for this component's properties
	UCharDynamicMaterialGenComp();

	/*------Material Vars------*/
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Materials")
		TArray<UMaterialInstanceDynamic*> matArray;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Materials")
		float matAlpha = 1.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Materials")
		float matGlowTemp = 0.f;
	UPROPERTY(BlueprintReadWrite, Category = "Materials")
		bool fadeEff = false;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable)
		void GenerateDynamicMaterials();

	UFUNCTION(BlueprintCallable)
		void ReplaceMaterialAndMakeDynamic(int32 MatIndex, UMaterialInterface* MatReplace);

	UFUNCTION(BlueprintCallable)
		ACharacter* GetOwnerCharacter() { return Character; }

		
};

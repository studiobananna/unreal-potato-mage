// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "mythorChar/BaseCharacter.h"
#include "mythorChar/Weapons/BaseWeapon.h"
#include "mythorChar/Weapons/UseWeapon.h"
#include "Magic/UseMagic.h"
#include "../mythorChar/Attacks/MythorWepAttackComponent.h"
#include "Magic/MythorMagicComponent.h"
#include "Magic/MythorWepSpecialAttackComponent.h"
#include "mythorChar.generated.h"

UCLASS()
class GAMEPROJECT_API AmythorChar : public ABaseCharacter
{
	GENERATED_BODY()

private:
	//Dodge Roll
	UPROPERTY() FTimerHandle RollTimerHandle;

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Sets default values for this character's properties
	AmythorChar();

	// Save stats between levels FOR GAME INSTANCE
	virtual void LevelChangeInst() override;

	// Load stats between levels FOR GAME INSTANCE
	virtual void LoadCharacterFromInstance() override;

	// Get Calculations From Level Ups
	virtual void LoadSkillStats() override;

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//Override jump function
	virtual void Jump();

	//------------------Inventory && HUD------------------
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Targetting")	// Target that's locked on
		class AActor* TargetLockOn;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Targetting")	// Is locked on
		bool TargetLocked;

	// Fleeing Battles
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Targetting")
		class UUserWidget* FleeingBattleWidget;

	//------------------Blueprint properties------------------

	// SPECIFIC MOVEMENT STATE VARS
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hurt")
		bool InHurtAnim = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float CharacterBaseZRotationRate = 510.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MoveLeanPercent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Movement")
		float MaxLeanAmount = 15.f;

	//Functions
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Battle")
		void BattleStance();
	UFUNCTION(BlueprintCallable, BlueprintCallable, Category = "Movement")
		void DodgeRoll();
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Movement")
		void DodgeRoll_BP();

	// Override Cancel Attack
	virtual void CancelAction() override;

	/*----------Input Bindings for Movement----------*/
	virtual void MoveForward(float amount) override;
	virtual void MoveRight(float amount) override;

	/*----------Input Actions----------*/
	//Attacks
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
		UMythorWepAttackComponent* WepAttackingComponent;
	//Attacks
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
		UMythorWepSpecialAttackComponent* WepSpecialAttackComponent;

	virtual void AttackingStart_Implementation() override;
	// Attack Hit
	virtual void OnAttackHit(UBaseHitbox* Hitbox) override;

	// Combo Meter Mechanics
	UPROPERTY(BlueprintReadWrite, Category = "Attacks")
		float m_ComboMeter = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
		bool m_InfiniteCombo = false;


	// MELEE SKILL TREE EFFECTS
	UPROPERTY(BlueprintReadWrite, Category = "Melee Skills")
		bool maxHealthAttackBNS = false;
		float maxHealthAttackBNSVal = 0.15f;

	UPROPERTY(BlueprintReadWrite, Category = "Melee Skills")
		bool PhysAttackLifeSteal = false;

	//MAGIC SKILL TREE EFFECTS
	UPROPERTY(BlueprintReadWrite, Category = "Magic Skills")
		bool elementalistSkill = false;

	UPROPERTY(BlueprintReadWrite, Category = "Magic Skills")
		bool FireLigDOTSkill = false;

	// ITEM USAGE
	UFUNCTION(BlueprintCallable, Category = "Items")
		virtual void UsingItems(int itemIndex) override;
	UFUNCTION(BlueprintImplementableEvent, Category = "Items")
		void UsingItems_BP(UUseItem* itemIndex);

	//Input
	UFUNCTION() void SpecialAttack_Input();
	UFUNCTION() void CycleWeapons();

	// Landing
	virtual void Landed(const FHitResult& Hit) override;
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Movement")
		void Landed_BP();
};

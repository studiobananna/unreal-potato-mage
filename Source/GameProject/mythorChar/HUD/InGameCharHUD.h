// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "mythorChar/BaseCharacter.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "GameFramework/InputSettings.h"
#include "InGameCharHUD.generated.h"

/**
 * 
 */
//DECLARE_DYNAMIC_DELEGATE_OneParam(FInputAxisDelegate, float, value);

UCLASS()
class GAMEPROJECT_API UInGameCharHUD : public UUserWidget
{
	GENERATED_BODY()
private:

public:

	UInGameCharHUD(const FObjectInitializer& ObjectInitializer);
	// Construction / Tick Functions
	virtual void NativePreConstruct();
	virtual void NativeConstruct();
	virtual void NativeDestruct();
	virtual void NativeTick(const FGeometry& MyGeometry, float InDeltaTime);

	//Button Timer Function
	UPROPERTY()FTimerHandle HoldButtonTimer;
		

	// Animations
	// Main
	UPROPERTY(Transient, meta = (BindWidgetAnim))
		UWidgetAnimation* SpawnCol_main;
	UPROPERTY(Transient, meta = (BindWidgetAnim))
		UWidgetAnimation* spr_target_anim01;
	UPROPERTY(Transient, meta = (BindWidgetAnim))
		UWidgetAnimation* Spawn_Misc;

	// Trees and Main Col
	UPROPERTY(Transient, meta = (BindWidgetAnim))
		UWidgetAnimation* TabOutTrees;
	UPROPERTY(Transient, meta = (BindWidgetAnim))
		UWidgetAnimation* TabOutMainColumn;

	// Magics
	UPROPERTY(Transient, meta = (BindWidgetAnim))
		UWidgetAnimation* AssignMagics;

	
	// Logic Properties
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menu")
		bool bReturnFill = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menu")
		float HoldReturnVal = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Character", meta = (ExposeOnSpawn = "true"))
		class ABaseCharacter* ActorChar;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menu", meta = (ClampMin = "0", ClampMax = "255"))
		int32 Row = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menu", meta = (ClampMin = "0", ClampMax = "255"))
		int32 Col = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menu", meta = (ClampMin = "0.0001", ClampMax = "1.0"))
		float pauseSpeed = 0.0001f;

	// Magic Logic
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MenuMagic")
		int32 MagicEquipScrl = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MenuMagic")
		int32 MagicEquipScrl_Max = 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "MenuMagic")
		bool bisMagicAssign = false;

	// Trees Logic
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menu", meta = (ClampMin = "0", ClampMax = "37")) // Clamp (10s are Rows, and 1s are Cols)
		int32 treesSelect = 0;

	// Menu Selection
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menu")
		FString MenuTab = "SKILLS";
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Menu")
		FString Selection = "";

	// Event Properties
	UFUNCTION(BlueprintImplementableEvent, Category = "Events")
		void RepawnToCheckpoint();

	// Input Selection/Function
	UFUNCTION(BlueprintCallable, Category = "Actions")
		void InputConfirm();
	UFUNCTION(BlueprintCallable, Category = "Actions")
		void InputResetSkillPred();
	UFUNCTION(BlueprintCallable, Category = "Actions")
		void Unpause();

	UFUNCTION(BlueprintCallable, Category = "Actions")
		void CheckKeyHeld(int32 key, float DeltaSeconds);

	int32 Key_itr = 0;
	int32 Key_Held = 0;

	APlayerController* PlayerController;
	TArray<FInputActionKeyMapping> OutMappings;
	UInputSettings* Settings;

	//UP / DOWN
	UFUNCTION(BlueprintCallable, Category = "Actions")
		void InputUp();
	UFUNCTION(BlueprintCallable, Category = "Actions")
		void InputDown();
	UFUNCTION(BlueprintImplementableEvent, Category = "Actions")
		void InputUp_BP();
	UFUNCTION(BlueprintImplementableEvent, Category = "Actions")
		void InputDown_BP();

	//LEFT / RIGHT
	UFUNCTION(BlueprintCallable, Category = "Actions")
		void InputLeft();
	UFUNCTION(BlueprintCallable, Category = "Actions")
		void InputRight();
	UFUNCTION(BlueprintImplementableEvent, Category = "Actions")
		void InputLeft_BP();
	UFUNCTION(BlueprintImplementableEvent, Category = "Actions")
		void InputRight_BP();

	// Input Switch Tab Functions
	UFUNCTION(BlueprintCallable, Category = "Actions")
		void InputTab_L();
	UFUNCTION(BlueprintCallable, Category = "Actions")
		void InputTab_R();
	UFUNCTION(BlueprintImplementableEvent, Category = "Actions")
		void InputTab_L_BP();
	UFUNCTION(BlueprintImplementableEvent, Category = "Actions")
		void InputTab_R_BP();

	//Switch Menu Select Type
	UFUNCTION(BlueprintCallable, Category = "Actions")
		void InputChangeMenuType();

	UFUNCTION(BlueprintImplementableEvent, Category = "Actions")
		void InputChangeMenuType_BP();

	UFUNCTION(BlueprintCallable, Category = "Actions")
		void ReturnCheckPoint();

	// Skill Point Properties
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Skills")
		int32 VitPredict = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Skills")
		int32 HastePredict = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Skills")
		int32 DexPredict = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Skills")
		int32 IntPredict = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Skills")
		int32 CurrentSpentPts = 0;

	// Stat Predictions
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Skills")
		float MaxHealthPredict = 0;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Skills")
		float MaxManaPredict = 0;

	//Sounds
	USoundCue* ConfirmSoundCue;
	USoundCue* PointClickSoundCue;
	USoundCue* BackSoundCue;
	USoundCue* OpenSoundCue;

	/*// Input Axis
	UFUNCTION(BlueprintCallable, Category = "Input")
		void ListenForAxisEvent(FName axis, FInputAxisDelegate func);
	void OnAxisValue(float value, FInputAxisDelegate func);

	// Logic Functions
	UFUNCTION()
		void ReturnToFill(float value);*/
};

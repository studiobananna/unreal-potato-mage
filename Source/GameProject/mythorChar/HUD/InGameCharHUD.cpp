// (C) Copyright Studio Bananna, all rights reserved


#include "InGameCharHUD.h"
#include "Kismet/GameplayStatics.h"

UInGameCharHUD::UInGameCharHUD(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer) {

	// Load Menu Sounds
	// CONFIRM
	static ConstructorHelpers::FObjectFinder <USoundCue>sConfirm(TEXT("SoundCue'/Game/Audio/SFX/menu/menu_select_Cue.menu_select_Cue'"));
	ConfirmSoundCue = sConfirm.Object;

	// USE POINTS
	static ConstructorHelpers::FObjectFinder <USoundCue>sPointClick(TEXT("SoundCue'/Game/Audio/SFX/menu/insertPoint_click01_Cue.insertPoint_click01_Cue'"));
	PointClickSoundCue = sPointClick.Object;

	// GO BACK
	static ConstructorHelpers::FObjectFinder <USoundCue>sBackSound(TEXT("SoundCue'/Game/Audio/SFX/menu/snd_menu_back_Cue.snd_menu_back_Cue'"));
	BackSoundCue = sBackSound.Object;

	// OPEN MENU
	static ConstructorHelpers::FObjectFinder <USoundCue>sOpenSound(TEXT("SoundCue'/Game/Audio/SFX/menu/snd_menuOpen_Cue.snd_menuOpen_Cue'"));
	OpenSoundCue = sOpenSound.Object;
}
void UInGameCharHUD::NativePreConstruct()
{
	Super::NativePreConstruct();
}

void UInGameCharHUD::NativeConstruct()
{
	Super::NativeConstruct();

	// Set the time dialation to pause
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), pauseSpeed);

	// Play Sound
	UAudioComponent* AudioComponent = UGameplayStatics::SpawnSound2D(this, OpenSoundCue, 1.25f);

	// Disable Player's Input
	ActorChar->DisableInput(UGameplayStatics::GetPlayerController(GetWorld(), 0));

	// Listen for Inputs
	// INPUT UnPause
	FOnInputAction UnPauseAction;
	UnPauseAction.BindUFunction(this, FName("Unpause"));
	ListenForInputAction(FName("Inventory"), IE_Pressed, false, UnPauseAction);
	ListenForInputAction(FName("MenuBackOut"), IE_Pressed, false, UnPauseAction);
	ListenForInputAction(FName("Pause"), IE_Pressed, false, UnPauseAction);

	// INPUT CONFIRM
	FOnInputAction ConfirmAction;
	ConfirmAction.BindUFunction(this, FName("InputConfirm"));
	ListenForInputAction(FName("Jump"), IE_Pressed, true, ConfirmAction);

	// INPUT UP
	FOnInputAction UpAction;
	UpAction.BindUFunction(this, FName("InputUp"));
	ListenForInputAction(FName("Menu_UP"), IE_Pressed, true, UpAction);

	// INPUT DOWN
	FOnInputAction DownAction;
	DownAction.BindUFunction(this, FName("InputDown"));
	ListenForInputAction(FName("Menu_DOWN"), IE_Pressed, true, DownAction);

	// INPUT LEFT
	FOnInputAction LeftAction;
	LeftAction.BindUFunction(this, FName("InputLeft"));
	ListenForInputAction(FName("Menu_LEFT"), IE_Pressed, true, LeftAction);

	// INPUT RIGHT
	FOnInputAction RightAction;
	RightAction.BindUFunction(this, FName("InputRight"));
	ListenForInputAction(FName("Menu_RIGHT"), IE_Pressed, true, RightAction);

	// INPUT Change Menu Tab RIGHT
	FOnInputAction TabRight;
	TabRight.BindUFunction(this, FName("InputTab_R"));
	ListenForInputAction(FName("Menu_TAB_RIGHT"), IE_Pressed, true, TabRight);

	// INPUT Change Menu Tab LEFT
	FOnInputAction TabLeft;
	TabLeft.BindUFunction(this, FName("InputTab_L"));
	ListenForInputAction(FName("Menu_TAB_LEFT"), IE_Pressed, true, TabLeft);

	// INPUT Toggle Menu Select
	FOnInputAction ChangeMenuAction;
	ChangeMenuAction.BindUFunction(this, FName("InputChangeMenuType"));
	ListenForInputAction(FName("ToggleMenuSelect"), IE_Pressed, true, ChangeMenuAction);

	// RETURN TO CHECKPOINT
	FOnInputAction ReturnCheckAction;
	ReturnCheckAction.BindUFunction(this, FName("ReturnCheckPoint"));
	ListenForInputAction(FName("CycleWep"), IE_Pressed, true, ReturnCheckAction);

	// INPUT RETURN TO FILL
	//FInputAxisDelegate ReturnAxis;
	//ReturnAxis.BindUFunction(this, FName("ReturnToFill"));
	//ListenForAxisEvent(FName("ReturnCheck"), ReturnAxis);

	// Play Any Animations
	PlayAnimation(SpawnCol_main, 0.f, 1, EUMGSequencePlayMode::Forward, 1.5f, false);	 // Main Column
	PlayAnimation(spr_target_anim01, 0.f, 0, EUMGSequencePlayMode::Forward, 1.f, false); // Hand Target Selector
	PlayAnimation(Spawn_Misc, 0.f, 1, EUMGSequencePlayMode::Forward, 1.5f, false);		 // Misc Anims

	// Get Player Controller and Input Settings
	PlayerController = GetWorld()->GetFirstPlayerController();
	Settings = const_cast<UInputSettings*>(GetDefault<UInputSettings>());
}

void UInGameCharHUD::NativeDestruct()
{
	Super::NativeDestruct();

	// Unpause the game
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1.f);

	// Enable Player's Input
	ActorChar->EnableInput(UGameplayStatics::GetPlayerController(GetWorld(), 0));

	// Give skillpoints back
	ActorChar->Inventory->Skillpoints += CurrentSpentPts;
	ActorChar->Inventory->Level -= CurrentSpentPts;

	// Prevent Character from launching
	ActorChar->GetCharacterMovement()->BrakingFrictionFactor = 100.f;
	ActorChar->GetCharacterMovement()->Velocity *= 0.5f;

	//Play Sound
	UAudioComponent* AudioComponent = UGameplayStatics::SpawnSound2D(this, BackSoundCue, 0.5f);
}

void UInGameCharHUD::NativeTick(const FGeometry & MyGeometry, float InDeltaTime)
{
	Super::NativeTick(MyGeometry, InDeltaTime);

	ActorChar->GetCharacterMovement()->BrakingFrictionFactor = 100.f;

	// If NOT Paused Check
	if (UGameplayStatics::GetGlobalTimeDilation(GetWorld()) != pauseSpeed) {
		// Reset the time dialation to pause if undone
		UGameplayStatics::SetGlobalTimeDilation(GetWorld(), pauseSpeed);
	}
	else // If Paused
	{
		// Disable Player's Input
		if (PlayerController) ActorChar->DisableInput(PlayerController);

		// Check if our owner Actor is in combat to allow teleporting to checkpoint
		// Increment Checkpoint Hold Counter
		if (bReturnFill && !ActorChar->bInCombat)
		{
			// Increment if less than 100
			if (HoldReturnVal < 100)
				HoldReturnVal += 50 * InDeltaTime;
			else if (HoldReturnVal >= 100) {
				//Get Location to Init Spawn
				FVector Location(0.f, 0.f, 0.f);
				FRotator Rotation(0.0f, 0.0f, 0.0f);
				FActorSpawnParameters SpawnInfo;

				// Spawn FadeActor
				RepawnToCheckpoint();

				// Remove from the actor to delete
				this->RemoveFromParent();
			}
		}
		else { // Decrement Checkpoint Hold Counter if not pressed
			if (HoldReturnVal > 0)
				HoldReturnVal -= 50 * InDeltaTime;
		}
	}

	// Force Stop Magic Animation
	if (!bisMagicAssign && IsAnimationPlaying(AssignMagics)) StopAnimation(AssignMagics);

	// Check if Keys are Held
	CheckKeyHeld(Key_Held, InDeltaTime);
}

void UInGameCharHUD::InputConfirm()
{
	if (MenuTab == "SKILLS") 
	{
		if (Selection == "CONFIRM") 
		{
			// Play Confirm Sound
			UAudioComponent* AudioComponent = UGameplayStatics::SpawnSound2D(this, ConfirmSoundCue, 0.25f);

			// Set new stats
			ActorChar->Inventory->vitality += VitPredict;
			ActorChar->Inventory->haste += HastePredict;
			ActorChar->Inventory->dexterity += DexPredict;
			ActorChar->Inventory->intelligence += IntPredict;

			// Reset Prediction Stats
			VitPredict = 0;
			HastePredict = 0;
			DexPredict = 0;
			IntPredict = 0;
			CurrentSpentPts = 0;

			// Set new health and mana
			ActorChar->LoadSkillStats();
		}
	}
	else if (MenuTab == "MAGICS" || MenuTab == "EQUIPMENT" || MenuTab == "ITEMS")
	{
		if (bisMagicAssign == false) {
			bisMagicAssign = true;
			PlayAnimation(AssignMagics, 0.f, 0, EUMGSequencePlayMode::Forward, 1.f, false);	 // Assign Equipped Magics

			// Play Confirm Sound
			UAudioComponent* AudioComponent = UGameplayStatics::SpawnSound2D(this, ConfirmSoundCue, 0.25f);
		}
		else {
			bisMagicAssign = false;

			// Play Back Sound
			UAudioComponent* AudioComponent = UGameplayStatics::SpawnSound2D(this, BackSoundCue, 0.75f);
		}
	}
}

void UInGameCharHUD::InputResetSkillPred()
{
	// Play Confirm Sound
	UAudioComponent* AudioComponent = UGameplayStatics::SpawnSound2D(this, ConfirmSoundCue, 0.25f);

	// Reset Prediction Stats
	VitPredict = 0;
	HastePredict = 0;
	DexPredict = 0;
	IntPredict = 0;
	CurrentSpentPts = 0;
}

void UInGameCharHUD::Unpause()
{
	if(!bisMagicAssign)
		RemoveFromParent();
}

void UInGameCharHUD::CheckKeyHeld(int32 key, float DeltaSeconds)
{
	switch (key) {
		case 0: Settings->GetActionMappingByName("Menu_UP", OutMappings);	break;
		case 1: Settings->GetActionMappingByName("Menu_DOWN", OutMappings); break;
		case 2: Settings->GetActionMappingByName("Menu_LEFT", OutMappings); break;
		case 3: Settings->GetActionMappingByName("Menu_RIGHT", OutMappings);break;
		case 4: Settings->GetActionMappingByName("Menu_TAB_LEFT", OutMappings); break;
		case 5: Settings->GetActionMappingByName("Menu_TAB_RIGHT", OutMappings);break;
		case 6: Settings->GetActionMappingByName("CycleWep", OutMappings); break;
	}

	bool flag = false; // Flag to execute the next statement once
	int32 i = 0;

	while(!flag && i < OutMappings.Num()) {
		if (PlayerController->IsInputKeyDown(FKey(OutMappings[i].Key.GetFName())))
		{
			flag = true;
			if (GEngine) GEngine->AddOnScreenDebugMessage(101, 1.0f, FColor::Yellow, FString::Printf(TEXT("DELTATIME - %f"), DeltaSeconds), true);
			if (GEngine) GEngine->AddOnScreenDebugMessage(100, 1.0f, FColor::Yellow, FString::Printf(TEXT("KEY HELD - %d"), Key_itr), true);
		}
		i++;
	}
	
	if (flag) {
		Key_itr += FMath::RoundToInt(100 * DeltaSeconds);

		if (Key_itr > 50 && key != 6) { // HOLD DIRECTIONAL BUTTONS
				Key_itr = 40;
				switch (key) {
				case 0: InputUp();   break;
				case 1: InputDown(); break;
				case 2: InputLeft(); break;
				case 3: InputRight(); break;
				case 4: InputTab_L(); break;
				case 5: InputTab_R(); break;
			}
		}
		if (key == 6) {
			if (ActorChar) {
				if (!ActorChar->bInCombat) {
					HoldReturnVal = Key_itr;
				}
			}

			if (HoldReturnVal > 150){	// HOLD RESPAWN BUTTON
				RepawnToCheckpoint();
				HoldReturnVal = 0;
			}
		}
	}
	else{Key_itr = 0;}
}

void UInGameCharHUD::InputUp()
{
	Row -= 1;
	InputUp_BP();
	Key_Held = 0;
}

void UInGameCharHUD::InputDown()
{
	Row += 1;
	InputDown_BP();
	Key_Held = 1;
}

void UInGameCharHUD::InputLeft()
{
	if (MenuTab == "SKILLS")
	{
		if (Selection == "VIT") {
			if (VitPredict > 0) {
				VitPredict--;
				CurrentSpentPts--;
				ActorChar->Inventory->Level--;
				ActorChar->Inventory->Skillpoints++;

				// Predict Health
				MaxHealthPredict = 50.f + (15.f * (VitPredict + ActorChar->Inventory->vitality));
			}
		}
		else if (Selection == "HST") {
			if (HastePredict > 0) {
				HastePredict--;
				CurrentSpentPts--;
				ActorChar->Inventory->Level--;
				ActorChar->Inventory->Skillpoints++;
			}
		}
		else if (Selection == "DEX") {
			if (DexPredict > 0) {
				DexPredict--;
				CurrentSpentPts--;
				ActorChar->Inventory->Level--;
				ActorChar->Inventory->Skillpoints++;
			}
		}
		else if (Selection == "INT") {
			if (IntPredict > 0) {
				IntPredict--;
				CurrentSpentPts--;
				ActorChar->Inventory->Level--;
				ActorChar->Inventory->Skillpoints++;

				// Predict Mana
				MaxManaPredict = 35.f + (7.f * (IntPredict + ActorChar->Inventory->intelligence));
			}
		}

		if (Selection == "INT" || Selection == "DEX" || Selection == "HST" || Selection == "VIT") {
			// Play Point Click Sound
			UAudioComponent* AudioComponent = UGameplayStatics::SpawnSound2D(this, PointClickSoundCue, 1.1f);
		}
	}

	Col--;
	InputLeft_BP();
	Key_Held = 2;
}

void UInGameCharHUD::InputRight()
{
	if (MenuTab == "SKILLS") 
	{
		if (Selection == "VIT") {
			if (ActorChar->Inventory->Skillpoints > 0) {
				VitPredict++;
				CurrentSpentPts++;
				ActorChar->Inventory->Level++;
				ActorChar->Inventory->Skillpoints--;

				// Predict Health
				MaxHealthPredict = 50.f + (15.f * (VitPredict + ActorChar->Inventory->vitality));
			}
		}
		else if (Selection == "HST") {
			if (ActorChar->Inventory->Skillpoints > 0) {
				HastePredict++;
				CurrentSpentPts++;
				ActorChar->Inventory->Level++;
				ActorChar->Inventory->Skillpoints--;
			}
		}
		else if (Selection == "DEX") {
			if (ActorChar->Inventory->Skillpoints > 0) {
				DexPredict++;
				CurrentSpentPts++;
				ActorChar->Inventory->Level++;
				ActorChar->Inventory->Skillpoints--;
			}
		}
		else if (Selection == "INT") {
			if (ActorChar->Inventory->Skillpoints > 0) {
				IntPredict++;
				CurrentSpentPts++;
				ActorChar->Inventory->Level++;
				ActorChar->Inventory->Skillpoints--;

				// Predict Mana
				MaxManaPredict = 35.f + (7.f * (IntPredict + ActorChar->Inventory->intelligence));
			}
		}

		if (Selection == "INT" || Selection == "DEX" || Selection == "HST" || Selection == "VIT") {
			// Play Point Click Sound
			UAudioComponent* AudioComponent = UGameplayStatics::SpawnSound2D(this, PointClickSoundCue, 0.75f);
		}
	}

	Col++;
	InputRight_BP();
	Key_Held = 3;
}

void UInGameCharHUD::InputTab_L() // Tab to the left
{
	// I wish there was a switch statement because if statements have been KILLING MEEEEEEE
	if (MenuTab == "EQUIPMENT")		 MenuTab = "SKILLS";
	else if (MenuTab == "MAGICS")	 MenuTab = "EQUIPMENT";
	else if (MenuTab == "ITEMS")	 MenuTab = "MAGICS";
	else if (MenuTab == "MAPQUESTS") MenuTab = "ITEMS";
	else MenuTab = "MAPQUESTS";
	Row = 0;
	Col = 0;
	bisMagicAssign = false;
	InputTab_L_BP();
	Key_Held = 4;

	// Set Skill Menu to Normal
	PlayAnimation(TabOutMainColumn, 0.f, 1, EUMGSequencePlayMode::Reverse, 1.f, false);	 // Main Column
	PlayAnimation(TabOutTrees, 0.f, 1, EUMGSequencePlayMode::Forward, 1.f, false);	 // Trees Tab
}

void UInGameCharHUD::InputTab_R() // Tab to the right
{
	// I'M SORRY :(
	if (MenuTab == "SKILLS")		 MenuTab = "EQUIPMENT";
	else if (MenuTab == "EQUIPMENT") MenuTab = "MAGICS";
	else if (MenuTab == "MAGICS")	 MenuTab = "ITEMS";
	else if (MenuTab == "ITEMS")	 MenuTab = "MAPQUESTS";
	else MenuTab = "SKILLS";
	Row = 0;
	Col = 0;
	bisMagicAssign = false;
	InputTab_R_BP();
	Key_Held = 5;

	// Set Skill Menu to Normal
	PlayAnimation(TabOutMainColumn, 0.f, 1, EUMGSequencePlayMode::Reverse, 1.f, false);	 // Main Column
	PlayAnimation(TabOutTrees, 0.f, 1, EUMGSequencePlayMode::Forward, 1.f, false);	 // Trees Tab
}

void UInGameCharHUD::InputChangeMenuType()
{
	if (ActorChar)
	{
		if (MenuTab == "SKILLS" && Selection != "SKILLTREES") {
			if (ActorChar->GameInst->PL_B_UNLOCKED_SKILLS)
			{
				PlayAnimation(TabOutMainColumn, 0.f, 1, EUMGSequencePlayMode::Forward, 1.f, false);	 // Main Column
				PlayAnimation(TabOutTrees, 0.f, 1, EUMGSequencePlayMode::Reverse, 1.f, false);	 // Trees Tab
				Selection = "SKILLTREES";
				Row = 0;
				InputUp_BP();
			}
		}
		else if (MenuTab == "SKILLS" && Selection == "SKILLTREES") {
			if (ActorChar->GameInst->PL_B_UNLOCKED_SKILLS)
			{
				PlayAnimation(TabOutMainColumn, 0.f, 1, EUMGSequencePlayMode::Reverse, 1.f, false);	 // Main Column
				PlayAnimation(TabOutTrees, 0.f, 1, EUMGSequencePlayMode::Forward, 1.f, false);	 // Trees Tab
				Selection = "VIT";
				Row = 4;
				InputUp_BP();
			}
		}
		else if (MenuTab == "MAGICS" || MenuTab == "ITEMS") {
			MagicEquipScrl += 1;
			if (MagicEquipScrl > MagicEquipScrl_Max) MagicEquipScrl = 0;
		}
	}

	InputChangeMenuType_BP();
}

void UInGameCharHUD::ReturnCheckPoint()
{
	Key_Held = 6;
}

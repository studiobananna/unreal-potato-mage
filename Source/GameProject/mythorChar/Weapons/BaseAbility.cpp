// (C) Copyright Studio Bananna, all rights reserved

#include "BaseAbility.h"
#include "mythorChar/BaseCharacter.h"

// Sets default values
ABaseAbility::ABaseAbility()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	//Set Root Component
	Root = CreateDefaultSubobject<USceneComponent>(TEXT("Root"));
	RootComponent = Root;
	//Get Hitbox
	Hitbox = CreateDefaultSubobject<UBaseHitbox>(TEXT("Hitbox"));
	Hitbox->SetupAttachment(Root);
	Hitbox->hasCharParent = charParent;
}

// Called when the game starts or when spawned
void ABaseAbility::BeginPlay()
{
	Super::BeginPlay();

	// Override Status Effects variables in the hitbox with these
	Hitbox->StatusEffects = StatusEffects;
	Hitbox->CanUseStatusEffects = bUseStatusEffects;

	// Set our parent if initialized
	if (Character) {
		SetParent(Character);
		Hitbox->HitFaction = Character->Faction;
		Hitbox->SetDamage((GetBaseDamage() * Character->LevelScale));
	}

	// If there is an inventory item, then subtract from its count
	if(InventoryItem) InventoryItem->SubtractCount();

	// Get Base Damage
	BaseDamage = Hitbox->GetBaseDamage();

	// Get our damage scaling factors
	SetDamage(GetBaseDamage());

	// If character is still alive
	if (Character) {
		if (Character->health <= 0)
		{
			Destroy();
		}
	}
	
}

void ABaseAbility::Destroyed()
{
	// End the slow down effect incase the object dies
	EndSlowDownEffect();
}

// Called every frame
void ABaseAbility::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

// Get the direction of the hitbox force
FVector ABaseAbility::GetDirection_Implementation()
{
	return Hitbox->HitDirection;
}

// Set if the hitbox is active or not (if not, then it does nothing)
void ABaseAbility::SetActiveHitbox(bool isActive)
{
	Hitbox->bIsActiveHit = isActive;

	if (parent) {
		Hitbox->CharParentCoords = GetParentCoords();
	}
}

// if there is a valid parent, then tell the parent that the hitbox was hit
void ABaseAbility::NotifyParentHit()
{
	// Go to Character Parent if exists and tell the hitbox hit something
	if (GetParent()) {
		ABaseCharacter* charPar = (ABaseCharacter*)GetParent();
		if(charPar)
			charPar->OnAttackHit(Hitbox);
	}
}

// Dealing damage with this ability
void ABaseAbility::SetDamage(float DamageAmount)
{
	// Get Character stats
	if (parent) {
		ABaseCharacter* charPar = (ABaseCharacter*)parent;

		float SkillScalingAmt = 0.f;

		//Scaling Factors
		SkillScalingAmt = DamageAmount + (static_cast<float>(charPar->Inventory->vitality) * 3.f * VitScaleGrade)
			+ (static_cast<float>(charPar->Inventory->haste) * 3.f * HastScaleGrade)
			+ (static_cast<float>(charPar->Inventory->dexterity) * 3.f * DexScaleGrade)
			+ (static_cast<float>(charPar->Inventory->intelligence) * 3.f * IntScaleGrade);

		// Set Elemental Skill Bonuses
		DamageAmount = SkillScalingAmt + (DamageAmount * charPar->CharHitboxDamageComp->physicalBNS * (Hitbox->bIsPhysical))
			+ (DamageAmount * charPar->CharHitboxDamageComp->fireBNS   * (Hitbox->bIsFire))
			+ (DamageAmount * charPar->CharHitboxDamageComp->natureBNS * (Hitbox->bIsNature))
			+ (DamageAmount * charPar->CharHitboxDamageComp->darkBNS   * (Hitbox->bIsDark))
			+ (DamageAmount * charPar->CharHitboxDamageComp->magicBNS  * (Hitbox->bIsMagic));
	}
	Hitbox->SetDamage(DamageAmount);
}

// Set the force of the ability
void ABaseAbility::SetForce(float ForceAmount)
{
	Hitbox->SetForce(ForceAmount);
}

// Set the base damage, disregarding any scaling or after effects
void ABaseAbility::SetBaseDamage(float BaseAmount)
{
	Hitbox->SetBaseDamage(BaseAmount);
}

// Set the launch angle of the ability
void ABaseAbility::SetAngle(float AngleAmount)
{
	Hitbox->SetAngle(AngleAmount);
}

// Set the parent of the ability for scaling factors and whatnot...
void ABaseAbility::SetParent(AActor* Parent)
{
	parent = Parent;					 // default actor casted parent
	Hitbox->parent = Parent;
	Character = (ABaseCharacter*)Parent; // Already BaseCharacter casted parent
	SetOwner(Parent);
}

void ABaseAbility::SetUsingStatusEffects(bool UsingStatusEffects)
{
	// Set variables in this class
	bUseStatusEffects = UsingStatusEffects;
	// Then in the hitbox
	Hitbox->CanUseStatusEffects = UsingStatusEffects;
}

// On hit with the ability
void ABaseAbility::OnHitEffect_Implementation()
{
	// After the Hitbox Interface Executes, Notify the parent
	NotifyParentHit();
}

// Slow Down effect implementation
void ABaseAbility::DoSlowDownEffect(float NewDilationSpeed, float Time)
{
	// Set the new Time Dilation
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), NewDilationSpeed);

	// Set the timer for how long to slowdown for
	GetWorld()->GetTimerManager().SetTimer(SlowDownTimerHandle, this, &ABaseAbility::EndSlowDownEffect, Time, false);
}

void ABaseAbility::EndSlowDownEffect()
{
	// Set to the default Time Dilation speed
	UGameplayStatics::SetGlobalTimeDilation(GetWorld(), 1.f);
}

void ABaseAbility::DoCameraShake(TSubclassOf<UCameraShakeBase> cameraShake, float innerRadius, float outerRadius, AActor* TargetOverride)
{
	//APlayerCameraManager* cameraManager = UGameplayStatics::GetPlayerCameraManager(GetWorld(), 0);
	if (Character){
		FVector location = FVector(0.f, 0.f, 0.f);

		if (Character->Camera) {
			// default location
			location = Character->Camera->GetComponentLocation();
		}
		if (TargetOverride) {
			// target override location
			location = TargetOverride->GetActorLocation();
		}

		UGameplayStatics::PlayWorldCameraShake(this, cameraShake, location, 200.f, 200.f);
	}
}

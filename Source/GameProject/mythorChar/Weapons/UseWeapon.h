// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Inventory/UseItem.h"
#include "mythorChar/Weapons/BaseWeapon.h"
#include "UseWeapon.generated.h"


UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent), Blueprintable)
class GAMEPROJECT_API UUseWeapon : public UUseItem
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UUseWeapon();

	//Weapon Spawn Default Objects
	UPROPERTY(EditAnywhere, BlueprintReadOnly)
		TSubclassOf<ABaseWeapon> WepSpawn;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
		
};

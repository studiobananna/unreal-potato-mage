// (C) Copyright Studio Bananna, all rights reserved


#include "BaseWeapon.h"
#include "Runtime/CoreUObject/Public/UObject/ConstructorHelpers.h"

// Sets default values
ABaseWeapon::ABaseWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	//Get Mesh
	Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("Mesh"));
	Mesh->SetupAttachment(Root);
	SetActiveHitbox(false);
}

bool ABaseWeapon::ComboEndEffect(float TimeDilation = 0.1f, float Time = 0.01f, float ForceThreshold = 10.f, 
	USoundCue* Sound = nullptr, UParticleSystem* ParticleSystem = nullptr)
{
	if (GetForce() >= ForceThreshold) 
	{
		// Do Combo End SlowDown
		DoSlowDownEffect(TimeDilation, Time);
		
		// Play Sound
		UGameplayStatics::PlaySound2D(this, Sound, 2.f, FMath::RandRange(1.0f, 1.2f), 0.f);

		// Spawn Particle System
		UGameplayStatics::SpawnEmitterAtLocation(this, ParticleSystem, GetActorLocation(), FRotator(0.f, 0.f, 0.f), true);

		return true;
	}

	return false;

}

// Called when the game starts or when spawned
void ABaseWeapon::BeginPlay()
{
	// OVERRIDES Base Damage in Hitbox Component
	SetBaseDamage(WepBaseDamage);

	Super::BeginPlay();
}

// Called every frame
void ABaseWeapon::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

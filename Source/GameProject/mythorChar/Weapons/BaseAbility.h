// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Hitbox/BaseHitbox.h"
#include "Effects/HitboxEffect.h"
#include "Sound/SoundCue.h"
#include "Kismet/GameplayStatics.h"
#include "BaseAbility.generated.h"

UCLASS()
class GAMEPROJECT_API ABaseAbility : public AActor, public IHitboxEffect
{
	GENERATED_BODY()
private:
	//Intitialize Vars
	AActor* parent = NULL;

public:	
	// Sets default values for this actor's properties
	ABaseAbility();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void Destroyed() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Base Stuff
	UPROPERTY()
		float BaseDamage = 0.f;

	//Root & Mesh
	UPROPERTY()
		USceneComponent* Root;
	//Opacity
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Material")
		float MatOpacity = 1.f;

	//Scaling
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
		float VitScaleGrade = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
		float HastScaleGrade = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
		float DexScaleGrade = 0.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox")
		float IntScaleGrade = 0.f;

	//Hitbox / Collision
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Hitbox", meta = (AllowPrivateAccess = "true"))
		UBaseHitbox* Hitbox;
	UPROPERTY(VisibleAnywhere, Category = "Trigger Capsule")
		UCapsuleComponent *TriggerCapsule;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Trigger Capsule")
		bool charParent = true;

	// Init Parent
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox", meta = (ExposeOnSpawn = "true"))
		class ABaseCharacter* Character = NULL;

	// Get Parent Item Component
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Item", meta = (ExposeOnSpawn = "true"))
		class UUseItem* InventoryItem = NULL;


	//Status Ailment Tsubclass Type (OVERRIDES THIS HITBOX'S STATUS EFFECTS ARRAY)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox", meta = (ExposeOnSpawn = "true"))
		TArray<TSubclassOf<UStatusEffectComponent>> StatusEffects;
	
	// See if it should be using status effects or not (MAINLY BASED ON SKILLS WHICH UNLOCK THEM)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Hitbox", meta = (ExposeOnSpawn = "true"))
		bool bUseStatusEffects = true;

	//Attacks / Active Hitbox
	UFUNCTION(BlueprintNativeEvent, BlueprintCallable, Category = "Battle")
		FVector GetDirection();
	virtual FVector GetDirection_Implementation() override;

	UFUNCTION()
		void SetActiveHitbox(bool isActive);

	UFUNCTION()
		void NotifyParentHit();

	//Get Set Functions for Vars
	int32 AttackDealt = -1;
	float GetDamage() { return round(Hitbox->GetDamage()); }
	void SetDamage(float DamageAmount);
	float GetForce() { return Hitbox->GetForce(); }

	void SetForce(float ForceAmount);
	float GetBaseDamage() { return Hitbox->GetBaseDamage(); }
	void SetBaseDamage(float BaseAmount);
	float GetAngle() { return Hitbox->GetAngle() * 0.085f; }
	void SetAngle(float AngleAmount);

	//Parent Functions and Vars
	UFUNCTION(BlueprintCallable, Category = "Battle")
		AActor* GetParent() { return parent; }
	UFUNCTION(BlueprintCallable, Category = "Battle")
		FVector GetParentCoords() { return parent->GetActorLocation(); }
	UFUNCTION(BlueprintCallable, Category = "Battle")
		void SetParent(AActor* Parent);

	// Set If Using Status Effects Function (Since it also affects this ability's hitbox)
	UFUNCTION(BlueprintCallable, Category = "Battle")
		void SetUsingStatusEffects(bool UsingStatusEffects);

	// Effect Functions
	// On Hit
	void OnHitEffect_Implementation() override;
	// Slow Down Effect
	FTimerHandle SlowDownTimerHandle;
	// Camera Shake
	class UCameraShakeBase* m_CameraShake;

	// NewDilationSpeed == The Speed at which the time is set; Time == For how long to slow down for
	UFUNCTION(BlueprintCallable)
		void DoSlowDownEffect(float NewDilationSpeed, float Time);
	UFUNCTION()
		void EndSlowDownEffect();

	// Do a World Camera Shake, but with player cam as default epicenter
	UFUNCTION(BlueprintCallable)
		void DoCameraShake(TSubclassOf<UCameraShakeBase> cameraShake, float innerRadius, float outerRadius, AActor* TargetOverride = nullptr);
};

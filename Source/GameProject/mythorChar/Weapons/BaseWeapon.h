// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Components/SkeletalMeshComponent.h"
#include "Hitbox/BaseHitbox.h"
#include "Components/CapsuleComponent.h"
#include "Effects/HitboxEffect.h"
#include "BaseAbility.h"
#include "BaseWeapon.generated.h"

UCLASS()
class GAMEPROJECT_API ABaseWeapon : public ABaseAbility
{
	GENERATED_BODY()
private:

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	
public:	
	// Sets default values for this actor's properties
	ABaseWeapon();

	//Root & Mesh
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category = "Mesh", meta = (AllowPrivateAccess = "true"))
		USkeletalMeshComponent* Mesh;

	// Weapon Stats
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (ExposeOnSpawn = "true"))
		int32 WeaponType = 1;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats", meta = (ExposeOnSpawn = "true"))
		float AttackSpeed = 1.f;

	// OVERRIDES the damage inside of the hitbox component, mainly used for weapons to calculate their base damage easier
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Stats")
		float WepBaseDamage = 1.f;

	// Moving Weapons Effects
	UFUNCTION(BlueprintImplementableEvent, Category = "Events")
		void OnMoveWep();

	// Combo End Effect
	UFUNCTION(BlueprintCallable)
		bool ComboEndEffect(float TimeDilation, float Time, float ForceThreshold, USoundCue* Sound, UParticleSystem* ParticleSystem);
};

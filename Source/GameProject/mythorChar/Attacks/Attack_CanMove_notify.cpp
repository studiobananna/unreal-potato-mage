// (C) Copyright Studio Bananna, all rights reserved

#include "Attack_CanMove_notify.h"
#include "mythorChar/mythorChar.h"
#include "Engine.h"

void UAttack_CanMove_notify::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) {

	if (MeshComp != NULL && MeshComp->GetOwner() != NULL)
	{
		AmythorChar* Player = Cast<AmythorChar>(MeshComp->GetOwner());
		if (Player != NULL)
		{
			Player->AllowMovement = true;
			Player->WepAttackingComponent->m_AttackComboType = 1;
		}
	}
}
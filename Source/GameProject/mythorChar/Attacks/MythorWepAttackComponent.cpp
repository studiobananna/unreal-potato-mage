// (C) Copyright Studio Bananna, all rights reserved


#include "MythorWepAttackComponent.h"
#include "../Magic/MythorMagicComponent.h"
#include "../mythorChar.h"

// Sets default values for this component's properties
UMythorWepAttackComponent::UMythorWepAttackComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UMythorWepAttackComponent::BeginPlay()
{
	Super::BeginPlay();

	// Get character owner
	Character = (AmythorChar*)GetOwner();

	// Get Game Inst
	GameInst = Character->GameInst;

	if (Character) {
		Character->WepAttackingComponent = this;
	}
	
}


// Called every frame
void UMythorWepAttackComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

/*-----------------Attacking Weapon Functions----------------*/
float UMythorWepAttackComponent::ModifyDamage(float BaseDamage, float Modifier) {
	//Do the modifier
	return BaseDamage * Modifier;
}


// Reset Weapon's Hitbox to default properties
void UMythorWepAttackComponent::ResetWeaponToDefaults() {
	//Weapon Attack Stat Modifiers to Default
	if (m_Wep1) {
		m_Wep1->SetDamage(m_Wep1->WepBaseDamage);
		m_Wep1->SetForce(1);
		m_Wep1->Hitbox->ClearHitArry();
	}		// Empty the hitbox array
	if (m_Wep2) {
		m_Wep2->SetDamage(m_Wep2->WepBaseDamage);
		m_Wep2->SetForce(1);
		m_Wep2->Hitbox->ClearHitArry();
	}		// Empty the hitbox array
}


void UMythorWepAttackComponent::SetAttackStats(float DamageMod1, float DamageMod2, float Force1, float Force2, float Angle = 0.f) {
	//Weapon Attack Stat Modifiers
	if (m_Wep1) {
		float damageVal = m_Wep1->WepBaseDamage;
		m_Wep1->SetDamage(ModifyDamage(damageVal, DamageMod1));
		m_Wep1->SetForce(Force1);
		m_Wep1->SetAngle(Angle);
	}
	if (m_Wep2) {
		float damageVal = m_Wep2->WepBaseDamage;
		m_Wep2->SetDamage(ModifyDamage(damageVal, DamageMod2));
		m_Wep2->SetForce(Force2);
		m_Wep2->SetAngle(Angle);
	}
}


void UMythorWepAttackComponent::DoAttack(float BrakingFriction, float AttackNumber, float Wep1RDamageMod,
	float Wep2LDamageMod, float Wep1RForceMod, float Wep2LForceMod, float Angle) {

	if (!Character) return;

	// Set Braking Friction
	Character->GetCharacterMovement()->BrakingFrictionFactor = BrakingFriction;

	// Set Attack Number
	m_AttackType = AttackNumber;

	// Print the Attack Type
	UE_LOG(LogTemp, Warning, TEXT("%d"), m_AttackType);

	//Weapon Attack Stat Modifiers
	SetAttackStats(Wep1RDamageMod, Wep2LDamageMod, Wep1RForceMod, Wep2LForceMod, Angle);
}


void UMythorWepAttackComponent::SetWepIfAttacking(bool bIsAttackWep1, bool bIsAttackWep2) {
	//Weapon Attack Stat Modifiers
	if (m_Wep1) { m_Wep1->SetActiveHitbox(bIsAttackWep1); }
	if (m_Wep2) { m_Wep2->SetActiveHitbox(bIsAttackWep2); }
}


void UMythorWepAttackComponent::LoadCharacterSpawnWeapons()
{
	if (GameInst) {
		m_bisBattle ? SpawnWeapon(GameInst->MYTH_wep, false) : SpawnWeapon(GameInst->MYTH_wep, true);
		m_WeaponCycleSpawn = GameInst->MYTH_wep;
	}
}


void UMythorWepAttackComponent::SetWepOpacity(float amount) {
	if (m_Wep1) { m_Wep1->MatOpacity = amount; }
	if (m_Wep2) { m_Wep2->MatOpacity = amount; }
}


void UMythorWepAttackComponent::CycleWeapons() {
	if (!Character) return;
	if (Character->EventUseTarget) return;

	// Only allow cycling weapons if not casting any magic, and no forces are applied
	if (Cast<UMythorMagicComponent>(Character->MagicAbilityComponent)->MagicType == 0 
		&& !Character->m_AllowRollingForce && !Character->m_AllowAttackForce)
	{
		(m_WeaponCycleSpawn < m_WepCycleAmtMax && m_WeaponCycleSpawn < Character->Inventory->EquipWeaponArry.Num())
			? m_WeaponCycleSpawn++ : m_WeaponCycleSpawn = 1;
		SpawnWeapon(m_WeaponCycleSpawn, false);
	}
}


void UMythorWepAttackComponent::AutoSheathe() {
	if (!Character->bInCombat) 
	{
		if (!Character->isAttacking && !m_isSheathed)
			MoveWeapon(false);
	}
	else if (Character->MagicAbilityComponent)
	{
		if (!Cast<UMythorMagicComponent>(Character->MagicAbilityComponent)->bisSprinting)
			MoveWeapon(true);
	}
}


void UMythorWepAttackComponent::MoveWeapon(bool isBattle) {
	//Create Socket Names
	FName fnWeaponSocket = TEXT("Hand_RSocket");
	FName fnWeaponSocket01 = TEXT("Hand_LSocket");

	// Sheathe after timer
	GetWorld()->GetTimerManager().SetTimer(SheatheWepTimer, this, &UMythorWepAttackComponent::AutoSheathe, 3.25f, false);

	//Check if we are setting to battle or not
	if (isBattle) {
		//Change for GS
		if (m_Wep1) {
			if (m_Wep1->WeaponType == EWeapon::Greatsword) {
				fnWeaponSocket = TEXT("Hand_GreatRSocket");
			}
		}
		//Attach the objects
		if (m_Wep1) {
			m_Wep1->AttachToComponent(Character->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, fnWeaponSocket);
		}
		if (m_Wep2) {
			m_Wep2->AttachToComponent(Character->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, fnWeaponSocket01);
		}
		m_isSheathed = false; 
		m_bisBattle = true; 
		// Update battle stance
		Character->BattleStance();
	}

	//Not Battle
	else {
		//Set names to sheathe local
		fnWeaponSocket = TEXT("Back_RSocket");
		fnWeaponSocket01 = TEXT("Back_LSocket");

		//Change for GS
		if (m_Wep1) {
			if (m_Wep1->WeaponType == EWeapon::Greatsword) {
				fnWeaponSocket = TEXT("Back_RSocket_0");
			}
		}
		//Attach objects
		if (m_Wep1) {
			m_Wep1->AttachToComponent(Character->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, fnWeaponSocket);
			m_Wep1->OnMoveWep(); // Weapon Movement Effects wep 1
		}
		if (m_Wep2) {
			m_Wep2->AttachToComponent(Character->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, fnWeaponSocket01);
			m_Wep2->OnMoveWep(); // Weapon Movement Effects wep 2
		}
		m_isSheathed = true; 
		m_bisBattle = false; 
		// Update battle stance
		Character->BattleStance();
	}
}


void UMythorWepAttackComponent::SpawnWeapon(int spawnType, bool sheathe) {
	if (!Character) return;

	Character->CancelAction();

	if (Character->AllowMovement) {
		m_WeaponCycleSpawn = spawnType;

		//Get Location to Init Spawn
		FVector Location = Character->HitboxCapsule->GetComponentLocation();
		FRotator Rotation(0.0f, 0.0f, 0.0f);
		FActorSpawnParameters SpawnInfo;

		//Check if Weapon exists, if so, delete it
		if (m_Wep1 != NULL) {
			m_Wep1->Destroy();
		}
		if (m_Wep2 != NULL) {
			m_Wep2->Destroy();
		}

		//Get our weapon we want to choose
		TSubclassOf<ABaseWeapon> Spawn1;
		TSubclassOf<ABaseWeapon> Spawn2;

		//Spawn Desired Weapon in each slot
		if (Character->Inventory) 
		{
			// ---------------Setup Weapons from Inventory Weapon Slots---------------------
			if (Character->Inventory->EquipWeaponArry.Num() > 0) 
			{
				for (int i = 0; i < m_WepCycleAmtMax; i++) {								// Loops for each weapon slot
					if (m_WeaponCycleSpawn == i + 1 && Character->Inventory->EquipWeaponArry[i]) {
						UUseWeapon* InvWep1 = Character->Inventory->EquipWeaponArry[i];		// Get the weapon in slot 1 in inventory

						// Check the weapon type (This case 1 ==Dual Daggers)
						if (InvWep1->WepSpawn.GetDefaultObject()->WeaponType == EWeapon::DualDaggers) {
							Spawn1 = InvWep1->WepSpawn; Spawn2 = InvWep1->WepSpawn;	// Set both weapons to the daggers
							m_AttackComboMax = 4;										// Set the combo to the amount for the weapon type (should redo this...)
						}

						// Check the weapon type (This case 2 == Greatsword)
						else if (InvWep1->WepSpawn.GetDefaultObject()->WeaponType == EWeapon::Greatsword) {
							Spawn1 = InvWep1->WepSpawn; Spawn2 = NULL;				// ...and do the same as before this time with 3 as the max combo
							m_AttackComboMax = 3;
						}
						m_AttackSpeed = InvWep1->WepSpawn.GetDefaultObject()->AttackSpeed;
					}
				}
			}
		}

		//Spawn Weapons
		//Right Weapon
		if (Spawn1) {
			m_Wep1 = GetWorld()->SpawnActor<ABaseWeapon>(Spawn1, Location, Rotation, SpawnInfo);
			m_Wep1->SetParent(Character);
			m_Wep1->Hitbox->HitFaction = Character->Faction;
			m_Wep1->Hitbox->bIsActiveHit = false;
		}

		//Left Weapon
		if (Spawn2) {
			m_Wep2 = GetWorld()->SpawnActor<ABaseWeapon>(Spawn2, Location, Rotation, SpawnInfo);
			m_Wep2->SetParent(Character);
			m_Wep2->Hitbox->HitFaction = Character->Faction;
			m_Wep2->Hitbox->bIsActiveHit = false;
		}

		if (!sheathe) {
			//Set Battle Stance
			m_bisBattle = true;
			Character->BattleStance();

			//Move Weapons to hands
			MoveWeapon(true);

			m_isSheathed = false;
		}
		// If sheathing weapons
		else {
			//Set Battle Stance
			m_bisBattle = false;
			Character->BattleStance();

			//Move Weapons to back
			MoveWeapon(false);

			m_isSheathed = true;
		}
	}
}


// Spawning Weapons in the cycle
void UMythorWepAttackComponent::SpawnWeapon001() {
	m_isSheathed ? SpawnWeapon(1, false) : SpawnWeapon(1, true);
}
void UMythorWepAttackComponent::SpawnWeapon002() {
	m_isSheathed ? SpawnWeapon(2, false) : SpawnWeapon(2, true);
}


// 
// Resets the weapon hitbox for attacks that hit multiple times
void UMythorWepAttackComponent::AttackResetWepHitbox() {
	if (m_Wep1) {
		m_Wep1->Hitbox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		m_Wep1->Hitbox->ClearHitArry();
		m_Wep1->Hitbox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
	if (m_Wep2) {
		m_Wep1->Hitbox->SetCollisionEnabled(ECollisionEnabled::NoCollision);
		m_Wep2->Hitbox->ClearHitArry();
		m_Wep1->Hitbox->SetCollisionEnabled(ECollisionEnabled::QueryOnly);
	}
}


/*-----------------Attacking Start----------------*/
void UMythorWepAttackComponent::AttackingStart() {
	/*-----Grounded Attacks-----*/
	if (!Character->isAttacking && Character->WepSpecialAttackComponent->SPAttackType == 0 &&
		Cast<UMythorMagicComponent>(Character->MagicAbilityComponent)->MagicType == 0 && !Character->InHurtAnim && m_Wep1)
	{
		//In Battle
		m_bisBattle = true;
		Character->BattleStance();

		//Move Weapons to hands
		MoveWeapon(true);

		UE_LOG(LogTemp, Warning, TEXT("You Attacked!"));

		//Add to Attacking Queue
		m_AttackQueue += 1;

		//Set is attacking and disable movement
		Character->AllowRot = true;
		Character->isAttacking = true;
		Character->AllowMovement = false;
		Cast<UMythorMagicComponent>(Character->MagicAbilityComponent)->MagicType = 0;

		//Check Type Value
		if(m_AttackType < 0) m_AttackType = 0;

		//Increase AttackType
		// On Ground
		if (Character->GetCharacterMovement()->IsMovingOnGround()) {
			// Reset if at max combo
			if (m_AttackQueue > m_AttackComboMax) {
				//Set Attacking Queue to 0
				m_AttackQueue = 0;
				m_AttackType = 0;
				m_AttackComboType = 1;

				//Set is attacking and disable movement
				Character->AllowRot = false;
				Character->isAttacking = false;
				Character->AllowMovement = true;
			}
			FVector Vel = Character->GetCharacterMovement()->Velocity;
			Character->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.6f, Vel.Y * 0.6f, Vel.Z * 0.6f);

			//Execute Attack
			Attacks(m_AttackQueue);
		}
		/*-----Aerial Attacks-----*/
		else {
			//Air Attacks Movement Initialize
			//Initialize Movement by stopping velocity, lowering gravity
			if (m_Wep1 || m_Wep2) {
				FVector Vel = Character->GetCharacterMovement()->Velocity;
				Character->GetCharacterMovement()->Velocity = FVector(Vel.X * 0.1f, Vel.Y * 0.1f, Vel.Z * 0.1f);
				Character->GetCharacterMovement()->GravityScale = 1.f;
			}

			// Reset if at max combo aerial
			if (m_AttackQueue > m_AttackAirComboMax) {
				//Set Attacking Queue to 0
				m_AttackQueue = 0;
				m_AttackType = 0;

				//Set is attacking and disable movement
				Character->AllowRot = false;
				Character->isAttacking = false;
				Character->AllowMovement = true;
			}

			//Execute Aerial Attack
			Air_Attacks(m_AttackQueue);
		}
		// Execute Animations
		BP_Attacks(m_AttackType);
	}
}


/*-------Grounded Attacks-------*/
void UMythorWepAttackComponent::Attacks(int type) {

	if (!Character->AllowMovement && Character->isAttacking && m_Wep1) {
		// Dagger Attack Combos--------------------------------
		if (m_Wep1->WeaponType == EWeapon::DualDaggers && m_AttackComboType == 2) { 
			type += 4; 
			m_AttackComboMax = 3;		// Combo 2 Daggers
		}
		else if (m_Wep1->WeaponType == EWeapon::DualDaggers) { 
			m_AttackComboMax = 4;			// Combo 1 Daggers
		}
		// Greatsword Attack Combos--------------------------------
		if (m_Wep1->WeaponType == EWeapon::Greatsword && m_AttackComboType == 2) { 
			type += 12;
			m_AttackComboMax = 1;			// Combo 2 GS
		}
		else if (m_Wep1->WeaponType == EWeapon::Greatsword) { 
			type += 9; 
			m_AttackComboMax = 3;			// Combo 1 GS
		}

		// Get our attacks based on our Attacking Component Class
		GetCharAttackTypeStats(type);

		// Then do the Attack with those stats
		DoAttack(m_BrakingFriction, m_AttackNum, m_Damage1Mod, m_Damage2Mod, m_Force1Mod, m_Force2Mod, m_Angle);
	}
}


/*-------Aerial Attacks-------*/
void UMythorWepAttackComponent::Air_Attacks(int type) {
	if (!Character->AllowMovement && Character->isAttacking && m_Wep1) {
		// Change attack number based on weapon
		if (m_Wep1->WeaponType == 2) { 
			type += 9; 
		}

		// Add forward impulse on attack
		if (Character->TargetLockOn && Character->TargetLocked)
			Character->GetCharacterMovement()->AddImpulse((Character->TargetLockOn->GetActorLocation() - 
				Character->GetActorLocation()).GetSafeNormal() * FVector(1.f, 1.f, 0.f) * FMath::Pow(10, 4.62));
		else
			Character->GetCharacterMovement()->AddImpulse(Character->GetActorForwardVector() * 1 * FMath::Pow(10, 4.62));

		// Get our attacks based on our Attacking Component Class
		GetCharAttackAerialStats(type);

		// Then do the Attack with those stats
		DoAttack(m_BrakingFriction, m_AttackNum, m_Damage1Mod, m_Damage2Mod, m_Force1Mod, m_Force2Mod, m_Angle);

	}
}

void UMythorWepAttackComponent::OnAttackHit(UBaseHitbox* Hitbox) {

	// If Attacked with a Weapon then check for lifesteal skill
	/*if (Hitbox->GetOwner()->GetClass()->IsChildOf(ABaseWeapon::StaticClass())) {
		if (Character->PhysAttackLifeSteal) {
			Character->health += Hitbox->damage * 0.009f;
		}
	}*/
}

void UMythorWepAttackComponent::GetCharAttackTypeStats(int AttackType)
{
	switch (AttackType) {
		/*-----------------------------DAGGER ATTACKS-----------------------------*/
	//ATTACK01
	case 1:
		// Variables for Stats 001
		m_BrakingFriction = 4.f; m_AttackNum = 1; m_Damage1Mod = 0.5f;
		m_Damage2Mod = 0.5f; m_Force1Mod = 2.f; m_Force2Mod = 0.f; m_Angle = 0.f;
		break;

		//ATTACK02
	case 2:
		// Variables for Stats 002
		m_BrakingFriction = 4.f; m_AttackNum = 2; m_Damage1Mod = 0.6f;
		m_Damage2Mod = 0.6f; m_Force1Mod = 5.75f * 0.1f; m_Force2Mod = 3.75f; m_Angle = 0.f;
		break;

		//ATTACK03
	case 3:
		// Variables for Stats 003
		m_BrakingFriction = 2.f; m_AttackNum = 3; m_Damage1Mod = 0.7f;
		m_Damage2Mod = 0.9f; m_Force1Mod = 0.f; m_Force2Mod = 6.f; m_Angle = 0.f;
		break;

		//ATTACK04
	case 4:
		// Variables for Stats 004
		m_BrakingFriction = 2.f; m_AttackNum = 4; m_Damage1Mod = 4.5f;
		m_Damage2Mod = 4.55f; m_Force1Mod = 18.6f; m_Force2Mod = 0.f; m_Angle = 4.f;
		break;

		// -----------COMBO 2--------------
		//ATTACK05
	case 5:
		// Variables for Stats 005
		m_BrakingFriction = 2.f; m_AttackNum = 5; m_Damage1Mod = 0.25f;
		m_Damage2Mod = 0.5f; m_Force1Mod = 3.f; m_Force2Mod = 3.f; m_Angle = 0.f;
		break;

		//ATTACK06
	case 6:

		// Variables for Stats 006
		m_BrakingFriction = 2.f; m_AttackNum = 6; m_Damage1Mod = 0.25f;
		m_Damage2Mod = 0.5f; m_Force1Mod = 0.f; m_Force2Mod = 4.75f; m_Angle = 0.f;
		break;

		//ATTACK07
	case 7:
		// Variables for Stats 007
		m_BrakingFriction = 2.f; m_AttackNum = 7; m_Damage1Mod = 1.25f * 0.5f;
		m_Damage2Mod = 1.25f; m_Force1Mod = 0.f; m_Force2Mod = 12.5f; m_Angle = 10.f;
		break;

		/*-----------------------------GREATSWORD ATTACKS-----------------------------*/
	//ATTACK01
	case 10:
		// Variables for Stats 010
		m_BrakingFriction = 2.f; m_AttackNum = 10; m_Damage1Mod = 2.f;
		m_Damage2Mod = 0.f; m_Force1Mod = 5.f; m_Force2Mod = 0.f; m_Angle = 0.f;
		break;

		//ATTACK02
	case 11:
		// Variables for Stats 011
		m_BrakingFriction = 2.f; m_AttackNum = 11; m_Damage1Mod = 3.2f;
		m_Damage2Mod = 0.f; m_Force1Mod = 8.f; m_Force2Mod = 0.f; m_Angle = 0.5f;
		break;

		//ATTACK03
	case 12:
		// Variables for Stats 012
		m_BrakingFriction = 1.5f; m_AttackNum = 12; m_Damage1Mod = 7.75f;
		m_Damage2Mod = 0.f; m_Force1Mod = 18.f; m_Force2Mod = 0.f; m_Angle = 3.f;
		break;

		// ---------------COMBO 2---------------
	case 13:
		// Variables for Stats 013
		m_BrakingFriction = 1.5f; m_AttackNum = 13; m_Damage1Mod = 2.25f;
		m_Damage2Mod = 0.f; m_Force1Mod = 12.f; m_Force2Mod = 0.f; m_Angle = 2.f;
		break;
	default:
		break;
	}
}


void UMythorWepAttackComponent::GetCharAttackAerialStats(int AttackType)
{
	switch (AttackType) 
	{
		/*-----------------------------DAGGER AIR ATTACKS-----------------------------*/
	//AIR ATTACK01
	case 1:
		// Variables for Stats 001
		m_BrakingFriction = 1.f; m_AttackNum = 1; m_Damage1Mod = 0.4f;
		m_Damage2Mod = 0.35f; m_Force1Mod = 5.4f; m_Force2Mod = 0.7f; m_Angle = 0.f;
		break;

		//AIR ATTACK02
	case 2:
		// Variables for Stats 002
		m_BrakingFriction = 1.f; m_AttackNum = 2; m_Damage1Mod = 0.4f;
		m_Damage2Mod = 0.45f; m_Force1Mod = 5.8f; m_Force2Mod = 0.3f; m_Angle = 0.f;
		break;

		//AIR ATTACK03
	case 3:
		// Variables for Stats 003
		m_BrakingFriction = 1.f; m_AttackNum = 3; m_Damage1Mod = 3.4f;
		m_Damage2Mod = 3.15f; m_Force1Mod = 25.f; m_Force2Mod = 20.f; m_Angle = -4.5f;
		break;

		/*-----------------------------GREATSWORD AIR ATTACKS-----------------------------*/
		//AIR ATTACK01
	case 10:
		// Variables for Stats 010
		m_BrakingFriction = 1.f; m_AttackNum = 10; m_Damage1Mod = 1.65f;
		m_Damage2Mod = 0.f; m_Force1Mod = 3.f; m_Force2Mod = 0.f; m_Angle = 3.f;
		break;

		//AIR ATTACK02
	case 11:
		// Variables for Stats 011
		m_BrakingFriction = 1.f; m_AttackNum = 11; m_Damage1Mod = 1.65f;
		m_Damage2Mod = 0.f; m_Force1Mod = 3.5f; m_Force2Mod = 0.f; m_Angle = 3.f;
		break;

		//AIR ATTACK03
	case 12:
		// Variables for Stats 012
		m_BrakingFriction = 1.f; m_AttackNum = 12; m_Damage1Mod = 4.1f;
		m_Damage2Mod = 0.f; m_Force1Mod = 12.1f; m_Force2Mod = 0.f; m_Angle = 4.f;
		break;

		//AIR ATTACK03 (LANDED)
	case 13:
		// Variables for Stats 013
		m_BrakingFriction = 1.f; m_AttackNum = 13; m_Damage1Mod = 2.4f;
		m_Damage2Mod = 0.f; m_Force1Mod = 15.0f; m_Force2Mod = 0.f; m_Angle = 5.f;
		break;
	default:
		break;
	}
}
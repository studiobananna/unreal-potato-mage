// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Animation/AnimNotifies/AnimNotifyState.h"
#include "Attack01_anim_notifyState.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API UAttack01_anim_notifyState : public UAnimNotifyState //<---Look at this for help!
{
	GENERATED_BODY()

public:
	virtual void NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase * Animation, float TotalDuration) override;
	virtual void NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase * Animation, float FrameDeltaTime) override;
	virtual void NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase * Animation) override;
	int32 initAttack;
	int32 initSPAttack;
};

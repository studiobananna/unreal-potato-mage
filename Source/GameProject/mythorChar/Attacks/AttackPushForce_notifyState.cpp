// (C) Copyright Studio Bananna, all rights reserved


#include "mythorChar/Attacks/AttackPushForce_notifyState.h"
#include "mythorChar/BaseCharacter.h"

void UAttackPushForce_notifyState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float TotalDuration) {

}

void UAttackPushForce_notifyState::NotifyTick(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation, float FrameDeltaTime) {
	if (MeshComp != NULL && MeshComp->GetOwner() != NULL)
	{
		// Get the character this anim notify is targetting
		ABaseCharacter* Character = Cast<ABaseCharacter>(MeshComp->GetOwner());

		// See if said character is null or not
		if (Character != NULL)
		{
			// Set velocity and frictions
			Character->m_AllowAttackForce = true;
		}
	}
}

void UAttackPushForce_notifyState::NotifyEnd(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation)
{
	if (MeshComp != NULL && MeshComp->GetOwner() != NULL)
	{
		// Get the character this anim notify is targetting
		ABaseCharacter* Character = Cast<ABaseCharacter>(MeshComp->GetOwner());

		// See if said character is null or not
		if (Character != NULL)
		{
			// Set velocity and frictions
			Character->m_AllowAttackForce = false;
		}
	}
}
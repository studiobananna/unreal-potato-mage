// (C) Copyright Studio Bananna, all rights reserved


#include "RollingState.h"
#include "mythorChar/mythorChar.h"
#include "Engine.h"

void URollingState::NotifyBegin(USkeletalMeshComponent* MeshComp, UAnimSequenceBase * Animation, float TotalDuration) {
	if (MeshComp != NULL && MeshComp->GetOwner() != NULL)
	{
		ABaseCharacter* Player = Cast<ABaseCharacter>(MeshComp->GetOwner());
		if (Player != NULL)
		{
			Player->m_AllowRollingForce = true;
		}
	}
}

void URollingState::NotifyTick(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float FrameDeltaTime) {

}

void URollingState::NotifyEnd(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation)
{
	if (MeshComp != NULL && MeshComp->GetOwner() != NULL)
	{
		ABaseCharacter* Player = Cast<ABaseCharacter>(MeshComp->GetOwner());
		if (Player != NULL)
		{
			Player->m_AllowRollingForce = false;
		}
	}
}

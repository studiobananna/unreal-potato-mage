// (C) Copyright Studio Bananna, all rights reserved

#include "Attack01_anim_notifyState.h"
#include "mythorChar/mythorChar.h"
#include "Engine.h"

void UAttack01_anim_notifyState::NotifyBegin(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float TotalDuration) {

	if (MeshComp != NULL && MeshComp->GetOwner() != NULL)
	{
		AmythorChar* Player = Cast<AmythorChar>(MeshComp->GetOwner());
		if (Player != NULL && !Player->isRolling)
		{
			initAttack = Player->WepAttackingComponent->m_AttackType;
			initSPAttack = Player->WepSpecialAttackComponent->SPAttackType;
			Player->isAttacking = false;
			Player->WepAttackingComponent->ResetWeaponToDefaults();
			Player->WepAttackingComponent->SetWepIfAttacking(false, false);
		}
	}
}

void UAttack01_anim_notifyState::NotifyTick(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation, float FrameDeltaTime) {
}

void UAttack01_anim_notifyState::NotifyEnd(USkeletalMeshComponent * MeshComp, UAnimSequenceBase * Animation) {

	if (MeshComp != NULL && MeshComp->GetOwner() != NULL)
	{
		AmythorChar* Player = Cast<AmythorChar>(MeshComp->GetOwner());
		if (Player != NULL)
		{
			if (Player->WepAttackingComponent->m_AttackType == initAttack && 
				Player->WepSpecialAttackComponent->SPAttackType == initSPAttack && !Player->isRolling)
			{
				Player->WepAttackingComponent->m_AttackType = 0;
				Player->WepSpecialAttackComponent->SPAttackType = 0;
				Player->WepAttackingComponent->m_AttackQueue = 0;
				Player->isAttacking = false;
				Player->AllowMovement = true;
			}
		}
	}
}
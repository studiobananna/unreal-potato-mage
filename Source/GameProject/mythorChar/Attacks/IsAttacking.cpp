// (C) Copyright Studio Bananna, all rights reserved


#include "IsAttacking.h"
#include "mythorChar/mythorChar.h"

void UIsAttacking::Notify(USkeletalMeshComponent* MeshComp, UAnimSequenceBase* Animation) {

	if (MeshComp != NULL && MeshComp->GetOwner() != NULL)
	{
		AmythorChar* Player = Cast<AmythorChar>(MeshComp->GetOwner());
		if (Player != NULL && Player->isAttacking)
		{
			Player->WepAttackingComponent->SetWepIfAttacking(true, true);
		}
	}
}
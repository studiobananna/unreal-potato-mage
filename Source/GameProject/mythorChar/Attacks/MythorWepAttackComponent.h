// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "../Weapons/BaseAbility.h"
#include "../Weapons/UseWeapon.h"
#include "PotatoGameInstance.h"
#include "MythorWepAttackComponent.generated.h"

UENUM(BlueprintType)
enum EWeapon {
	NoneWeapon			UMETA(DisplayName = "None"),
	DualDaggers			UMETA(DisplayName = "DualDaggers"),
	Greatsword			UMETA(DisplayName = "Greatsword"),
};


UCLASS(Blueprintable, BlueprintType, ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class GAMEPROJECT_API UMythorWepAttackComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	//Attack General
	UPROPERTY() FTimerHandle SheatheWepTimer;

	UPROPERTY()
		class AmythorChar* Character;

public:	
	// Sets default values for this component's properties
	UMythorWepAttackComponent();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	// The Attack State that the character is in
	UPROPERTY(BlueprintReadWrite, Category = "Attacks")
		int32 m_AttackType = -1;
	UPROPERTY(BlueprintReadWrite, Category = "Attacks")
		int32 m_AttackQueue = 0;

	UPROPERTY() float m_BrakingFriction = 0.f;
	UPROPERTY() float m_AttackNum		= 0;
	UPROPERTY() float m_Damage1Mod		= 0.f;
	UPROPERTY() float m_Damage2Mod		= 0.f;
	UPROPERTY() float m_Force1Mod		= 0.f;
	UPROPERTY() float m_Force2Mod		= 0.f;
	UPROPERTY() float m_Angle			= 0.f;

	UPROPERTY(BlueprintReadWrite, Category = "Attacks")
		float m_AttackSpeed = 1.f;

	UPROPERTY(BlueprintReadOnly)
		UPotatoGameInstance* GameInst;
	//Weapon Objects to Spawn
	UPROPERTY(BlueprintReadOnly)
		class ABaseWeapon* m_Wep1 = NULL;
	UPROPERTY(BlueprintReadOnly)
		class ABaseWeapon* m_Wep2 = NULL;

	UPROPERTY(BlueprintReadWrite)
		bool m_isSheathed = false;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		int32 m_WepCycleAmtMax = 2;

	//Attack Combos
	UPROPERTY() int32 m_AttackComboMax = 4;
	UPROPERTY() int32 m_AttackAirComboMax = 3;

	// Attacking/Combat
	UPROPERTY(BlueprintReadWrite, Category = "Attacks")		// Bool to reduce the gravity of aerial attacks with an upward force
		bool m_AttackGrav = false;	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Attacks")
		bool m_bisBattle = false;
	UPROPERTY(BlueprintReadWrite, Category = "Attacks")
		int32 m_WeaponCycleSpawn = 0;
	UPROPERTY(BlueprintReadWrite, Category = "Attacks")
		int32 m_AttackComboType = 1;

	// Set stats based on attacks
	UFUNCTION()
		void GetCharAttackTypeStats(int AttackType);
	UFUNCTION()
		void GetCharAttackAerialStats(int AttackType);

	// Pointer to Character functions
	UFUNCTION(BlueprintCallable)
		class AmythorChar* GetOwnerCharacter() { return Character; }
	UFUNCTION(BlueprintCallable)
		void SetOwnerCharacter(class AmythorChar* inCharacter) { Character = inCharacter; }

	//Functions
	UFUNCTION()
		void LoadCharacterSpawnWeapons();

	UFUNCTION(BlueprintCallable, Category = "Weapons")
		void SetWepOpacity(float amount);

	// Reset Weapon's Hitbox to default properties
	UFUNCTION(BlueprintCallable, Category = "Weapons")
		void AttackResetWepHitbox();

	//------------------Spawn Weapon------------------
	UFUNCTION(BlueprintCallable)
		void SpawnWeapon(int spawnType, bool sheathe);
	UFUNCTION(BlueprintCallable)
		void MoveWeapon(bool isBattle);
	UFUNCTION(BlueprintCallable)
		void SpawnWeapon001();
	UFUNCTION(BlueprintCallable)
		void SpawnWeapon002();
	UFUNCTION(BlueprintCallable)
		void CycleWeapons();
	UFUNCTION(BlueprintCallable)
		void AutoSheathe();

	UFUNCTION()
		void AttackingStart();
	UFUNCTION(BlueprintCallable, Category = "Attacks")
		void Attacks(int type);
	UFUNCTION(BlueprintImplementableEvent, BlueprintCallable, Category = "Attacks")
		void BP_Attacks(int type);
	UFUNCTION(BlueprintCallable, Category = "Attacks")
		void Air_Attacks(int type);

	// Attack Hit
	void OnAttackHit(UBaseHitbox* Hitbox);

	//Weapon Modifiers
	UFUNCTION()
		float ModifyDamage(float BaseDamage, float Modifier);
	UFUNCTION()
		void SetWepIfAttacking(bool bIsAttackWep1, bool bIsAttackWep2);
	UFUNCTION(BlueprintCallable, Category = "Attacks")
		void ResetWeaponToDefaults();
	UFUNCTION(BlueprintCallable, Category = "Attacks")
		void SetAttackStats(float DamageMod1, float DamageMod2, float Force1, float Force2, float Angle);
	// Function for setting the stat mods for weapons or setting the friction when doing an attack
	UFUNCTION()
		void DoAttack(float BrakingFriction, float AttackNumber, float Wep1RDamageMod, 
			float Wep2LDamageMod, float Wep1RForceMod, float Wep2LForceMod, float Angle);

};

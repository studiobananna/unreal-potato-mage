// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "mythorChar/BaseCharacter.h"
#include "BaseNPC.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API ABaseNPC : public ABaseCharacter
{
	GENERATED_BODY()

public:
	// Sets default values for this character's properties
	ABaseNPC();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	UPROPERTY(BlueprintReadWrite, Category = "Dialogue")
		int32 m_dialogueTextItr;

	UPROPERTY(BlueprintReadWrite, Category = "Dialogue")
		int32 m_dialogueTextItrTens;

	UPROPERTY(BlueprintReadWrite, Category = "Dialogue")
		TArray<FString> m_dialogueArray;
	
};

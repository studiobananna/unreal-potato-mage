// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "SuperMeterComponent.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class GAMEPROJECT_API USuperMeterComponent : public UActorComponent
{
	GENERATED_BODY()

private:
	class ABaseCharacter* OwnerCharacter;

public:	
	// Sets default values for this component's properties
	USuperMeterComponent();

	// Super Meter (For Super Attacks, or Transformation)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Super")
		float m_superMeterMax = 100.f;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Super")
		float m_superMeter = 0.f;
	// Governs super meter regeneration
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Super")
		float m_superMeterRegen = 0.f;
	// Governs super meter recovered on hitting attack
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Super")
		float m_superMeterHitGain = 0.05f;
	// Governs super meter recovered on getting hurt
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Super")
		float m_superMeterHurtGain = 0.055f;

	//Sound Cue when ready
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Super")
		class USoundCue* m_SuperReady;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Super")
		float m_superMeterMinUsePerc = 0.5f;

	//*----------Functions----------*/
	UFUNCTION(BlueprintCallable, Category = "Super")
		void AddSuperMeter(float addAmount);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

		
};

// (C) Copyright Studio Bananna, all rights reserved


#include "Character_General/BaseNPC.h"

ABaseNPC::ABaseNPC() 
{
	// Set this character to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
}

void ABaseNPC::BeginPlay()
{

}

void ABaseNPC::Tick(float DeltaTime)
{

}

// (C) Copyright Studio Bananna, all rights reserved


#include "Character_General/SuperMeterComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundCue.h"
#include "../mythorChar/BaseCharacter.h"

// Sets default values for this component's properties
USuperMeterComponent::USuperMeterComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
	// Get Owning Character
	OwnerCharacter = (ABaseCharacter*) GetOwner();

	if (OwnerCharacter) {
		OwnerCharacter->SuperMeterComponent = this;
	}
}


// Called when the game starts
void USuperMeterComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void USuperMeterComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
	if (m_superMeterRegen > 0.f) {
		AddSuperMeter(m_superMeterRegen);
	}
}

void USuperMeterComponent::AddSuperMeter(float addAmount)
{
	m_superMeter += addAmount;
	m_superMeter = FMath::Clamp(m_superMeter, 0.f, m_superMeterMax);

	float minUsePerc = m_superMeterMax * m_superMeterMinUsePerc;

	if (m_superMeter >= minUsePerc && ( ( m_superMeter - addAmount ) < minUsePerc ) ) 
	{
		UGameplayStatics::PlaySound2D(this, m_SuperReady, 1.f, FMath::RandRange(1.0f, 1.2f), 0.f);
	}
}
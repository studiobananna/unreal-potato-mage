// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Components/WidgetComponent.h"
#include "ArmorBreakWidgetComponent.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API UArmorBreakWidgetComponent : public UWidgetComponent
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintImplementableEvent, Category = "ArmorWidget")
		void BP_PlaySplash();
};

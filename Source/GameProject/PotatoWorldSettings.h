// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/WorldSettings.h"
#include "PotatoWorldSettings.generated.h"

/**
 * 
 */

UCLASS(config = game, notplaceable)
class GAMEPROJECT_API APotatoWorldSettings : public AWorldSettings
{
	GENERATED_BODY()

public:
	APotatoWorldSettings();

	// The level scale for all enemies in the map
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Gameplay")
		float LevelScale = 1;
};

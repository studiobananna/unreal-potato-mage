// (C) Copyright Studio Bananna, all rights reserved

#include "GameProjectGameModeBase.h"
#include "ConstructorHelpers.h"
#include "mythorChar/BaseCharacter.h"
#include "Kismet/GameplayStatics.h"
#include "Blueprint/UserWidget.h"

AGameProjectGameModeBase::AGameProjectGameModeBase() {

	// Set Default Player Character Object to spawn
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Characters/mythorChar/BP_Player"));

	// Create Callback component
	CallbackComponent = CreateDefaultSubobject<UCallbackComponent>(TEXT("CallbackComponent"));

	if (PlayerPawnBPClass.Class != NULL) {
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
void AGameProjectGameModeBase::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

void AGameProjectGameModeBase::BeginPlay()
{
	Super::BeginPlay();

	// Get our player
	ABaseCharacter* MyCharacter = Cast<ABaseCharacter>(UGameplayStatics::GetPlayerPawn(this, 0));

	// Assign Health Mana HUD widget to the character associated with our player
	if (HUDWidgetClass && MyCharacter) {
		CurrentWidget = CreateWidget<UUserWidget>(GetWorld(), HUDWidgetClass);
		CurrentWidget->AddToPlayerScreen(0);
		MyCharacter->PlayerHUDWidget = CurrentWidget;
	}
}

// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "mythorChar/Magic/UseMagic.h"
#include "Inventory/UUseQuest.h"
#include "mythorChar/Weapons/UseWeapon.h"
#include "mythorChar/Weapons/BaseWeapon.h"
#include "Sound/SoundCue.h"
#include "PotatoWorldSettings.h"
#include "PotatoGameInstance.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API UPotatoGameInstance : public UGameInstance
{
	GENERATED_BODY()
public:

	//  Constructor
	UPotatoGameInstance();

	/** virtual function to allow custom GameInstances an opportunity to set up what it needs */
	virtual void Init() override;
		
	/** virtual function to allow custom GameInstances an opportunity to do cleanup when shutting down */
	virtual void Shutdown() override;
	
	/** Opportunity for blueprints to handle the game instance being shutdown. */
	//UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Shutdown"))
		//void ReceiveShutdown();

	/** Opportunity for blueprints to handle the game instance being initialized. */
	//UFUNCTION(BlueprintImplementableEvent, meta = (DisplayName = "Init"))
		//void ReceiveInit();

	// -------------PLAYER GENERAL STAT SAVE--------------------------
	UPROPERTY(BlueprintReadWrite, Category = "Player Current Stats")
		float PL_health = 0.f;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Player Current Stats")
		float PL_mana = 0.f;

	UPROPERTY(BlueprintReadWrite, Category = "Player Stats")
		int32 PL_Level = 1;
	UPROPERTY(BlueprintReadWrite, Category = "Player Stats")
		int32 PL_Skillpoints = 8;
	UPROPERTY(BlueprintReadWrite, Category = "Player Leveling Stats")
		int32 PL_vitality = 3;
	UPROPERTY(BlueprintReadWrite, Category = "Player Leveling Stats")
		int32 PL_haste = 3;
	UPROPERTY(BlueprintReadWrite, Category = "Player Leveling Stats")
		int32 PL_dexterity = 4;
	UPROPERTY(BlueprintReadWrite, Category = "Player Leveling Stats")
		int32 PL_intelligence = 4;
	UPROPERTY(BlueprintReadWrite, Category = "Player Stats")
		int32 PL_EXP = 0.f;
	UPROPERTY(BlueprintReadWrite, Category = "Player Stats")
		int32 PL_EXP_MAX = 120.f;

	// UNLOCKS
	UPROPERTY(BlueprintReadWrite, Category = "Unlocks")
		bool PL_B_UNLOCKED_SPRINT = false;
	UPROPERTY(BlueprintReadWrite, Category = "Unlocks")
		bool PL_B_UNLOCKED_SKILLS = false;

	// -------------PLAYER SPAWN LOCATION--------------------------
	UPROPERTY(BlueprintReadWrite, Category = "Player Location")
		int32 PL_SpawnPoint = 0;
	UPROPERTY(BlueprintReadWrite, Category = "Player Location") // Hide the HUD
		bool PL_isLoadingSaveLocation = false;

	// -------------PLAYER MYTHOR SAVE--------------------------
	UPROPERTY(BlueprintReadWrite, Category = "Player Mythor Stats")
		int32 MYTH_wep = 1;
	UPROPERTY(BlueprintReadWrite, Category = "Player Mythor Stats") // Change Mythor's materials for events
		int32 MYTH_BodyType = 0;

	// -------------GLOBAL BASED VARS--------------------------
	UPROPERTY(BlueprintReadWrite, Category = "Global") // Holds a value to trigger certain events (which boss to spawn for boss arena...etc.) (USE WITH CAUTION)
		int32 TimerResetModifier = 0;

	// Affects enemy level scaling for the room
	UPROPERTY(BlueprintReadWrite, Category = "Global")
		float LevelScale = 1;

	// -------------EVENT BASED VARS--------------------------
	UPROPERTY(BlueprintReadWrite, Category = "Events") // Holds a value to trigger certain events (which boss to spawn for boss arena...etc.) (USE WITH CAUTION)
		int32 EVT_EventHolder01 = 0;
	UPROPERTY(BlueprintReadWrite, Category = "Events") // Hide the HUD
		bool EVT_HideHUD = false;
	UPROPERTY(BlueprintReadWrite, Category = "Events") // Event trigger for Intro003
		bool SET_newLevelSpawn = true;

	// -------------SAVE MAP AND LOCATION VARS--------------------------
	UPROPERTY(BlueprintReadWrite, Category = "Map") // Save Map Name to Load
		FString SCENE_MAP_NAME = "";
	UPROPERTY(BlueprintReadWrite, Category = "Map") // Save Station saved at the display
		FString SCENE_SAVE_STATION_NAME = "";
	UPROPERTY(BlueprintReadWrite, Category = "Map") // Save Map Player Location
		FVector SCENE_PLAYER_LOCATION = FVector(0.f, 0.f, 0.f);
	UPROPERTY(BlueprintReadWrite, Category = "Map") // Save Map Player Rotation
		FRotator SCENE_PLAYER_ROTATION = FRotator(0.f, 0.f, 0.f);


	// --------------------------NPC EVENT VARS--------------------------
	UPROPERTY(BlueprintReadWrite, Category = "Events") // Event trigger for Intro003
		bool EVT_Intro_Scene003 = false;
	UPROPERTY(BlueprintReadWrite, Category = "Events") // Event handling for Flanna NPC
		int32 EVT_NPC_Flan = 0;
	UPROPERTY(BlueprintReadWrite, Category = "Events") // Event handling for Frederick NPC
		int32 EVT_NPC_Frederick = 0;


	// -------------SAVING/LOADING VARS--------------------------
	UPROPERTY(BlueprintReadWrite, Category = "Saving") // Save game slot (old)
		int32 SaveSlotIndex = 0;


	// -------------PLAYER SAVE INVENTORY--------------------------
	// ---------------------Currency---------------------
	UPROPERTY(BlueprintReadWrite, Category = "Player Stats")
		int32 PL_shungite_CUR = 0;

	//---------------------Magics---------------------
	// All collected Magics in inventory
	UPROPERTY(BlueprintReadWrite, Category = "Magic")
		TArray<FString> PL_InvMagicArry;
	// Equipped Magics
	UPROPERTY(BlueprintReadWrite, Category = "Magic")
		TArray<FString> PL_MagicArry;
	UPROPERTY(BlueprintReadWrite, Category = "Magic")
		int32 PL_MagicArryScrollType = 1;

	//----------PASSIVES--------------
	UPROPERTY(BlueprintReadWrite, Category = "Passives")
		bool PL_DevilVision = true;

	//---------------------Weapon Equips---------------------
	// All collected Weapons in inventory
	UPROPERTY(BlueprintReadWrite, Category = "Magic")
		TArray<FString> PL_InvWeaponArry;

	UPROPERTY(BlueprintReadWrite, Category = "Magic")
		TArray<FString> PL_EquipWeaponArry;

	//---------------------Items---------------------
	// All collected Items in inventory
	UPROPERTY(BlueprintReadWrite, Category = "Items")
		TArray<FString> PL_InvItemArry;
	// Save Collected Items
	UPROPERTY(BlueprintReadWrite, Category = "Items")
		TMap<FString, bool> PL_MAP_CollectedItems;

	//---------------------Quests---------------------
	UPROPERTY(BlueprintReadWrite, Category = "Quests")
		TArray<FString> PL_QuestArry;

	UPROPERTY(BlueprintReadWrite, Category = "Quests")
		FString PL_TrackedQuest;

	UFUNCTION(BlueprintCallable, Category = "Inventory")
		void EmptySavedInventory();

	UPROPERTY(BlueprintReadWrite, Category = "Unlocks")
		TArray<FString> PL_FightUnlocks;

	// ABYSSAL MODE
	UPROPERTY(BlueprintReadWrite, Category = "Abyssal")
		int32 EVT_AbyssalEnemyArrNumb = 0;

	UPROPERTY(BlueprintReadWrite, Category = "Abyssal")
		int32 EVT_AbyssalFloor = 0;

	UPROPERTY(BlueprintReadWrite, Category = "Abyssal")
		float EVT_AbyssalEnemyScale = 1.f;

	UPROPERTY(BlueprintReadWrite, Category = "Abyssal")
		USoundCue* BattleMusicSoundCue;
	UPROPERTY(BlueprintReadWrite, Category = "Abyssal")
		UAudioComponent* BattleMusicAudioComp;

	//---------------------FUNCTIONS---------------------
	UFUNCTION(BlueprintCallable, Category = "Save/Load")
		void SaveGame(int SaveSlot);
	UFUNCTION(BlueprintCallable, Category = "Save/Load")
		UPotatoSaveGame* LoadGame(int SaveSlot);

	// Checks to see if the saved game exists, then load it, or return false
	UFUNCTION(BlueprintCallable, Category = "Save/Load")
		bool StartLoadedGame(int SaveSlot);

	// Checks to see if the saved game exists, then load it, or return false
	UFUNCTION(BlueprintCallable, Category = "Save/Load")
		void ResetGameInstance();
};

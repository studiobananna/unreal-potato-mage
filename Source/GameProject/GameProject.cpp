// (C) Copyright Studio Bananna, all rights reserved

#include "GameProject.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, GameProject, "GameProject" );

// (C) Copyright Studio Bananna, all rights reserved

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "CallbackComponent.h"
#include "GameProjectGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class GAMEPROJECT_API AGameProjectGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
public:
		AGameProjectGameModeBase();

		virtual void Tick(float DeltaTime) override;

		virtual void BeginPlay() override;
protected:
	//Get Widget Class for HUD
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = "HUDWidgetClass", Meta = (BlueprintProtected = "true"))
	TSubclassOf<class UUserWidget> HUDWidgetClass;

public:

	UPROPERTY()
	UUserWidget* CurrentWidget;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Callbacks")
		UCallbackComponent* CallbackComponent;
};
